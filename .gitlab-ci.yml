stages:
  - notify
  - prepare
  - docker_build
  # Adding a test stage isn't needed by any job, but
  # a temporary stop gap. See https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150839#note_1884473792.
  - test
  - validate
  - generate
  - post_generate
  - release  # Perform semantic release
  - publish  # Publish to package/container registry
  - renovate_bot

include:
  - local: .gitlab-ci-asdf-versions.yml
  - local: .gitlab-ci-other-versions.yml

  # This template should be included in all Infrastructure projects.
  # It includes standard checks, gitlab-scanners, validations and release processes
  # common to all projects using this template library.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/README.md#templatesstandardyml
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.43  # renovate:managed
    file: templates/standard.yml

  # Includes a base template for running an opinionated docker buildx build
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docker.md
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v2.43  # renovate:managed
    file: 'docker.yml'

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.dev_tools:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  stage: validate

.build_tools:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  artifacts:
    paths:
      - tamland.log
    when: always
  before_script:
    - pip list

lint:
  extends: .dev_tools
  script:
    - make lint

formatting:
  extends: .dev_tools
  script:
    - make formatting

security_scan:
  extends: .dev_tools
  script:
    - make security-scan

mypy:
  extends: .dev_tools
  script:
    - mypy --install-types --non-interactive

test:
  extends: .dev_tools
  script:
    - make test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

.build_with_cache:
  extends: .build_tools
  variables:
    # Freeze time so we can use the cache stored in the package registry
    # (which does not get updated automatically anymore)
    TAMLAND_NOW_TIME: "2024-01-04"
  before_script:
    - scripts/download-data-cache.sh

test_shell_forecast_and_report:
  # Temporary measure to trigger the forecasting code path through the shell
  stage: validate
  extends: .build_with_cache
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  script:
    - bin/shell forecast . --pattern=patroni.cpu --manifest=.manifest-example-for-ci.json
    - bin/shell report . --publish-to /tmp/report
  variables:
    TAMLAND_HISTORICAL_DAYS: 7
    TAMLAND_MIN_SAMPLES_FOR_PREDICTION: 24
    TAMLAND_FORECAST_DAYS: 7
    TAMLAND_ONLY_CACHE: 1
  artifacts:
    expire_in: 1 day
    paths:
      - forecasts

test_chatops:
  stage: validate
  extends: .build_with_cache
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  allow_failure: true
  script:
    - bin/shell chatops . patroni.cpu --manifest=.manifest-example-for-ci.json

.container_builds:
  extends:
    - .docker_buildx_base

container_image_build:
  stage: prepare
  variables:
    DOCKER_DESTINATION: $CI_REGISTRY_IMAGE:${CI_COMMIT_SHORT_SHA}
    DOCKER_BUILD_FILE: Dockerfile
  extends:
    - .container_builds

# Tags use the git tag, not the slug
container_image_tagged:
  stage: publish
  variables:
    DOCKER_BUILD_FILE: Dockerfile
    DOCKER_DESTINATION: $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}
    # Also publish the latest tag...
    DOCKER_BUILDX_EXTRA_ARGS: |
      --tag $CI_REGISTRY_IMAGE:latest
  extends:
    - .container_builds
  rules:
    - if: '$CI_COMMIT_TAG'

helm:validate_chart:
  stage: validate
  image: alpine/k8s:1.32.2
  script:
    - helm lint ./chart

.helm-publish:
  stage: publish
  image: alpine/k8s:1.32.2
  script:
    - export version_info=$(./scripts/print-version.sh)
    - export app_version=$(echo "${version_info}" | jq -r .app_version)
    - export helm_version=$(echo "${version_info}" | jq -r .helm_version)
    - export helm_channel=$(echo "${version_info}" | jq -r .helm_channel)
    - chart/build_and_release.sh "${helm_channel}" "${helm_version}" "${app_version}"

helm:publish_chart:
  extends:
    - .helm-publish
  needs: ["container_image_build"]
  rules:
    - if: '$CI_COMMIT_TAG == null'

helm:publish_chart_tagged:
  extends:
    - .helm-publish
  needs: ["container_image_tagged"]
  rules:
    - if: '$CI_COMMIT_TAG'

pages:
  stage: publish
  image: python
  script:
    - pip install mkdocs
    - mkdir public
    - mkdocs build -f docs/mkdocs.yml -d ../public
  artifacts:
    paths:
    - public
  only:
    refs:
      - main
