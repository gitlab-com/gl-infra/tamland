#!/usr/bin/env bash

#helm plugin install https://github.com/chartmuseum/helm-push

set -ue

helm_channel="$1"
helm_version="$2"
app_version="$3"

helm repo add --username gitlab-ci-token --password "${CI_JOB_TOKEN}" tamland "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/${helm_channel}"
helm package ./chart --app-version="${app_version}" --version="${helm_version}"
helm cm-push "tamland-${helm_version}.tgz" tamland
