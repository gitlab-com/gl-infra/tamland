import os
from datetime import date
import gitlab_api_client

# Internal script to create to-do items for yourself when on capacity planning rotation.
#
# How it works:
#
# Using your personal access token with "api" scope,
# a to-do item will be added when a capacity planning issue due date is overdue, or
# when "capacity-planning::"" workflow status label is missing. Issues API will only create to-do item for user on issue once,
# so you can safely run script multiple times during your capacity planning rotation.
#
# How to run:
#
# GITLAB_API_PRIVATE_TOKEN="glpat-xxxxxxx" bin/tamland-run python scripts/triage_duties.py
if __name__ == "__main__":
    client = gitlab_api_client.GitLabAPIClient(
        os.environ.get("GITLAB_API_URL", "https://gitlab.com/api/v4"),
        os.environ.get("GITLAB_API_PRIVATE_TOKEN", "glpat-xxxxxxx"),
    )
    project_id = os.environ.get("CAPACITY_PLANNING_PROJECT_ID", "26716041")

    issues = client.get_issues(project_id)

    for issue in issues:
        # https://about.gitlab.com/handbook/engineering/infrastructure/capacity-planning/#due-dates
        is_overdue = issue["due_date"] and issue["due_date"] < str(date.today())
        # https://about.gitlab.com/handbook/engineering/infrastructure/capacity-planning/#workflow-status-labels
        is_missing_workflow_status = not any(
            label
            in [
                "capacity-planning::investigate",
                "capacity-planning::monitor",
                "capacity-planning::in-progress",
                "capacity-planning::verification",
            ]
            for label in issue["labels"]
        )

        if is_overdue or is_missing_workflow_status:
            client.create_todo_item(project_id, issue["iid"])

    # ...that's it! 🎉 You should now have to-do list full of capacity planning issues.
    # Refer to https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/projections.html#triage-duties
    #
    # Looking for more? Feel free to contribute to include more types of issues for triage duties, e.g.
    # capacity-planning::priority::do, violation:hard-80%-confidence, unassigned, etc.
