#!/usr/bin/env bash

set -euo pipefail

# Use OPS_GITLAB_NET_PRIVATE_TOKEN if set, otherwise use CI_JOB_TOKEN.
if [ -z "${OPS_GITLAB_NET_PRIVATE_TOKEN+x}" ]; then
  AUTH_HEADER="JOB-TOKEN: $CI_JOB_TOKEN"
else
  AUTH_HEADER="PRIVATE-TOKEN: $OPS_GITLAB_NET_PRIVATE_TOKEN"
fi

GITLAB_API_URL="${GITLAB_API_URL:-https://ops.gitlab.net/api/v4}"
GITLAB_PROJECT_ID="${GITLAB_PROJECT_ID:-387}"

curl -L --fail -O --header "$AUTH_HEADER" "${GITLAB_API_URL}/projects/${GITLAB_PROJECT_ID}/packages/generic/thanos-data-cache/v1/data.tar.gz"

tar -xzf data.tar.gz

du -hs data
