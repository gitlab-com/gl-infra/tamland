#!/usr/bin/env bash

set -e

if [ -z "$PROMETHEUS_USERNAME" ]; then
  echo "Missing Prometheus username!"
  exit 1
fi
if [ -z "$PROMETHEUS_PASSWORD" ]; then
  echo "Missing Prometheus password!"
  exit 1
fi

basic_auth=$(echo -n "${PROMETHEUS_USERNAME}:${PROMETHEUS_PASSWORD}" | base64)

cat <<EOF
{
  "headers": {
    "Authorization": "Basic ${basic_auth}",
    "X-Scope-OrgID": "gitlab-gprd|gitlab-ops|metamonitoring|gitlab-observability|runway"
  }
}
EOF
