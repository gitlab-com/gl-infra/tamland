#!/usr/bin/env bash

set -euo pipefail

version_info=$(./scripts/print-version.sh)

echo "${version_info}"

version=$(echo "${version_info}" | jq -r .version)
tagged=$(echo "${version_info}" | jq -r .tagged)

if [ "${tagged}" = "true" ]; then
  poetry version "${version}"
  poetry build
  TWINE_PASSWORD="${CI_JOB_TOKEN}" TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi" dist/*
else
  echo "Not publishing pypi package for an untagged version"
fi
