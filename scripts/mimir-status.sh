#!/usr/bin/env bash
# This script can be used to verify whether connection to Mimir is fully functional from localhost
set -e
curl "$PROMETHEUS_QUERY_URL/api/v1/status/config"
