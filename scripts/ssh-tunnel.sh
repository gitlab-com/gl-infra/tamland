#!/usr/bin/env bash

ssh -D "18202" -l "${USER:-$(whoami)}" "lb-bastion.gstg.gitlab.com" 'echo "Connected! Press Enter to disconnect."; read disconnect' >&2
