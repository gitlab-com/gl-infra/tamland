#!/usr/bin/env bash

set -euo pipefail

if ! command -v jq &>/dev/null; then
  echo "jq not found, trying to install using apt" >&2
  apt-get update >&2
  apt-get install --yes jq >&2
fi

# We hardcode the URL here, so we can also access release information from other GitLab instances and locally
releases_url="${RELEASES_URL:-https://gitlab.com/api/v4/projects/20709159/releases}"
echo "Getting release information from ${releases_url}" >&2
release_data=$(curl -s "${releases_url}")
release_sha=$(echo "$release_data" | jq -r '.[0].commit.id')
release_tag=$(echo "$release_data" | jq -r '.[0].tag_name')

local_sha=$(git rev-parse HEAD)

if [ "${local_sha}" = "${release_sha}" ]; then
  # Full release - tag with information from release
  app_version=${release_tag}
  helm_version=${release_tag#v}
  helm_channel="stable"
  tagged=true
else
  # Local version doesn't match release, tag accordingly
  git fetch --tags >&2
  app_version="${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}"
  helm_version=$(git describe --tags)
  helm_channel="unstable"
  tagged=false
fi

echo "Local SHA: ${local_sha}" >&2
echo "Latest release: ${release_tag} = ${release_sha}" >&2
echo "==> Helm channel: ${helm_channel}" >&2
echo "==> Tamland version: ${app_version}" >&2

cat <<JSON
{
  "app_version": "${app_version}",
  "helm_version": "${helm_version}",
  "helm_channel": "${helm_channel}",
  "sha": "${local_sha}",
  "tagged": ${tagged}
}
JSON
