#!/usr/bin/env bash

set -euo pipefail

AUTH_HEADER="PRIVATE-TOKEN: $TAMLAND_CACHE_MAINTENANCE_TOKEN"

# Get the first package id with the name `thanos-data-cache`
package_id=$(curl --fail --header "$AUTH_HEADER" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages?package_name=thanos-data-cache" | jq -r '.[0].id')

# Get all package ids except for the first one ordered by `created_at`
old_ids=$(curl --fail --header "$AUTH_HEADER" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/${package_id}/package_files" | jq 'sort_by(.created_at) | reverse | .[1:] | .[].id')

for file_id in $old_ids; do
  url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/${package_id}/package_files/${file_id}"
  echo "Deleting ${url}"
  curl --fail --request DELETE --header "$AUTH_HEADER" "$url"
done
