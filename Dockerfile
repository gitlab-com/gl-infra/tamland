FROM python:3.13-bullseye

RUN mkdir -p /opt/tamland
WORKDIR /opt/tamland

RUN mkdir -p scripts
COPY scripts/docker-install-deps.sh scripts/docker-install-deps.sh
RUN ./scripts/docker-install-deps.sh

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install poetry

COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install

COPY . /opt/tamland
