import os


def relative_path(path):
    return (
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..")
        + "/"
        + path
    )


class suppress_stdout_stderr(object):
    """
    A context manager for doing a "deep suppression" of stdout and stderr in
    Python, i.e. will suppress all print, even if the print originates in a
    compiled C/Fortran sub-function.
    """

    def __init__(self):
        self.f = open(relative_path("tamland.prophet.log"), "a")
        # Open a pair of null files
        self.file_fds = [self.f.fileno() for x in range(2)]
        # Save the actual stdout (1) and stderr (2) file descriptors.
        self.save_fds = (os.dup(1), os.dup(2))

    def __enter__(self):
        # Assign the null pointers to stdout and stderr.
        os.dup2(self.file_fds[0], 1)
        os.dup2(self.file_fds[1], 2)

    def __exit__(self, *_):
        # Re-assign the real stdout/stderr back to (1) and (2)
        os.dup2(self.save_fds[0], 1)
        os.dup2(self.save_fds[1], 2)
        # Close the log file
        self.f.close()
