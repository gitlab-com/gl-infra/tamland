# This file contains common utility methods used in
# operations/saturation/utilization forecasting
import os
from tamland.core import prom_cache
from datetime import datetime, timezone
from pathlib import Path

min_samples_for_prediction = int(
    os.getenv("TAMLAND_MIN_SAMPLES_FOR_PREDICTION", "720")
)  # 30 days, in hours
now_cache_path = Path(prom_cache.default_cache_dir, "now")


def cache_end_time() -> str:
    end_time_s = ""
    if now_cache_path.exists():
        end_time_s = now_cache_path.read_text()

    return end_time_s


# `now` is, in order of precedence:
# 1. `TAMLAND_NOW_TIME` as YYYY-MM-DD
# 2. `data/now`, when `TAMLAND_ONLY_CACHE=1`
# 3. Today's date
def find_now():
    end_time_s = os.getenv("TAMLAND_NOW_TIME", "")

    if not end_time_s and prom_cache.only_cache:
        end_time_s = cache_end_time()

    if end_time_s and end_time_s != "":
        end_time = datetime.strptime(end_time_s, "%Y-%m-%d")
    else:
        end_time = datetime.now()

    return truncate_datetime(end_time)


# Most of operations in Tamland work with "day" span. It may be troublesome if
# a datetime object includes smaller units. This method stripped insignificant
# units from the datetime.
def truncate_datetime(dt):
    return dt.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone.utc)


now = find_now()
