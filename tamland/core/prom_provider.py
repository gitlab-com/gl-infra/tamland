from __future__ import annotations
import os
import subprocess  # nosec B404
from typing import Protocol

import requests
from cachetools.func import ttl_cache
from prometheus_pandas import query
from pydantic import BaseModel
from pydantic_yaml import parse_yaml_raw_as

from tamland.core.logger import logger

DEFAULT_CACHE_TTL = float(os.environ.get("PROMETHEUS_HEADER_PROVIDER_TTL", 1800))
"""Default cache time to live (seconds) for cached authentication information (headers) from a header provider"""


def factory() -> PrometheusProvider:
    url = os.environ.get(
        "THANOS_QUERY_URL",
        os.environ.get("PROMETHEUS_QUERY_URL", "http://localhost:10902"),
    )
    header_provider = os.environ.get("PROMETHEUS_HEADER_PROVIDER", None)

    if header_provider:
        return PrometheusProviderWithDynamicHeaders(
            url=url, header_provider=header_provider
        )
    else:
        return DefaultPrometheusProvider(url=url)


class PrometheusUnavailableError(Exception):
    """Prometheus or a header provider it is dependent on is currently not available"""

    pass


class PrometheusProvider(Protocol):
    def prometheus(self) -> query.Prometheus: ...


class DefaultPrometheusProvider(PrometheusProvider):
    """Provides a query.Prometheus instance for the given URL"""

    def __init__(self, *, url: str):
        self.url = url

    @ttl_cache(ttl=DEFAULT_CACHE_TTL)
    def prometheus(self) -> query.Prometheus:
        """
        Instantiate a query.Prometheus instance to connect to Prometheus

        The instance is cached for DEFAULT_CACHE_TTL seconds.
        This allows to automatically re-generate the instance after the timeout has been hit.

        For example, this is useful to facilitate token refreshes before they expire and puts
        a limit on the refresh frequency.
        """
        session = requests.Session()

        for key, value in self.headers().items():
            session.headers[key] = value

        return query.Prometheus(self.url, session)

    def headers(self) -> dict[str, str]:
        """Any additional headers to add to requests, this can be overridden"""
        return dict()


class HeaderProviderOutput(BaseModel):
    headers: dict[str, str]


class PrometheusProviderWithDynamicHeaders(DefaultPrometheusProvider):
    """
    Allows to inject dynamic headers from an external header provider

    The header_provider needs to be executable and return a JSON document
    following the schema of HeadersProviderOutput.
    """

    def __init__(self, *, url: str, header_provider: str):
        self.url = url
        self.header_provider = header_provider

    def headers(self) -> dict[str, str]:
        """Retrieve headers from an external header provider"""
        try:
            logger.info(f"Refreshing prometheus headers from: {self.header_provider}")
            result = subprocess.run(  # nosec B603
                self.header_provider.split(" "),
                capture_output=True,
                text=True,
            )

            if result.returncode != 0:
                raise PrometheusUnavailableError(
                    f"returned non-zero exit status:\n{result.stdout}\n----\n{result.stderr}----\n"
                )

            return parse_yaml_raw_as(HeaderProviderOutput, result.stdout).headers
        except subprocess.CalledProcessError as e:
            raise PrometheusUnavailableError(e)
