from __future__ import annotations

import math
from dataclasses import dataclass, field
from datetime import datetime
from functools import cache
from typing import Tuple, Iterator, cast, IO

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import matplotlib
from matplotlib import pyplot
from pandas import DataFrame
from plotly.graph_objs import Figure
from prophet import Prophet
from prophet.plot import plot_plotly
from pydantic import BaseModel

from tamland.core import forecasting
from tamland.core import prom_query
from tamland.core.logger import logger
from tamland.core.models import Component, Event, SLO
from tamland.core.util import suppress_stdout_stderr

from pydantic.version import VERSION as PYDANTIC_VERSION

assert PYDANTIC_VERSION >= "2"

# use `agg` as a backend for matplotlib, otherwise TkAgg could be picked which
# requires connecting to wayland display and does not allow rendering off the main
# thread. We can use this non-interactive backend for now as we don't generate
# interactive graphs
# https://matplotlib.org/stable/users/explain/figure/backends.html#selecting-a-backend
matplotlib.use("Agg")


class ComponentForecastMetrics(BaseModel):
    """Captures statistical metrics like mean-squared-error, confidence range, etc."""

    confidence_range: float | None = None
    """The width of the prediction interval at the end of the forecast, i.e. yhat_upper - yhat_lower"""


@dataclass
class ComponentForecast:
    component: Component
    history: HistoricalData
    model: Prophet
    series: pd.DataFrame
    slo_violations: list[PredictedSLOViolation] = field(default_factory=list)
    metrics: ComponentForecastMetrics = field(default_factory=ComponentForecastMetrics)


class PredictedSLOViolation(BaseModel):
    """Predicted dates for SLO violations"""

    slo: SLO
    yhat_date: datetime | None = None
    """Predicated date when SLO is going to be violated"""
    yhat_upper_date: datetime | None = None
    """Predicted date at upper end of confidence interval (typically 80% confidence)"""

    def predicts_violation(self) -> bool:
        """Check if any of the prediction dates is set, i.e. if there is a predicted violation"""
        return any([self.yhat_date, self.yhat_upper_date])


class HistoricalData:
    def __init__(self, component: Component) -> None:
        self.component = component

    @cache
    def load(self) -> pd.DataFrame:
        """Retrieve historical data frame from Prometheus for given Component. Subsequent calls return cached data frame."""

        # Load historical values
        from_dt, to_dt = self.component.capacity_planning.history_range
        df = prom_query.batched_query_range(
            self.component.saturation_ratio_query, from_dt, to_dt
        )

        if len(df) == 0:
            return df

        df.rename(columns={df.columns[0]: "y"}, inplace=True)
        df["ds"] = df.index

        # This is a fix for a regression related to upgrading from PyArrow 12 to PyArrow 18.
        # See https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/4014
        df["ds"] = df["ds"].astype("datetime64[ns]")

        # Boundaries
        df["cap"] = 1
        df["floor"] = 0

        # Remove outliers from y series but remember them in y_outlier series
        df["y_outlier"] = None
        for outlier in self.component.capacity_planning.ignore_outliers:
            start = outlier.start
            end = outlier.end

            df.loc[(df["ds"] >= start) & (df["ds"] < end), "y_outlier"] = df.loc[
                (df["ds"] >= start) & (df["ds"] < end), "y"
            ]

            df.loc[(df["ds"] >= start) & (df["ds"] < end), "y"] = None

        return df

    @property
    def series(self) -> pd.DataFrame:
        return self.load()

    def generate_changepoints(self) -> list[str]:
        # "recreate" the default changepoints and adding custom ones
        # code references https://github.com/facebook/prophet/blob/3ccbd0e0e95d04bbadf94486a7418644347a275f/python/fbprophet/forecaster.py#L367
        df = self.series

        history = df[df["y"].notnull()].copy()

        # Distribute changepoints across the first changepoint_range % of history.
        # This defaults to 0.8, but we allow per component overrides here.
        # When increasing the range, we pick up any recent trend changes earlier.
        #
        # Resembles implementation in https://github.com/facebook/prophet/blob/3ccbd0e0e95d04bbadf94486a7418644347a275f/python/fbprophet/forecaster.py#L38-L40
        hist_size = int(
            np.floor(
                history.shape[0] * self.component.capacity_planning.changepoint_range
            )
        )
        cp_indexes = (
            np.linspace(
                0,
                hist_size - 1,
                self.component.capacity_planning.changepoints_count + 1,
            )
            .round()
            .astype(int)
        )
        default_changepoints = list(
            map(
                lambda x: x.date(),
                history.iloc[cp_indexes]["ds"].tail(-1),
            )
        )

        def within_history(date) -> bool:
            """Check whether given date is within bounds of history = training data"""
            dt = pd.to_datetime(date)
            too_low = dt < history["ds"].min()
            too_high = dt > history["ds"].max()
            return not (too_low or too_high)

        # Since changepoints are specified as date and default_changepoints has datetime objects,
        # there is an edge case when we don't have complete data coverage for the first day in
        # history and this day is a chosen default changepoint.
        #
        # Example: History starts at 2022-07-25 13:00:00, but we can't choose 2022-07-25 as a changepoint
        #          because Prophet will complain there is not enough data for this date.
        #
        # As a result of this, we filter out any dates that fall outside of the training data.
        #
        # This also allows us to retain a history of changepoints, so we don't have to clean this
        # up as time goes by.
        all_changepoints = map(
            lambda d: d.strftime("%Y-%m-%d"),
            default_changepoints + self.component.capacity_planning.changepoints,
        )

        return list(np.unique(list(filter(within_history, all_changepoints))))


class Forecaster:
    def __init__(
        self,
        component: Component,
        *,
        min_samples_for_prediction: int = forecasting.min_samples_for_prediction,
    ) -> None:
        self.component = component
        self.min_samples_for_prediction = min_samples_for_prediction

    def generate(self) -> ComponentForecast | None:
        logger.info(f"Starting forecast for {self.component}")

        history = HistoricalData(self.component)

        df = history.load()

        if len(df) < self.min_samples_for_prediction:
            logger.info(
                f"Not enough samples for {self.component} to generate forecast "
                f"(a minimum of {self.min_samples_for_prediction} is needed, but we only have {len(df)})"
            )
            return None

        prophet = Prophet(
            daily_seasonality=self.component.capacity_planning.daily_seasonality,
            weekly_seasonality=self.component.capacity_planning.weekly_seasonality,
            yearly_seasonality=self.component.capacity_planning.yearly_seasonality,
            changepoint_prior_scale=0.05,
            changepoint_range=self.component.capacity_planning.changepoint_range,
            changepoints=history.generate_changepoints(),
        )

        prophet.add_country_holidays(country_name="US")

        try:
            with suppress_stdout_stderr():
                prophet.fit(df)
        except ValueError as e:
            logger.error(
                f"Skipping the forecast for {self.component}. Not enough data ({e})"
            )
            return None

        future = prophet.make_future_dataframe(
            periods=24 * self.component.capacity_planning.forecast_days, freq="h"
        )

        # Boundaries
        future["floor"] = 0
        forecast = prophet.predict(future)

        for slo in self.component.saturation_point.slos:
            forecast[f"{slo.name}_threshold"] = slo.threshold

        slo_violations = SLOViolationPredictor(self.component).predict_violations(
            forecast
        )

        last_record = forecast.iloc[-1]
        metrics = ComponentForecastMetrics(
            confidence_range=last_record["yhat_upper"] - last_record["yhat_lower"]
        )

        return ComponentForecast(
            component=self.component,
            history=history,
            model=prophet,
            series=forecast,
            slo_violations=slo_violations,
            metrics=metrics,
        )


class SLOViolationPredictor:
    """Predict SLO violations based on forecast data"""

    def __init__(self, component: Component):
        self.component = component

    def predict_violations(self, df: DataFrame) -> list[PredictedSLOViolation]:
        violations = []

        for slo in self.component.saturation_point.slos:
            violations.append(
                PredictedSLOViolation(
                    slo=slo,
                    yhat_date=self._find_earliest_violation(df, slo.threshold, "yhat"),
                    yhat_upper_date=self._find_earliest_violation(
                        df, slo.threshold, "yhat_upper"
                    ),
                )
            )

        return violations

    def _find_earliest_violation(
        self, df: DataFrame, threshold: float, series: str
    ) -> datetime | None:
        now = datetime.now()
        mask = (df["ds"] > now) & (df[series] >= threshold)

        earliest_df = df.loc[mask]
        if len(earliest_df) == 0:
            return None

        return cast(datetime, earliest_df.iloc[0]["ds"].to_pydatetime())


class EventAnnotator:
    def __init__(self, forecast: ComponentForecast) -> None:
        self.forecast = forecast

    def _events(self) -> Iterator[Tuple[int, Event]]:
        for i, event in enumerate(
            sorted(self.forecast.component.events, key=lambda e: e.date)
        ):
            yield i + 1, event

    def add_to_figure(self, fig: Figure) -> None:
        for i, event in self._events():
            fig.add_vline(
                x=event.date, line_width=2, line_dash="dash", line_color="green"
            )

            dmin = self.forecast.history.series["floor"].min()
            dmax = self.forecast.history.series["cap"].max()

            fig.add_annotation(
                x=event.date,
                y=dmax,
                yref="paper",
                showarrow=True,
                text=f" <b>{i}</b> ",
                font=dict(size=18, color="white"),
                arrowcolor="green",
                bgcolor="green",
            )

            # Re-add similar trace so we get legend entries
            # (and keep the vline as it goes straight to the upper border)
            fig.add_trace(
                go.Scatter(
                    x=[event.date, event.date],
                    y=[dmin, dmax],
                    mode="lines",
                    line=dict(color="green", width=2, dash="dash"),
                    name=f"<b>({i})</b>: {event.name}",
                    showlegend=True,
                    legendgroup="events",
                    legendgrouptitle_text="Component events",
                )
            )


class Plotter:
    """Generate plots for the given forecast data"""

    def __init__(self, forecast: ComponentForecast):
        self.forecast = forecast

    @property
    def component(self) -> Component:
        return self.forecast.component

    def plot_components(self) -> Figure:
        return self.forecast.model.plot_components(self.forecast.series)

    def plot_debug(self, fp: IO, *, format: str = "png", figsize=(1200, 800)) -> None:
        self._plot(fp, format=format, changepoints=True, trend=True, figsize=figsize)

    def plot(self, fp: IO, *, format: str = "png") -> None:
        self._plot(fp, format=format)

    def _plot(
        self,
        fp: IO,
        *,
        format: str,
        changepoints=False,
        trend=False,
        figsize=(1200, 800),
    ) -> None:
        title = "%s service: '%s' resource" % (
            self.forecast.component.service.name,
            self.forecast.component.saturation_point.name,
        )
        fig = plot_plotly(
            self.forecast.model,
            self.forecast.series,
            changepoints=changepoints,
            trend=trend,
            figsize=figsize,
        )

        # Disable legends for all
        for item in fig.data:
            item.showlegend = item.name in ("Predicted", "Actual")

        EventAnnotator(self.forecast).add_to_figure(fig)

        fig.add_trace(
            go.Scatter(
                x=self.forecast.history.series["ds"],
                y=self.forecast.history.series["y_outlier"],
                name="Outlier",
                marker=dict(color="grey", size=4),
                mode="markers",
                showlegend=len(
                    self.forecast.component.capacity_planning.ignore_outliers
                )
                > 0,
                connectgaps=True,
            )
        )

        fig.add_vrect(
            x0=self.forecast.component.capacity_planning.to_dt,
            x1=self.forecast.component.capacity_planning.forecast_to_dt,
            annotation_text=f"{self.forecast.component.capacity_planning.forecast_days} days forecast",
            annotation_position="top left",
            opacity=0.1,
            line_width=0.5,
            fillcolor="grey",
        )

        line_styles = {"soft": "longdash", "hard": "dash"}
        for slo in self.forecast.component.saturation_point.slos:
            if line_styles.get(slo.name) is None:
                # Exclude SLOs not explicitly included in line_styles,
                # i.e. we exclude the "saturation" SLO from plotting
                continue

            fig.add_trace(
                go.Scatter(
                    x=self.forecast.series["ds"],
                    y=self.forecast.series[f"{slo.name}_threshold"],
                    name=slo.name,
                    showlegend=False,
                    line=dict(color="firebrick", width=2, dash=line_styles[slo.name]),
                )
            )
            fig.add_annotation(
                x=0,
                y=slo.threshold,
                xref="paper",
                showarrow=False,
                text=f" <b>{slo.name} SLO</b> ",
                font=dict(size=10, color="firebrick"),
                arrowcolor="firebrick",
                bgcolor="white",
            )

        fig.update_layout(
            title=dict(
                text=title,
                yanchor="top",
                xanchor="left",
                font=dict(size=24),
                automargin=True,
            ),
            xaxis_title="Date",
            yaxis_title="Utilization (%s)"
            % (self.forecast.component.capacity_planning.strategy),
            margin_t=100,
            margin_l=20,
            margin_r=20,
            showlegend=True,
            legend_title_text="Legend",
        )

        self._configure_yaxes(fig)

        fig.update_xaxes(
            fixedrange=True,
            rangeslider=dict(visible=False),
            rangeselector=dict(visible=False),
            autorange=False,
            range=list(self.forecast.component.capacity_planning.total_range),
        )

        fig.write_image(fp, format=format)
        pyplot.close()  # Closes the latest figure

    def _configure_yaxes(self, fig):
        # If the forecast exceeds 100%, limit the yaxis max to 100%
        # otherwise, just limit to non-negative values to give a better
        # zoomed-in view of low saturation values
        if len(self.forecast.series.query("yhat_upper > 1").index) > 0:
            bottom_y = math.floor(self.forecast.series["yhat"].min() * 10) / 10
            bottom_y = max(bottom_y, 0)

            fig.update_yaxes(tickformat=",.0%", range=[bottom_y, 1])
        else:
            fig.update_yaxes(tickformat=",.0%", rangemode="nonnegative")
