from __future__ import annotations

import datetime
import json
from datetime import timedelta
from typing import cast

import pandas as pd
from pandas import DataFrame
from prometheus_client.parser import _parse_labels

from tamland.core import forecasting
from tamland.core import prom_cache
from tamland.core import prom_provider
from tamland.core.logger import logger
from tamland.core.prom_cache import default_step, only_cache

max_query_retries = 5
query_timeout = timedelta(minutes=2)

# Fetch Prometheus data in chunks not bigger than this
chunk_size = timedelta(days=1)
prometheus_provider = prom_provider.factory()

query_ui_default_tab = 0  # 0: graph, 1: table
query_ui_default_range_input = "6h"


def format_query(query):
    return " ".join(list(map(lambda x: x.strip(), query.split("\n"))))


def format_date(dt):
    return dt.isoformat()


def query_range_with_retry(query, from_dt, to_dt):
    if only_cache:
        raise Exception(
            f"TAMLAND_ONLY_CACHE is set but tried to fetch {query}@{from_dt}:{to_dt} from Thanos (cache end time: {forecasting.cache_end_time()})"
        )

    count = max_query_retries
    while True:
        try:
            logger.debug(
                "query_range_with_retry - (%s to %s): %s",
                format_date(from_dt),
                format_date(to_dt),
                format_query(query),
            )
            return prometheus_provider.prometheus().query_range(
                query, from_dt, to_dt, default_step, timeout=query_timeout
            )
        except Exception as e:
            count = count - 1
            if count <= 0:
                raise e
            else:
                logger.exception(e)
                logger.warning(
                    "Prometheus query_range failed. Retrying: %s", format_query(query)
                )


def query_range_with_cache(query, from_dt, to_dt, ignore_cache=False):
    if ignore_cache:
        logger.debug("query_range_with_cache (cache ignored)")
        return query_range_with_retry(query, from_dt, to_dt)

    prom_query = prom_cache.PromQuery(query=query, from_dt=from_dt, to_dt=to_dt)
    df = prom_cache.cache.read(prom_query)
    if df is None:
        logger.debug("query_range_with_cache (cache missed)")
        df = query_range_with_retry(query, from_dt, to_dt)
        prom_cache.cache.write(prom_query, df)
        return df

    logger.debug(
        "query_range_with_cache (cache hit - %s entries) - (%s to %s): %s",
        df.shape[0],
        format_date(from_dt),
        format_date(to_dt),
        format_query(query),
    )
    return df


# Break long-terms queries down into chunked queries
# but return all the data in a single dataframe
def batched_query_range(query, from_dt, to_dt) -> DataFrame:
    def sort_df_by_date(df: DataFrame) -> DataFrame:
        return df.sort_index()

    # This is currently only used in tests where we pass around dates for convenience
    # the comparison below requires a time. This ensures we always work between
    # midnight of 2 dates
    if isinstance(from_dt, datetime.date):
        from_dt = datetime.datetime.combine(from_dt, datetime.time.min)
    if isinstance(to_dt, datetime.date):
        to_dt = datetime.datetime.combine(to_dt, datetime.time.min)

    if not from_dt < to_dt:
        raise AssertionError(f"from_dt: {from_dt} not smaller than {to_dt}")

    # We can rely on the cache for the first batch, if `to_dt` is  rolled back
    # to the beginning of the current day, for which we have data in thanos.
    # This happens in forecasting.truncate_datetime.
    # It might have been cached already by the previous run, and if it wasn't it's safe to load it.
    # If the to_date is in the future, we can't cache, because it won't be polulated yet in Thanos
    from_dt_partial = to_dt - chunk_size
    df = query_range_with_cache(
        query,
        from_dt_partial,
        to_dt - timedelta(seconds=default_step),
        ignore_cache=(to_dt > datetime.datetime.now()),
    )

    to_dt_partial = from_dt_partial
    empty_in_a_row_count = 0

    while True:
        from_dt_partial = to_dt_partial - chunk_size
        if from_dt_partial < from_dt:
            from_dt_partial = from_dt

        df_partial = query_range_with_cache(
            query,
            from_dt_partial,
            to_dt_partial - timedelta(seconds=default_step),
            ignore_cache=(to_dt_partial > datetime.datetime.now()),
        )
        df = pd.concat([df, df_partial])

        # If we fetch 3 chunks in a row with no data,
        # give up and don't try any further back
        if df_partial.empty:
            empty_in_a_row_count = empty_in_a_row_count + 1
            if empty_in_a_row_count >= 3:
                return sort_df_by_date(df)
        else:
            empty_in_a_row_count = 0

        if from_dt_partial <= from_dt:
            return sort_df_by_date(df)

        # Prepare to_dt for next iteration
        to_dt_partial = from_dt_partial


PromSelectors = dict[str, str | list[str]]


def parse_selector(selector: str) -> PromSelectors:
    """
    Parse a PromQL selector into a PromLabels dictionary
    """
    return cast(PromSelectors, _parse_labels(selector.strip("{}")))


def selector_for(dict: PromSelectors) -> str:
    """
    Return a PromQL selector for the given dictionary with labels

    A label value can either be a string or a list of string
    (which is going to translate to a regex selector).
    """
    pairs = []
    for k, v in dict.items():
        if type(v) is list:
            v = "|".join(v)
            op = "=~"
        else:
            op = "="

        pairs.append(f"{k}{op}{json.dumps(v)}")

    return ",".join(pairs)
