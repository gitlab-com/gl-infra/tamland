from __future__ import annotations

import json
import os
import re
from dataclasses import dataclass, field
from datetime import datetime, timedelta
from functools import reduce, cached_property
from itertools import chain
from typing import ClassVar, Iterator, Optional, cast, IO
from dateutil import parser as dateparser
from pydantic import BaseModel, ConfigDict, Field, model_validator

from tamland.core import prom_query, forecasting
from tamland.core.logger import logger

JSON = dict


class GrafanaDashboard(BaseModel):
    name: str
    url: str

    def __str__(self) -> str:
        return f"[{self.name}]({self.url})"


@dataclass
class Service:
    name: str
    overview_dashboard: GrafanaDashboard = Field(alias="overviewDashboard")
    resource_dashboard: dict[str, GrafanaDashboard] = Field(alias="resourceDashboard")
    events: list[Event] = field(default_factory=list)
    selectors: prom_query.PromSelectors = field(default_factory=dict)
    label: str | None = None
    owner: Team | None = None

    def __str__(self) -> str:
        return self.name


class SLO(BaseModel):
    name: str
    threshold: float


class Team(BaseModel):
    name: str
    manager: str | None = None
    label: str | None = None


@dataclass
class SaturationPoint:
    name: str
    title: str
    description: str
    severity: str
    horizontally_scalable: bool

    slos: list[SLO]
    capacity_planning: CapacityPlanningParameters

    raw_query: str | None = None
    """
    The prometheus query as given in the manifest,
    typically includes a %(selector)s template to be replaced with the selectors
    specific to a component (see Component#raw_query)
    """

    def __str__(self) -> str:
        return f"{self.name}"


@dataclass
class Reference:
    """A named link-reference"""

    title: str
    ref: str


@dataclass
class Event:
    """An annotation for a particular event in history, e.g. for a service or component"""

    date: datetime
    name: str
    references: list[Reference] = field(default_factory=list)

    def __str__(self) -> str:
        return f"Historical event on {self.date.strftime('%Y-%m-%d')}: {self.name}"


class SaturationDimension(BaseModel):
    """
    A dimension for a saturation point.
    It comes as a 2-element dict, e.g. {"selector":'shard="*"', "label":"shard=catchall"}.
    Label is optional. When not present, the selector will be used as the label.
    """

    model_config = ConfigDict(frozen=True)

    selector: str
    label: Optional[str] = None

    def __str__(self) -> str:
        return self.label or self.selector

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return str(self) == str(other)


class SaturationDimensionLookup:
    service: Service
    saturation_point: SaturationPoint

    def __init__(self, service, saturation_point):
        self.service = service
        self.saturation_point = saturation_point

    def is_disabled(self) -> bool:
        return os.getenv("TAMLAND_DISABLE_DYNAMIC_DIMENSION_LOOKUP", None) is not None

    def query(self, params: CapacityPlanningParameters) -> set[SaturationDimension]:
        if (
            params.saturation_dimension_dynamic_lookup_query is None
            or self.is_disabled()
        ):
            return params.saturation_dimensions

        from_dt, to_dt = params.history_range
        df = prom_query.batched_query_range(self._promql(params), from_dt, to_dt)
        dynamic_dimensions: list[str] = sorted(
            list(
                # stripping brackets from each PromQL selector in the data frame result,
                # e.g. ['{persistentvolumeclaim="zoekt-data-gitlab-gitlab-zoekt-0"}', ...]
                # is turned into
                # e.g.: ['persistentvolumeclaim="zoekt-data-gitlab-gitlab-zoekt-0"', ...]
                {labels.strip("{}") for labels in df.to_dict().keys()}
            )
        )

        if params.is_dynamic_dimensions_over_limit(len(dynamic_dimensions)):
            limit = params.saturation_dimension_dynamic_lookup_limit
            capped_dimensions = dynamic_dimensions[:limit]
            skipped_dimensions = set(dynamic_dimensions) - set(capped_dimensions)
            self._log_over_limit(limit, dynamic_dimensions, skipped_dimensions)
            dynamic_dimensions = capped_dimensions

        dynamic_saturation_dimensions: set[SaturationDimension] = set(
            map(lambda dim: SaturationDimension(selector=dim), dynamic_dimensions)
        )

        return params.saturation_dimensions | dynamic_saturation_dimensions

    def _promql(self, params: CapacityPlanningParameters) -> str:
        return str(params.saturation_dimension_dynamic_lookup_query) % {
            "selector": prom_query.selector_for(self.service.selectors),
        }

    def _log_over_limit(self, limit, dynamic_dimensions, skipped_dimensions):
        logger.warning(
            "Too many saturation dimensions discovered (%(total)s) for %(component)s.\n"
            "Consider increasing the limit with the parameter `saturation_dimension_dynamic_lookup_limit`, "
            "currently set to %(limit)s.\nSkipping %(skipped_len)s dimensions:\n%(skipped_dimensions)s\n"
            % {
                "component": f"{self.service}.{self.saturation_point}",
                "limit": limit,
                "total": len(dynamic_dimensions),
                "skipped_len": len(skipped_dimensions),
                "skipped_dimensions": "\n".join(sorted(skipped_dimensions)),
            }
        )


@dataclass
class Component:
    """A specific component for a service"""

    service: Service
    saturation_point: SaturationPoint
    capacity_planning: CapacityPlanningParameters
    saturation_ratio_query_template: str
    events: list[Event] = field(default_factory=list)
    dimension: Optional[SaturationDimension] = None

    @property
    def selectors(self) -> dict:
        """All Prometheus selectors (labels) relevant for this component"""
        keys = (
            dict(
                component=self.saturation_point.name,
                type=self.service.name,
            )
            | self.service.selectors
        )

        return keys

    @property
    def raw_query(self) -> str:
        """
        Returns the raw Prometheus query used for the saturation point
        after applying this component's selectors
        """
        selectors = self.selectors
        # The source metric do not include the component label.
        # It's only available as part of the recording rule.
        del selectors["component"]

        selector = prom_query.selector_for(selectors)
        if self.dimension:
            selector = selector + "," + self.dimension.selector

        return str(self.saturation_point.raw_query) % dict(selector=selector)

    @property
    def saturation_ratio_query(self) -> str:
        if self.capacity_planning.experiment_saturation_ratio_raw_query:
            return self.raw_query

        selector = prom_query.selector_for(self.selectors)
        if self.dimension:
            selector = selector + "," + self.dimension.selector

        return self.saturation_ratio_query_template % selector

    def __str__(self) -> str:
        """
        {service}.{saturation_point}.{dimension}

        Example:
        sidekiq.ruby_thread_contention.shard="catchall"
        """
        if self.dimension:
            return f"{self.service}.{self.saturation_point}.{self.dimension}"
        else:
            return f"{self.service}.{self.saturation_point}"

    @property
    def title(self) -> str:
        """
        Similar to __str__, but tailored for reporting.

        Example:
        ci-runners Service, private_runners.shard="private"
        """
        title = f"{self.service} Service, {self.saturation_point}"
        if self.dimension:
            title += f".{self.dimension}"
        return title


@dataclass
class ReportPage:
    """A page in the forecast report with a grouping of services"""

    path: str
    service_pattern: str
    title: str = ""

    @property
    def slug(self) -> str:
        return self.path.replace(".md", "")

    def includes_service(self, service: Service) -> bool:
        """Check whether the given service matches the service pattern, i.e. if it's included in this page"""
        return re.match(self.service_pattern, service.name) is not None


class MalformedManifest(Exception): ...


class ManifestParser:
    @staticmethod
    def changepoint(changepoint: str) -> datetime:
        try:
            return dateparser.parse(changepoint)
        except dateparser.ParserError:
            raise MalformedManifest(f"Invalid changepoint={changepoint}")

    @staticmethod
    def outlier(values: dict) -> Outlier:
        try:
            return Outlier(
                start=dateparser.parse(values["start"]),
                end=dateparser.parse(values["end"]),
            )
        except (dateparser.ParserError, KeyError):
            raise MalformedManifest(f"Invalid outlier={values}")


class PromDefaults(BaseModel):
    base_url: str = Field(alias="baseURL")
    service_label: str = Field(alias="serviceLabel")
    default_selectors: prom_query.PromSelectors = Field(alias="defaultSelectors")
    query_templates: dict[str, str] = Field(alias="queryTemplates")


class Defaults(BaseModel):
    prometheus: PromDefaults


class ManifestReader(BaseModel):
    """Read the Tamland manifest and provide access to its contents"""

    data: dict

    @classmethod
    def from_io(cls, io: IO) -> ManifestReader:
        """Create ManifestReader with a manifest read from the IO stream"""
        return cls(data=cast(JSON, json.load(io)))

    @classmethod
    def from_file(cls, file: str) -> ManifestReader:
        """Create ManifestReader with a manifest read from the given local file"""
        with open(file, "r") as f:
            return cls.from_io(f)

    @model_validator(mode="after")
    def validate_components(self) -> ManifestReader:
        seen: set[str] = set()
        duplicate_components: set[str] = set()
        for name in map(lambda c: str(c), self.components()):
            if name in seen:
                duplicate_components.add(name)
            seen.add(name)

        assert (
            len(duplicate_components) == 0
        ), f"Duplicate components:{set(duplicate_components)}"

        return self

    def services(self) -> Iterator[Service]:
        for service, _ in self._services_with_details():
            yield service

    def service(self, name: str) -> Service | None:
        filtered = filter(lambda s: s.name == name, self.services())

        return next(filtered, None)

    def defaults(self) -> Defaults:
        return Defaults(**self.data["defaults"])

    def pages(self) -> Iterator[ReportPage]:
        report = self.data.get("report")
        if report is None:
            raise MalformedManifest(f"Missing key: {report}")

        for page in report.get("pages", []):
            yield ReportPage(
                path=page["path"],
                title=page["title"],
                service_pattern=page["service_pattern"],
            )

    def _services_with_details(self) -> Iterator[tuple[Service, dict]]:
        for name, s in self.data["services"].items():
            details = s.get("capacityPlanning", {})
            events = list(map(self._event_from_json, details.get("events", [])))

            selectors = self.defaults().prometheus.default_selectors | details.get(
                "selectors", dict()
            )

            service = Service(
                name=name,
                selectors=selectors,
                events=events,
                overview_dashboard=GrafanaDashboard(**s.get("overviewDashboard", {})),
                resource_dashboard={
                    component: GrafanaDashboard(**val)
                    for (component, val) in s.get("resourceDashboard").items()
                },
                label=s.get("label"),
                owner=self.team(s.get("owner")),
            )
            yield service, details

    def _event_from_json(self, event_dict: JSON) -> Event:
        refs = list(
            map(
                lambda ref: Reference(title=ref.get("title"), ref=ref.get("ref")),
                event_dict.get("references", []),
            )
        )

        return Event(
            date=dateparser.parse(event_dict["date"]),
            name=event_dict["name"],
            references=refs,
        )

    def saturation_points(self) -> Iterator[tuple[SaturationPoint, list[str]]]:
        for name, details in self.data["saturationPoints"].items():
            if details["capacityPlanning"].get("strategy") == "exclude":
                # Filter out any points we'd like to exclude from capacity planning
                continue

            slos = [
                SLO(name=k, threshold=v)
                for (k, v) in details["slos"].items()
                if k in ["soft", "hard"]
            ]

            # Unconditionally append a SLO for 100% saturation
            slos.append(SLO(name="saturation", threshold=1.0))

            yield SaturationPoint(
                **{
                    "name": name,
                    "title": details.get("title", f"{name} saturation"),
                    "description": details.get("description", ""),
                    "severity": details.get("severity"),
                    "horizontally_scalable": details.get("horizontallyScalable", False),
                    "raw_query": details.get("raw_query", None),
                    "capacity_planning": CapacityPlanningParameters(
                        **details["capacityPlanning"]
                    ),
                },
                slos=slos,
            ), details["appliesTo"]

    def components(self) -> Iterator[Component]:
        services: dict[str, tuple[Service, dict]] = {
            service.name: (service, details)
            for (service, details) in self._services_with_details()
        }

        for point, service_names in self.saturation_points():
            for service_name in service_names:
                lookup = services.get(service_name)
                if lookup is None:
                    continue
                service, service_overrides = lookup
                capacity_planning = self._lookup_and_merge_params(
                    point, service_overrides
                )
                dimensions = SaturationDimensionLookup(service, point).query(
                    capacity_planning
                )
                for dimension in dimensions:
                    yield Component(
                        dimension=dimension,
                        service=service,
                        saturation_point=point,
                        capacity_planning=capacity_planning,
                        events=self._events_for_component(point, service_overrides),
                        saturation_ratio_query_template=self._saturation_ratio_query_template(
                            capacity_planning.strategy, point
                        ),
                    )
                if capacity_planning.saturation_dimensions_keep_aggregate:
                    yield Component(
                        service=service,
                        saturation_point=point,
                        capacity_planning=capacity_planning,
                        events=self._events_for_component(point, service_overrides),
                        saturation_ratio_query_template=self._saturation_ratio_query_template(
                            capacity_planning.strategy, point
                        ),
                    )

    def teams(self) -> Iterator[Team]:
        for team in self.data["teams"]:
            yield Team(**team)

    def team(self, name: str) -> Team | None:
        return self._team_map.get(name)

    @cached_property
    def _team_map(self) -> dict[str, Team]:
        return {team.name: team for team in self.teams()}

    def _saturation_ratio_query_template(
        self, strategy: str, point: SaturationPoint
    ) -> str:
        template = self.defaults().prometheus.query_templates.get(strategy)
        if template is None:
            raise MalformedManifest(
                f"missing query template for strategy={strategy}, saturation_point={str(point)}"
            )
        return template

    def component_by(self, key: str) -> Component | None:
        return self._component_index.get(key)

    @cached_property
    def _component_index(self) -> dict[str, Component]:
        return {str(c): c for c in self.components()}

    def _events_for_component(
        self, point: SaturationPoint, service_overrides: dict
    ) -> list[Event]:
        events = map(
            lambda c: c.get("events", []),
            filter(
                lambda o: o.get("name") == point.name,
                service_overrides.get("components", []),
            ),
        )

        return [self._event_from_json(e) for e in chain.from_iterable(events)]

    def _lookup_and_merge_params(
        self, point: SaturationPoint, service_overrides: dict | None
    ) -> CapacityPlanningParameters:
        if service_overrides is None:
            return point.capacity_planning

        overrides = map(
            lambda c: c.get("parameters", {}),
            filter(
                lambda o: o.get("name") == point.name,
                service_overrides.get("components", []),
            ),
        )

        params = reduce(lambda x, y: x | y, overrides, dict(point.capacity_planning))
        params["changepoints"] = list(
            map(ManifestParser.changepoint, params["changepoints"])
        )
        params["ignore_outliers"] = list(
            map(ManifestParser.outlier, params["ignore_outliers"])
        )

        # The saturation_dimensions from the service_overrides come as a list[str | dict].
        # Since set(list[str | dict]) isn't hashable, we have to validate and convert them:
        service_dimensions = set(
            map(
                SaturationDimension.model_validate,
                service_overrides.get("saturation_dimensions", []),
            )
        )

        params["saturation_dimensions"] = (
            params["saturation_dimensions"] | service_dimensions
        )

        params["saturation_dimensions_keep_aggregate"] = service_overrides.get(
            "saturation_dimensions_keep_aggregate", True
        )

        return CapacityPlanningParameters(**params)


@dataclass
class Outlier:
    """A model to represent an outlier from capacity planning"""

    start: datetime
    end: datetime


class CapacityPlanningParameters(BaseModel):
    """A model to hold parameters for capacity planning for a specific component"""

    FORECAST_DAYS_DEFAULT: ClassVar[int] = 90
    HISTORICAL_DAYS_DEFAULT: ClassVar[int] = 365
    CHANGEPOINTS_COUNT_DEFAULT: ClassVar[int] = 25

    CHANGEPOINT_RANGE_DEFAULT: ClassVar[float] = 0.98
    """Consider the first CHANGEPOINT_RANGE_DEFAULT percentage of history for changepoints"""

    SATURATION_DIMENSION_DYNAMIC_LOOKUP_LIMIT: ClassVar[int] = 10

    strategy: str

    forecast_days: int = FORECAST_DAYS_DEFAULT
    """Forecast horizon, predict this many days into the future"""

    historical_days: int = HISTORICAL_DAYS_DEFAULT
    """Timeframe for historical data to base the forecast on"""

    changepoints_count: int = CHANGEPOINTS_COUNT_DEFAULT
    """Number of changepoints to add to forecast"""

    changepoints: list[datetime] = field(default_factory=list)
    """Manually specified changepoints"""

    changepoint_range: float = CHANGEPOINT_RANGE_DEFAULT
    """Distribute changepoints a cross the first changepoint_range % of history automatically"""

    to_dt: datetime = field(default_factory=forecasting.find_now)
    """The notion of 'now' for this forecast. This is where history ends and the forecast begins."""

    ignore_outliers: list[Outlier] = field(default_factory=list)
    """Manually specified ignored outliers"""

    saturation_dimensions: set[SaturationDimension] = field(default_factory=set)
    """Manually specified saturation dimensions as PromQL selectors"""

    saturation_dimensions_keep_aggregate: bool = True
    """Flag to indicate if saturation_dimensions aggregated component should be kept"""

    saturation_dimension_dynamic_lookup_query: str | None = None
    """A PromQL query template to dynamically lookup labels and use as saturation dimensions"""

    saturation_dimension_dynamic_lookup_limit: int = (
        SATURATION_DIMENSION_DYNAMIC_LOOKUP_LIMIT
    )
    """A limit to the number of dynamically looked up labels to use as saturation dimensions, where 0 means unlimited"""

    experiment_saturation_ratio_raw_query: bool = False
    """Enable the component to forecast using the raw PromQL query"""

    daily_seasonality: bool | str = True
    """Allow Prophet to take into account daily trends when forecasting: 'auto', True, or False"""

    weekly_seasonality: bool | str = True
    """Allow Prophet to take into account weekly trends when forecasting: 'auto', True, or False"""

    yearly_seasonality: bool | str = False
    """Allow Prophet to take into account yearly trends when forecasting: 'auto', True, or False"""

    @property
    def from_dt(self) -> datetime:
        return self.to_dt - timedelta(days=self.historical_days)

    @property
    def forecast_to_dt(self) -> datetime:
        return self.to_dt + timedelta(days=self.forecast_days)

    @property
    def history_range(self) -> tuple[datetime, datetime]:
        return self.from_dt, self.to_dt

    @property
    def total_range(self) -> tuple[datetime, datetime]:
        return self.from_dt, self.forecast_to_dt

    def __init__(self, **data: dict):
        super().__init__(**data)

        # Instantiate but prefer settings from environment variables, if those are set
        self.forecast_days = self._int_from_env(
            "TAMLAND_FORECAST_DAYS", default=self.forecast_days
        )
        self.historical_days = self._int_from_env(
            "TAMLAND_HISTORICAL_DAYS", default=self.historical_days
        )

    @staticmethod
    def _int_from_env(key: str, *, default: int) -> int:
        return int(os.getenv(key, str(default)))

    def is_dynamic_dimensions_over_limit(self, size) -> bool:
        return (
            self.saturation_dimension_dynamic_lookup_limit > 0
            and size > self.saturation_dimension_dynamic_lookup_limit
        )
