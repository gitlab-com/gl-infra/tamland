from __future__ import annotations
import os
from dataclasses import dataclass
from datetime import datetime
from hashlib import sha256
from io import BytesIO
from typing import Protocol

import pandas as pd
from fs import open_fs
from fs.base import FS
from pandas import DataFrame, Series

from tamland.core.logger import logger
from tamland.core.util import relative_path

default_cache_dir = os.environ.get("THANOS_QUERY_CACHE_DIR", relative_path("data/"))
default_cache_fs = os.environ.get("PROM_QUERY_CACHE_FS", "osfs://" + default_cache_dir)
second_level_cache_fs = (
    os.environ.get("PROM_QUERY_SECOND_CACHE_FS", None) or None
)  # treat empty string as None

default_step = 3600
instant_series_column_name = "Instant"
only_cache = os.environ.get("TAMLAND_ONLY_CACHE", "0") == "1"


def _init_cache() -> PromCacheManager:
    """
    Initialize cache instance for global use
    Defaults to using a single local-disk cache in `default_cache_dir`
    """
    cache = MultiLevelCache()
    cache.add(FSPromCacheManager(fs=open_fs(default_cache_fs, create=True)))

    if second_level_cache_fs is not None:
        cache.add(FSPromCacheManager(fs=open_fs(second_level_cache_fs, create=True)))

    return cache


@dataclass
class PromQuery:
    """Prometheus query with a time range"""

    query: str
    from_dt: datetime
    to_dt: datetime

    def is_instant(self) -> bool:
        return self.from_dt == self.to_dt

    def __str__(self) -> str:
        return f"{self.query} [{self.from_dt}, {self.to_dt}]"


class PromCacheManager(Protocol):
    def read(self, query: PromQuery) -> DataFrame | Series | None: ...

    def write(self, query: PromQuery, df: DataFrame) -> None: ...


class MultiLevelCache(PromCacheManager):
    """Combines multiple PromCacheManagers into a hierarchy of caches with support for backfills"""

    def __init__(self) -> None:
        self.caches: list[PromCacheManager] = []

    def add(self, cache: PromCacheManager):
        self.caches.append(cache)

    def read(self, query: PromQuery) -> DataFrame | Series | None:
        """
        Go through the hierarchy and return the first cache hit if any.

        Caches will be backfilled as part of this operation and based off a higher-level cache hit.
        This means there can be writes as part of this read operation.
        """
        result = None
        backfill_opportunities = []

        for cache in self.caches:
            logger.debug(f"MultiLevelCache: Read from {cache}")
            result = cache.read(query)

            if result is None:
                # This level cache does not have data,
                # let's backfill it
                backfill_opportunities.append(cache)
            else:
                break

        if result is not None:
            for cache in backfill_opportunities:
                logger.debug(f"MultiLevelCache: Backfilling cache {cache} for {query}")
                cache.write(query, result)

        return result

    def write(self, query: PromQuery, df: DataFrame) -> None:
        """Cache the given data for the given query, across all levels of caching"""
        for cache in self.caches:
            cache.write(query, df)


# PromCacheManager manages the cache for prometheus queries. The data are pandas
# Dataframe, stored under Apache parquet format files. They are partitioned by
# query and day.
# The prometheus data from 2022-03-01 to 2022-03-10 generates the following
# folder structure:
# ./data/abcdef/              # abcdef is the sha256 hash of the query
#    |_ _query                # the original query
#    |_ 2022-03-01_step3600   # Parquet files, one file is equivalent to one day of data
#    |_ 2022-03-02_step3600
#    |_ 2022-03-03_step3600
#    |_ 2022-03-03_step3600_instant # Extra suffix for instant vectors
#    ...
#
# This cache manager collaborates very well with day range. Cache is considered
# to be "hit" when there are cache files for all days between the input from_dt
# and to_dt. If any day in between doesn't have a cache file, the whole range is
# considered to be Cache "missed". The caller is responsible for writing the
# cache when cache misses.
class FSPromCacheManager(PromCacheManager):
    """PyFilesystem based PromCacheManager"""

    def __init__(self, *, fs: FS | None = None) -> None:
        if fs is None:
            fs = open_fs(default_cache_fs, create=True)

        self.fs = fs
        self.step = default_step

    def _prepare(self, query: PromQuery) -> FS:
        path = self._hash_query(query)
        self.fs.makedirs(path, recreate=True)
        return self.fs.opendir(path)

    def _hash_query(self, query: PromQuery) -> str:
        m = sha256()
        m.update(query.query.encode("utf-8").strip())
        return m.hexdigest()

    def read(self, query: PromQuery) -> DataFrame | Series | None:
        query_fs = self._prepare(query)

        if query.is_instant():
            # This returns a Series
            return self._read_day_cache(query, query_fs, query.to_dt)

        df = pd.DataFrame()
        for day in pd.date_range(query.from_dt, query.to_dt):
            day_df = self._read_day_cache(query, query_fs, day)
            if day_df is None:
                return None
            df = pd.concat([df, day_df])
        return df

    def write(self, query: PromQuery, df: DataFrame) -> None:
        query_fs = self._prepare(query)

        # query for future reference
        with query_fs.open("_query", "w") as f:
            f.write(query.query)

        if df is None:
            logger.warn(
                f"Unexpected None value for DataFrame on cache write for query {query}"
            )
            return

        logger.debug(f"prom_cache_manager write %d entries to {query_fs}", df.shape[0])

        # Empty dataframe's index is unclear. We still need to write cache though
        if df.empty:
            for day in pd.date_range(query.from_dt, query.to_dt):
                self._save_to_parquet(query, query_fs, pd.DataFrame(), day)
            return

        df.sort_index()
        if query.is_instant():
            self._save_to_parquet(
                query,
                query_fs,
                df.to_frame(name=instant_series_column_name),
                query.from_dt,
            )
        else:
            for day, day_df in df.groupby(df.index.floor("d")):
                self._save_to_parquet(query, query_fs, day_df, day)

    def _read_day_cache(
        self, query: PromQuery, query_fs: FS, day: datetime
    ) -> DataFrame | Series | None:
        cache_path = self._day_cache_path(query, day)

        if query_fs.exists(cache_path):
            logger.debug(f"prom_cache_manager hit read from {query_fs}")
            with query_fs.open(cache_path, "rb") as fp:
                result = pd.read_parquet(BytesIO(fp.read()))

            if query.is_instant() and not result.empty:
                return result[instant_series_column_name]
            else:
                return result

        return None

    def _day_cache_path(self, query: PromQuery, day):
        instant_suffix = "_instant" if query.is_instant() else ""
        cache_file = "{day}_step{step}{instant_suffix}".format(
            day=day.strftime("%Y-%m-%d"), step=self.step, instant_suffix=instant_suffix
        )
        return cache_file

    def _save_to_parquet(self, query: PromQuery, query_fs: FS, df, day):
        with query_fs.open(self._day_cache_path(query, day), "wb") as fp:
            df.to_parquet(fp)

    def __str__(self) -> str:
        return f"FSPromCacheManager[{self.fs}]"


cache = _init_cache()
