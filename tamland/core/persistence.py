from __future__ import annotations

import gzip
import os
import uuid as uuid
import warnings
from contextlib import contextmanager
from dataclasses import field
from datetime import datetime
from itertools import chain
from pathlib import Path
from typing import AnyStr, IO, Generator
from tamland.core import executors
from tamland.core.executors import ExecutorProvider

import pandas as pd
from fs import path
from fs.base import FS
from fs.path import join
from prophet.serialize import model_to_json
from pydantic import BaseModel, UUID1, ValidationError
from pydantic_yaml import to_yaml_str, parse_yaml_raw_as
import concurrent

from tamland.core.forecaster import (
    Forecaster,
    ComponentForecast,
    Plotter,
    PredictedSLOViolation,
    ComponentForecastMetrics,
)
from tamland.core.logger import logger
from tamland.core.models import Component


class ComponentForecastOnDisk(BaseModel):
    """Forecast data we aim to persist"""

    service: str
    saturation_point: str
    forecast_date: datetime
    slo_violations: list[PredictedSLOViolation]
    created_at: datetime = datetime.utcnow()
    metrics: ComponentForecastMetrics = field(default_factory=ComponentForecastMetrics)

    def earliest_slo_violation(self) -> datetime | None:
        """Returns the earliest PredictedSLOViolation by its respective prediction date"""
        return find_earliest_slo_violation(self.slo_violations)

    def predicted_violations(self) -> dict[str, PredictedSLOViolation]:
        """Return a lookup for SLO violations by their respective SLO name, sorted by SLO threshold asc"""
        return {
            v.slo.name: v
            for v in sorted(self.slo_violations, key=lambda v: v.slo.threshold)
            if v.predicts_violation()
        }


class HighLevelComponentMetrics(BaseModel):
    """High level metrics for a component, flattened out for ease of access, possibly through a DataFrame"""

    component: str
    confidence_range: float | None
    earliest_slo_violation: datetime | None


class ForecastRun(BaseModel):
    """A top-level view on a forecast run which includes results for many components"""

    started_at: datetime = datetime.utcnow()
    finished_at: datetime | None = None

    uuid: UUID1 = uuid.uuid1()
    """Unique id for this run"""

    components: dict[str, str] = field(default_factory=dict)
    """Dictionary to map a component name to the path for its forecast"""

    metrics: list[HighLevelComponentMetrics] = field(default_factory=list)
    """High level metrics, best consumed and analyzed through a DataFrame (see #metrics_as_dataframe())"""

    @property
    def directory_name(self) -> str:
        return "%s-%s" % (self.started_at.strftime("%Y%m%d-%H%M%S"), self.uuid)

    def metrics_as_dataframe(self) -> pd.DataFrame:
        return pd.DataFrame(data=map(lambda x: dict(x), self.metrics))

    def __str__(self) -> str:
        return self.directory_name


class ForecastRunner:
    """Exposes functionality to execute forecasting for components and persist results"""

    def __init__(
        self, fs: FS, executor_provider: ExecutorProvider = executors.factory()
    ) -> None:
        self.fs = fs
        self.executor_provider = executor_provider

    @staticmethod
    def run_forecast_for_component(component: Component) -> ComponentForecast | None:
        return Forecaster(component).generate()

    def forecast_and_persist(
        self,
        components: list[Component],
        *,
        manifest_path: str,
    ) -> ForecastRun:
        """Execute forecasting and persist results in given filesystem"""

        run = ForecastRun()

        forecasting_executor = self.executor_provider.forecasting_executor()
        writing_executor = self.executor_provider.writing_executor()

        forecasting_futures = [
            forecasting_executor.submit(ForecastRunner.run_forecast_for_component, c)
            for c in components
        ]

        writing_futures = []
        for forecasting_future in concurrent.futures.as_completed(forecasting_futures):
            forecast = forecasting_future.result()
            if forecast is None:
                continue

            writing_futures.append(
                writing_executor.submit(self._save_and_append, run, forecast)
            )

        forecasting_executor.shutdown(wait=True)
        writing_executor.shutdown(wait=True)

        run.finished_at = datetime.utcnow()

        artifacts = {}
        with open(manifest_path, "r") as fp:
            artifacts["manifest.json"] = fp.read()

        RunPersistence(fs=self.fs).save_run(run, artifacts)

        return run

    def _save_and_append(self, run: ForecastRun, forecast: ComponentForecast) -> None:
        persistence = ForecastPersistence(fs=self.fs)
        path = persistence.save(run, forecast)

        component = str(forecast.component)
        run.components[component] = path

        run.metrics.append(
            HighLevelComponentMetrics(
                component=component,
                confidence_range=forecast.metrics.confidence_range,
                earliest_slo_violation=find_earliest_slo_violation(
                    forecast.slo_violations
                ),
            )
        )


def find_earliest_slo_violation(
    slo_violations: list[PredictedSLOViolation],
) -> datetime | None:
    """Given a list of predicted SLO violations, return the earliest violation date if any or otherwise None"""

    dates = list(
        chain.from_iterable(
            [
                filter(None, [slo_violation.yhat_date, slo_violation.yhat_upper_date])
                for slo_violation in slo_violations
            ]
        )
    )

    if len(dates) == 0:
        return None
    else:
        return min(dates)


MANIFEST_PATH = "/forecasts/manifests"
LATEST_RUN_PATH = "/forecasts/manifests/latest"
RUN_MANIFEST_FILENAME = "run-manifest.yml"
COMPONENTS_PATH = "/forecasts/components"


class RunPersistence:
    """Persistence layer for ForecastRun information"""

    def __init__(self, fs: FS) -> None:
        self._fs = fs

    def save_run(self, run: ForecastRun, artifacts: dict[str, AnyStr]) -> str:
        """Persist a ForecastRun into a YAML file, alongside with artifacts (if any)"""

        manifest_dir = os.path.join(MANIFEST_PATH, run.directory_name)
        self._fs.makedirs(manifest_dir, recreate=True)

        manifest_path = os.path.join(manifest_dir, RUN_MANIFEST_FILENAME)
        with self._fs.open(manifest_path, "w") as fp:
            fp.write(to_yaml_str(run))

        # Store artifacts
        for artifact, content in artifacts.items():
            with self._fs.open(os.path.join(manifest_dir, artifact), "w") as fp:
                fp.write(content)

        abspath = path.abspath(manifest_path)

        # Create/overwrite existing pointer to the latest forecast
        # (assuming the one at hand is the latest we have)
        with self._fs.open(LATEST_RUN_PATH, "w") as fp:
            fp.write(abspath)

        return abspath

    def list_runs(self) -> list[str]:
        """List paths to all ForecastRun instances persisted"""

        # PyFilesystem oddity - glob("/forecasts/manifests/*/something.yml") will also scan e.g. /cache
        # To work around this, we open the manifests directory explicitly and reconstruct the absolute path
        dir = self._fs.opendir(MANIFEST_PATH)
        return [
            str(
                Path(MANIFEST_PATH)
                / Path(match.path).relative_to(Path(match.path).anchor)
            )
            for match in dir.glob(f"*/{RUN_MANIFEST_FILENAME}")
        ]

    def latest_run(self) -> ForecastRun | None:
        """Get the latest ForecastRun (based on information from the persisted pointer to the latest forecast)"""
        if not self._fs.isfile(LATEST_RUN_PATH):
            return None

        with self._fs.open(LATEST_RUN_PATH, "r") as fp:
            path = fp.read().strip()

        return self.read_run(path)

    def read_run(self, manifest_path: str) -> ForecastRun:
        """Reconstruct ForecastRun based on persisted data"""
        with self._fs.open(manifest_path, "r") as fp:
            return parse_yaml_raw_as(ForecastRun, fp.read())

    @contextmanager
    def open_artifact(
        self, run: ForecastRun, artifact_name: str
    ) -> Generator[IO, None, None]:
        """Open named artifact data of the given run"""
        run_dir = os.path.join(MANIFEST_PATH, run.directory_name)
        with self._fs.open(join(run_dir, artifact_name), "r") as fp:
            yield fp


class ForecastPersistence:
    """Persist the resulting forecast along with generated plots"""

    def __init__(self, fs: FS) -> None:
        self._fs = fs

    def _component_to_directory_name(
        self, run: ForecastRun, component: Component
    ) -> str:
        if component.dimension:
            component_dir = os.path.join(
                component.saturation_point.name, str(component.dimension)
            )
        else:
            component_dir = component.saturation_point.name

        return os.path.join(
            COMPONENTS_PATH,
            component.service.name,
            component_dir,
            run.directory_name,
        )

    def read(
        self, run: ForecastRun, component: Component
    ) -> ComponentForecastOnDisk | None:
        """
        Read the forecast for the given component from storage and reconstruct its ComponentForecastOnDisk

        This returns None (and logs a warning) if the forecast data does not exist or is invalid.
        """
        forecast_dir = self._component_to_directory_name(run, component)
        forecast_file = os.path.join(forecast_dir, "forecast.yml")

        if not self._fs.isfile(forecast_file):
            logger.debug(
                f"Forecast {forecast_file} requested, but not found in storage"
            )
            return None

        try:
            with self._fs.open(forecast_file) as fp:
                return parse_yaml_raw_as(ComponentForecastOnDisk, fp.read())
        except ValidationError as e:
            logger.warn(
                f"Forecast {forecast_file} requested, pydantic threw a ValidationError: {e}"
            )
            return None

    def save(self, run: ForecastRun, forecast: ComponentForecast) -> str:
        """Save given forecast to a directory. It'll use a notion of time as a subdirectory to store the data in."""
        try:
            forecast_dir = self._component_to_directory_name(run, forecast.component)

            self._fs.makedirs(forecast_dir, recreate=True)

            logger.info(f"Saving forecast for {forecast.component} to {forecast_dir}")

            self._serialize_forecast(forecast, forecast_dir)

            with warnings.catch_warnings():
                warnings.filterwarnings(
                    action="ignore",
                    message=".*to_pydatetime is deprecated, in a future version.*",
                    category=FutureWarning,
                )
                self._save_plots(run, forecast)

            return forecast_dir
        except OSError as e:
            logger.error(
                f"Forecast failed to persist {e}\n"
                "It could be due to file system's limit on filename size. "
                "See https://en.wikipedia.org/wiki/Comparison_of_file_systems#Limits.\n"
                f"Consider labeling the component's dimension:\n{forecast.component}\n"
            )
            raise e

    @contextmanager
    def open_artifact(
        self, run: ForecastRun, component: Component, artifact_name: str
    ) -> Generator[IO | None, None, None]:
        """
        Open named artifact data of the given component forecast

        Yields None if the artifact file does not exist,
        otherwise a file pointer to read the artifact
        """
        forecast_dir = self._component_to_directory_name(run, component)
        artifact_file = join(forecast_dir, artifact_name)

        if not self._fs.isfile(artifact_file):
            logger.warning(
                f"Artifact {artifact_file} requested, but not found in storage"
            )
            yield None
        else:
            with self._fs.open(artifact_file, "rb") as fp:
                yield fp

    @contextmanager
    def save_artifact(
        self, run: ForecastRun, component: Component, artifact_name: str
    ) -> Generator[IO, None, None]:
        forecast_dir = self._component_to_directory_name(run, component)
        self._fs.makedirs(forecast_dir, recreate=True)

        with self._fs.open(join(forecast_dir, artifact_name), "wb") as fp:
            yield fp

    def _serialize_forecast(
        self, forecast: ComponentForecast, forecast_dir: str
    ) -> None:
        # Store full prophet model
        with self._fs.open(
            os.path.join(forecast_dir, "prophet-model.json.gz"), "wb"
        ) as fp:
            fp.write(gzip.compress(model_to_json(forecast.model).encode()))

        # Store forecast on disk
        fod = ComponentForecastOnDisk(
            service=forecast.component.service.name,
            saturation_point=forecast.component.saturation_point.name,
            slo_violations=forecast.slo_violations,
            forecast_date=forecast.component.capacity_planning.to_dt,
            metrics=forecast.metrics,
        )
        with self._fs.open(os.path.join(forecast_dir, "forecast.yml"), "w") as fp:
            fp.write(to_yaml_str(fod))

        with self._fs.open(os.path.join(forecast_dir, "history.parquet"), "wb") as fp:
            forecast.history.series.to_parquet(fp)

    def _save_plots(self, run: ForecastRun, forecast: ComponentForecast) -> None:
        plotter = Plotter(forecast)
        component = forecast.component

        with self.save_artifact(run, component, "forecast.png") as fp:
            plotter.plot(fp)

        with self.save_artifact(run, component, "forecast_debug.png") as fp:
            plotter.plot_debug(fp)

        with self.save_artifact(run, component, "forecast_debug_wide.png") as fp:
            plotter.plot_debug(fp, figsize=(3000, 800))

        with self.save_artifact(run, component, "forecast_debug.svg") as fp:
            plotter.plot_debug(fp, format="svg")

        with self.save_artifact(run, component, "forecast_components.png") as fp:
            plotter.plot_components().savefig(fp)
