"""A module to help dealing with feature flags consistenly across the project"""

import os


def is_enabled(flag: str, default_value: bool = False) -> bool:
    value = os.environ.get(f"FEATURE_{flag}", str(default_value)).lower()
    return value in ["1", "true", "t"]
