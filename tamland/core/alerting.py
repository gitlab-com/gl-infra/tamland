from __future__ import annotations

import os
from abc import abstractmethod
from dataclasses import dataclass
from datetime import datetime, timedelta
from enum import Enum
from typing import Protocol, Tuple

from tamland.core.models import SLO
from tamland.core.persistence import ComponentForecastOnDisk

VIOLATION_HORIZON_IN_DAYS = 90
"""The violation horizon puts a limit on how for out a predicted threshold violation will be treated as one."""


class AlertingRule(Protocol):
    def evaluate(self, forecast: ComponentForecastOnDisk) -> AlertingRuleOutcome:
        """Implements logic to trigger an alert for a given forecast"""
        ...


class AbstractAlertingRule(AlertingRule):
    def evaluate(self, forecast: ComponentForecastOnDisk) -> AlertingRuleOutcome:
        triggered, slo, description = self.violates_slo(forecast)

        return AlertingRuleOutcome(
            forecast=forecast,
            triggered=triggered,
            slo=slo,
            description=description or str(self.__doc__),
            rule=type(self),
        )

    @abstractmethod
    def violates_slo(
        self, forecast: ComponentForecastOnDisk
    ) -> Tuple[bool, SLO | None, str | None]: ...


class AlertingRule_MeanPrediction(AbstractAlertingRule):
    def violates_slo(
        self, forecast: ComponentForecastOnDisk
    ) -> Tuple[bool, SLO | None, str | None]:
        for violation in sorted(
            forecast.slo_violations, key=lambda v: v.slo.threshold, reverse=True
        ):
            if violation.yhat_date is None:
                continue

            if violation.yhat_date <= datetime.utcnow() + timedelta(
                days=VIOLATION_HORIZON_IN_DAYS
            ):
                return (
                    True,
                    violation.slo,
                    f"Predicted mean violation within {VIOLATION_HORIZON_IN_DAYS} days for {violation.slo}",
                )

        return (
            False,
            None,
            f"No violations predicted or beyond alerting horizon ({VIOLATION_HORIZON_IN_DAYS} days)",
        )


class AlertingRule_ConfidenceIntervalPrediction(AbstractAlertingRule):
    def violates_slo(
        self, forecast: ComponentForecastOnDisk
    ) -> Tuple[bool, SLO | None, str | None]:
        max_confidence_range = float(os.getenv("TAMLAND_MAX_CONFIDENCE_RANGE", "1"))

        for violation in sorted(
            forecast.slo_violations, key=lambda v: v.slo.threshold, reverse=True
        ):
            if violation.yhat_upper_date is None:
                continue

            if violation.yhat_upper_date > datetime.utcnow() + timedelta(
                days=VIOLATION_HORIZON_IN_DAYS
            ):
                # Outside of alerting horizon
                continue

            if (forecast.metrics.confidence_range or 0.0) < max_confidence_range:
                return (
                    True,
                    violation.slo,
                    f"Predicted violation based on confidence interval with "
                    f"confidence_range={forecast.metrics.confidence_range} and within {VIOLATION_HORIZON_IN_DAYS} days",
                )

        return (
            False,
            None,
            f"No violations predicted, not confident enough (> {max_confidence_range}) "
            f"or beyond alerting horizon ({VIOLATION_HORIZON_IN_DAYS} days)",
        )


@dataclass
class AlertingRuleOutcome:
    """Captures the outcome of a single AlertingRule with respect to the given forecast"""

    forecast: ComponentForecastOnDisk
    triggered: bool
    description: str
    rule: type[AlertingRule]
    slo: SLO | None = None

    def __str__(self) -> str:
        return f"Rule {self.rule.__name__}: {self.description}"


class AlertDecisionMaker:
    @classmethod
    def default(cls) -> AlertDecisionMaker:
        return AlertDecisionMaker(
            AlertingRule_MeanPrediction(), AlertingRule_ConfidenceIntervalPrediction()
        )

    def __init__(self, *rules: AlertingRule):
        self.rules = rules

    def decide(self, forecast: ComponentForecastOnDisk | None) -> CapacityAlert | None:
        """Given a component forecast, evaluate rules and decide on the alert status"""

        if forecast is None:
            return None

        outcomes = [rule.evaluate(forecast) for rule in self.rules]

        triggered = any(outcome.triggered for outcome in outcomes)
        status = AlertStatus.OPEN if triggered else AlertStatus.CLOSED

        return CapacityAlert(forecast=forecast, rule_outcomes=outcomes, status=status)


class AlertStatus(Enum):
    """Status of an alert to indicate whether there is a capacity warning in place"""

    OPEN = 1
    """Alert is open, i.e. there is a capacity warning"""

    CLOSED = 0
    """Alert is closed, i.e. there is no capacity warning"""


@dataclass
class CapacityAlert:
    """Captures the alert status and outcomes of alert rule evaluation for the given forecast"""

    forecast: ComponentForecastOnDisk
    rule_outcomes: list[AlertingRuleOutcome]
    status: AlertStatus

    def summarize_outcomes(self) -> str:
        outcomes = ",".join(
            f"{o.rule.__name__}[{o.triggered}, {o.description}]"
            for o in self.rule_outcomes
        )

        return f"{self.status}: {outcomes}"
