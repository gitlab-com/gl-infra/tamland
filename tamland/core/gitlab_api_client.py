import requests
from tamland.core.logger import logger
from typing import Protocol


class GitLabAPI(Protocol):
    def get_issues(
        self, project_id: str, state: str = "opened", labels: list[str] = []
    ):
        pass

    def create_issue(self, project_id: str, labels: list[str] | None = None, **data):
        pass

    def update_issue(self, project_id: str, issue_iid: str, data: dict):
        pass

    def close_issue(self, project_id: str, issue_iid: str):
        pass

    def upload_project_file(self, project_id: str, file_object) -> dict:
        pass

    def create_issue_link(
        self,
        project_id: str,
        issue_iid: str,
        target_project_id: str,
        target_issue_iid: str,
    ):
        pass

    def create_todo_item(self, project_id: str, issue_iid: str):
        pass

    def create_note(self, project_id: str, issue_iid: str, message: str):
        pass


class GitLabAPIClient(GitLabAPI):
    """
    GitLabAPIClient class is simple, lightweight custom API wrapper for few endpoints.
    If use case become more complex, consider `python-gitlab` as a dependency instead.
    """

    def __init__(self, base_url, private_token):
        self.base_url = base_url
        self.headers = {"Private-Token": private_token}
        self.default_timeout = 60

    # https://docs.gitlab.com/ee/api/issues.html#list-project-issues
    def get_issues(self, project_id, state="opened", labels: list[str] = []):
        """
        List issues in a project, optionally with filters for state and labels

        The label filtering is positive in the sense that when labels are given,
        we'll filter for issues with those labels. If the list of labels is empty,
        the filter is disabled, i.e. it's not possible to filter for issues without labels.
        """
        url = f"{self.base_url}/projects/{project_id}/issues"
        params = {"state": state, "per_page": 100}
        if len(labels) > 0:
            params["labels"] = ",".join(labels)

        logger.info(f"Listing Issues url={url} data={params}")

        response = requests.get(
            url, params=params, headers=self.headers, timeout=self.default_timeout
        )
        response.raise_for_status()
        if not response.headers.get("X-Next-Page"):
            return response.json()
        else:  # lazy pagination
            records = []
            while True:
                records += response.json()
                next_page = response.headers.get("X-Next-Page")
                if not next_page:
                    break
                params.update({"page": next_page})
                logger.info(f"Listing Issues url={url} data={params}")
                response = requests.get(
                    url,
                    params=params,
                    headers=self.headers,
                    timeout=self.default_timeout,
                )
                response.raise_for_status()
            return records

    # https://docs.gitlab.com/ee/api/issues.html#new-issue
    def create_issue(self, project_id, labels: list[str] | None = None, **data):
        url = f"{self.base_url}/projects/{project_id}/issues"
        logger.info(f"Creating Issue url={url} data={data}")

        if labels:
            data["labels"] = ",".join(labels)

        response = requests.post(
            url, json=data, headers=self.headers, timeout=self.default_timeout
        )
        response.raise_for_status()
        return response.json()

    # https://docs.gitlab.com/ee/api/issues.html#edit-issue
    def update_issue(self, project_id, issue_iid, data):
        url = f"{self.base_url}/projects/{project_id}/issues/{issue_iid}"
        logger.info(f"Updating Issue url={url} data={data}")

        response = requests.put(
            url, json=data, headers=self.headers, timeout=self.default_timeout
        )
        response.raise_for_status()
        return response.json()

    def close_issue(self, project_id, issue_iid):
        self.update_issue(project_id, issue_iid, {"state_event": "close"})

    # https://docs.gitlab.com/ee/api/projects.html#upload-a-file
    def upload_project_file(self, project_id, file_object):
        url = f"{self.base_url}/projects/{project_id}/uploads"
        files = {"file": file_object}
        logger.info(f"Uploading Project File url={url} files={files}")

        response = requests.post(
            url, files=files, headers=self.headers, timeout=self.default_timeout
        )
        response.raise_for_status()
        return response.json()

    # https://docs.gitlab.com/ee/api/issue_links.html#create-an-issue-link
    def create_issue_link(
        self, project_id, issue_iid, target_project_id, target_issue_iid
    ):
        url = f"{self.base_url}/projects/{project_id}/issues/{issue_iid}/links"
        data = {
            "issue_iid": issue_iid,
            "target_project_id": target_project_id,
            "target_issue_iid": target_issue_iid,
        }
        logger.info(f"Linking Issue url={url} data={data}")

        response = requests.post(
            url, json=data, headers=self.headers, timeout=self.default_timeout
        )
        if response.status_code == 409:
            return  # conflicts will occur if already linked, no worries
        else:
            response.raise_for_status()
        return response.json()

    # https://docs.gitlab.com/ee/api/issues.html#create-a-to-do-item
    def create_todo_item(self, project_id, issue_iid):
        url = f"{self.base_url}/projects/{project_id}/issues/{issue_iid}/todo"
        data = {
            "issue_iid": issue_iid,
        }
        logger.info(f"Creating a to-do item url={url} data={data}")

        response = requests.post(
            url, json=data, headers=self.headers, timeout=self.default_timeout
        )
        if response.status_code == 304:
            logger.info(
                f"To-do item already exists for the user on issue url={url} data={data}"
            )
            return
        else:
            response.raise_for_status()
        return response.json()

    # https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
    def create_note(self, project_id, issue_iid, message):
        url = f"{self.base_url}/projects/{project_id}/issues/{issue_iid}/notes"
        data = {"issue_iid": issue_iid, "body": message}
        logger.info(f"Creating an issue note url={url} data={data}")

        response = requests.post(
            url, json=data, headers=self.headers, timeout=self.default_timeout
        )
        response.raise_for_status()
        return response.json()


class DryRun(GitLabAPI):
    """
    DryRun is a null object to mimic GitLabAPIClient without hitting the real API.
    Used to experiment with Tamland locally in a safe way.
    """

    def get_issues(self, project_id, state="opened", labels: list[str] = []):
        logger.info("Completed dry run for #get_issues")
        return []

    def create_issue(self, project_id, labels: list[str] | None = None, **data):
        logger.info("Completed dry run for #create_issue")

    def update_issue(self, project_id, issue_iid, data):
        logger.info("Completed dry run for #update_issue")

    def close_issue(self, project_id, issue_iid):
        logger.info("Completed dry run for #close_issue")

    def upload_project_file(self, project_id, file_object):
        logger.info("Completed dry run for #upload_project_file")
        return {"url": "http://example.com/dry-run.svg"}

    def create_issue_link(
        self, project_id, issue_iid, target_project_id, target_issue_iid
    ):
        logger.info("Completed dry run for #create_issue_link")

    def create_todo_item(self, project_id, issue_iid):
        logger.info("Completed dry run for #create_todo_item")

    def create_note(self, project_id, issue_iid, message):
        logger.info("Completed dry run for #create_note")
