from __future__ import annotations
from concurrent.futures import Executor, ProcessPoolExecutor, ThreadPoolExecutor, Future
from typing import Callable, TypeVar, Protocol
import multiprocessing
import os


def factory() -> ExecutorProvider:
    if os.environ.get("TAMLAND_PARALLEL_FORECASTING", "0") in ["0", "false", "f"]:
        return LocalExecutorProvider()
    else:
        return ParallelExecutorProvider()


class ExecutorProvider(Protocol):
    """
    An executor provider can return executors to be used in `ForecastRunner`.
    Parts of the forecast can then be run in parallel depending on the chosen provider.
    """

    def forecasting_executor(self) -> Executor: ...

    def writing_executor(self) -> Executor: ...


T = TypeVar("T")


class LocalExecutor(Executor):
    """
    This executor runs the function in the local process or thread. Everything
    submitted to this executor is run sequentially.
    """

    def submit(self, fn: Callable[..., T], /, *args, **kwargs) -> Future[T]:
        f = Future[T]()
        f.set_result(fn(*args, **kwargs))
        return f


class LocalExecutorProvider(ExecutorProvider):
    def forecasting_executor(self) -> LocalExecutor:
        return LocalExecutor()

    def writing_executor(self) -> LocalExecutor:
        return LocalExecutor()


class ParallelExecutorProvider(ExecutorProvider):
    """
    This ExecutorProvider is to be used in `persistence.ForecastRunner` to allow
    parallelizing parts of the forecast.
    """

    def _parallel_processes(self) -> int:
        return int(
            os.environ.get(
                "TAMLAND_PARALLEL_FORECASTING_CPUS", multiprocessing.cpu_count()
            )
        )

    def forecasting_executor(self) -> Executor:
        """
        The forecasting in `ForecastRunner.forecast_and_persist` using prophet is mostly CPU bound.
        So to get around the GIL and to be able to use all cores, we use a ProcessPoolExecutor
        when executing in parallel.
        """
        return ProcessPoolExecutor(self._parallel_processes())

    def writing_executor(self) -> Executor:
        """
        The `ForecastRunner#_save_and_append` method is not threadsafe because it modifies
        the `run` object in place. We're only ever appending to a list without
        caring about the order. Or adding elements to a dictionary. We're
        counting on the GIL to make that work. This is why we're using a ThreadPoolExecutor

        In a future iteration, we could split the method up so the writing
        of forecast files happens in the separate process that does the forecasting.
        Passing around the `FS` object to a new process prevented that in
        the first iteration of parallel forecast generation.

        We're running 5 times the number of threads as we are running processes.
        This is a copy of the default behaviour in Python.
        """
        return ThreadPoolExecutor(self._parallel_processes() * 5)
