import logging
import os

logFormatter = logging.Formatter()

# Global logging config
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[logging.FileHandler("tamland.log")],
)

# Logger used by Tamland modules
logger = logging.getLogger("tamland")
logger.addHandler(logging.FileHandler("tamland.log"))
logger.setLevel(logging.getLevelName(os.getenv("LOGLEVEL", "INFO")))
