from __future__ import annotations

import logging
import os
import re
import time
from concurrent.futures import ProcessPoolExecutor, as_completed
from functools import partial
from logging import StreamHandler
from typing import Annotated

import typer
from fs import open_fs
from fs.copy import copy_dir
from progressbar import ProgressBar
from pydantic_yaml import to_yaml_str, parse_yaml_raw_as
from watchdog.events import (
    RegexMatchingEventHandler,
)
from watchdog.observers import Observer

from tamland.reporting.issue_tracker import (
    manage_capacity_warnings,
    IssueManagementConfig,
)
from tamland.core.logger import logger
from tamland.core.models import ManifestReader, Component
from tamland.core.persistence import RunPersistence, ForecastRunner, ForecastPersistence
from tamland.reporting.renderer import render_history_video
from tamland.reporting.reporting import BasicReport, ForecastRunAccessor
from tamland.core.util import relative_path


# When using the shell, let's add console logging
def _setup_shell_logger() -> None:
    stream_handler = StreamHandler()
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(logging.Formatter("%(asctime)s   %(message)s"))
    logger.addHandler(stream_handler)


_setup_shell_logger()

app = typer.Typer(add_completion=False)

ManifestOption = Annotated[str, typer.Option(help="Path to JSON manifest")]

DEFAULT_MANIFEST = relative_path("saturation.json")


def regex_filter(pattern: str, component: Component) -> bool:
    return re.match(pattern, str(component))


def set_filter(names: set[str], component: Component) -> bool:
    name = f"{component.service.name}.{component.saturation_point.name}"
    return name in names


def any_filter(*args, **kwargs) -> bool:
    return True


@app.command()
def forecast(
    target: Annotated[
        str,
        typer.Argument(
            help="Target filesystem path per PyFilesystem specs, e.g. gs://bucket/path or a local path etc."
        ),
    ],
    pattern: Annotated[
        str,
        typer.Option(
            help="Regex pattern evaluated on `service.component` or `service.component.dimension` (if any saturation dimensions),"
            " e.g. use `patroni.*\\.disk.*` to match `patroni-ci.disk_space`"
        ),
    ] = "",
    from_file: Annotated[
        str,
        typer.Option(
            help="Get a list of components from this file as a base for other filters specified (if any)"
        ),
    ] = "",
    manifest: ManifestOption = DEFAULT_MANIFEST,
):
    """Execute forecasting for saturation components, either for all or for components selected by service and/or component name"""
    reader = ManifestReader.from_file(manifest)

    name_filter = any_filter
    if from_file != "":
        with open(from_file, "r") as fp:
            name_filter = partial(set_filter, set(fp.read().split("\n")))

    components = sorted(
        filter(
            lambda c: regex_filter(pattern, c) and name_filter(c),
            reader.components(),
        ),
        key=lambda c: c.saturation_point.name,
    )

    print(f"Starting forecast for {len(components)} components:")
    for c in components:
        print(c)

    ForecastRunner(fs=open_fs(target, create=True)).forecast_and_persist(
        components, manifest_path=manifest
    )


@app.command()
def chatops(
    target: Annotated[
        str,
        typer.Argument(
            help="Target filesystem path per PyFilesystem specs, e.g. gs://bucket/path or a local path etc."
        ),
    ],
    component: Annotated[
        str,
        typer.Argument(
            help="Component name, e.g. use `patroni.*\\.disk.*` to match `patroni-ci.disk_space`"
        ),
    ],
    manifest: ManifestOption = DEFAULT_MANIFEST,
):
    """Forecast a single component and print output suitable to consume back into GitLab ChatOps"""
    reader = ManifestReader.from_file(manifest)

    comp = next(c for c in reader.components() if str(c) == component)

    def chatops_reply(content: str) -> None:
        print(f"section_start:{int(time.time())}:chat_reply\r\033[0K\n")
        print(content)
        print(f"\nsection_end:{int(time.time())}:chat_reply\r\033[0K")

    fs = open_fs(target, create=True)
    persistence = ForecastPersistence(fs=fs)

    run = ForecastRunner(fs=fs).forecast_and_persist([comp], manifest_path=manifest)
    forecast = persistence.read(run, comp)

    chatops_reply(to_yaml_str(forecast))


@app.command()
def latest(
    fs: Annotated[
        str,
        typer.Argument(
            help="Filesystem path containing forecasts per PyFilesystem specs, e.g. gs://bucket/path or a local path etc."
        ),
    ],
):
    """Print information about the latest forecast run (if any)"""
    persistence = RunPersistence(fs=open_fs(fs))
    run = persistence.latest_run()

    if run is None:
        print("No forecast runs available.")
        raise typer.Exit(code=1)

    print(f"Latest forecast: {run}")
    print(f"Started at:      {run.started_at}")
    print(f"Finished at:     {run.finished_at}")
    print(f"Duration:        {run.finished_at - run.started_at}")

    run.metrics

    df = run.metrics_as_dataframe()

    mean_confidence_range = df["confidence_range"].mean()
    print("\nStatistics:")
    print(f"Number of components:\t{len(run.components)}")
    print("Mean confidence range:\t%.3f" % mean_confidence_range)

    print("\nPredicted SLO violations:")
    for _, row in df[df["earliest_slo_violation"].notnull()].iterrows():
        print("%s: %s" % (row["component"], row["earliest_slo_violation"]))


@app.command()
def service_details(service: str, manifest: ManifestOption = DEFAULT_MANIFEST):
    """Show details for a service"""
    reader = ManifestReader.from_file(manifest)

    s = reader.service(service)

    if s is None:
        print(f"Service not found: {service}")
        raise typer.Exit(code=1)

    print(s)
    print("---------------------")
    print("Components:")
    for component in filter(lambda c: c.service.name == service, reader.components()):
        print(component)
    print("=====================")


@app.command()
def report(
    fs: Annotated[
        str,
        typer.Argument(
            help="Filesystem path containing forecasts per PyFilesystem specs, e.g. gs://bucket/path or a local path etc."
        ),
    ],
    target: Annotated[
        str,
        typer.Option(help="Target path to generate report in (local filesystem only)."),
    ] = "./_report",
    watch: Annotated[
        bool,
        typer.Option(
            help="Watch for file changes and re-generate report automatically when files are changed"
        ),
    ] = False,
    publish_to: Annotated[
        str,
        typer.Option(
            help="Publish static HTML report to given filesystem per PyFilesystem specs, e.g. s3://bucket/path"
        ),
    ] = None,
    publish_date_suffix: Annotated[
        bool,
        typer.Option(
            help="When publishing to the given publish_to location, add a directory with a date suffix, e.g. `s3://bucket/path/2023-11-01`"
        ),
    ] = True,
):
    """Generate report for latest report"""
    fs = open_fs(fs)
    run = RunPersistence(fs=fs).latest_run()
    run_accessor = ForecastRunAccessor(fs, run)

    if run is None:
        print("No forecast runs available.")
        raise typer.Exit(code=1)

    report = BasicReport(run_accessor, target_path=target)
    report.generate(clean=True)

    print(
        f"""

    In order to view the generated report in a browser, run the following code in background:
      $ cd {target} && mkdocs serve

    The rendered page will be available here:
      http://127.0.0.1:8000/
    """
    )

    if publish_to:
        # Copy the generated HTML report to the desired final destination specified by publish_to
        source_fs = open_fs(target)
        target_fs = open_fs(publish_to, create=True)
        formatted_date = run.finished_at.strftime("%Y-%m-%d")
        dst_path = f"/{formatted_date}" if publish_date_suffix else "/"

        print(f"Publishing report to {target_fs}{dst_path}")
        copy_dir(source_fs, "/report", target_fs, dst_path)

    if watch:
        handler = RegexMatchingEventHandler(
            regexes=[r"\./reporting/.*", r".*\.py$"], ignore_directories=True
        )

        handler.on_modified = lambda event: report.generate(clean=True)
        observer = Observer()
        observer.schedule(handler, ".", recursive=True)
        observer.start()

        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()


@app.command()
def manage_issues(
    fs: Annotated[
        str,
        typer.Argument(
            help="Filesystem path containing forecasts per PyFilesystem specs, e.g. gs://bucket/path or a local path etc."
        ),
    ],
    project_id: Annotated[
        int,
        typer.Option(
            help="GitLab project id of the issue tracker we'd like to manage (deprecated, specify through YAML config)"
        ),
    ] = None,
    config: Annotated[
        str,
        typer.Option(
            help="Path to issue management config (YAML), takes precedence over project_id"
        ),
    ] = None,
):
    fs = open_fs(fs)
    run = RunPersistence(fs=fs).latest_run()

    if run is None:
        print("No forecast runs available.")
        raise typer.Exit(code=1)

    if project_id and config is None:
        # Deprecated:
        # For backwards compatibility reasons, we also allow only injecting the project_id here
        # This can be removed in a major release.
        config = IssueManagementConfig(project_id=project_id)
    else:
        if config is None or not os.path.isfile(config):
            print(f"Configuration not found: {config}")
            raise typer.Exit(code=2)

        with open(config, "r") as fp:
            config = parse_yaml_raw_as(IssueManagementConfig, fp.read())

    run_accessor = ForecastRunAccessor(fs, run)
    manage_capacity_warnings(run_accessor, config)


@app.command()
def render_history(
    fs: Annotated[
        str,
        typer.Argument(
            help="Filesystem path containing forecasts per PyFilesystem specs, e.g. gs://bucket/path or a local path etc."
        ),
    ],
    worker: Annotated[int, typer.Option(help="Number of worker, starting from 1")] = 1,
    worker_count: Annotated[int, typer.Option(help="Overall number of workers")] = 1,
):
    """
    Renders history videos for all components in the latest forecast run and stores videos as artifacts

    Upon request, the function partitions the workload to support distribution across multiple workers.
    In order to distribute workload e.g. across 4 workers, call the function with:
      `--worker_count=4`
    and indicate the current worker with
      `--worker=1`.

    The current worker number `--worker` ranges from 1 to `--worker-count` (inclusive).

    By default, workload is not distributed across more than one worker.
    """
    _fs = open_fs(fs)
    run = RunPersistence(fs=_fs).latest_run()

    if run is None:
        print("No forecast runs available.")
        raise typer.Exit(code=1)

    run_accessor = ForecastRunAccessor(_fs, run)

    assert worker <= worker_count
    assert worker >= 1

    # If multiple workers are running, distribute components with forecasts
    # evenly across workers
    components_with_forecasts = [
        c for c in run_accessor.manifest().components() if run.components.get(str(c))
    ]
    components = [
        component
        for index, component in enumerate(components_with_forecasts, start=0)
        if index % worker_count == (worker - 1)
    ]

    with ProcessPoolExecutor() as executor:
        futures = [
            executor.submit(render_history_video, fs, run, component)
            for component in components
        ]

        count_scheduled = len(futures)
        count_done = 0
        logger.info("Scheduled %d rendering jobs" % count_scheduled)

        with ProgressBar(max_value=count_scheduled) as bar:
            for future in as_completed(futures):
                # retrieve the result, to check for errors
                try:
                    future.result()
                except Exception as e:
                    logger.warn(e)

                count_done += 1
                bar.update(count_done)


@app.command()
def validate_manifest(manifest: ManifestOption = DEFAULT_MANIFEST):
    """Validate the given manifest for structural correctness"""

    # A very basic implementation for now, until we'll get a JSON schema specification
    try:
        reader = ManifestReader.from_file(manifest)
        list(reader.services())
        list(reader.components())
        list(reader.pages())
        list(reader.saturation_points())
        list(reader.defaults())

        print("OK")
    except Exception as e:
        print(f"Invalid: {e}")
        raise e


@app.command()
def version():
    """Prints Tamland version"""
    version_file = relative_path("version")
    if os.path.exists(version_file):
        with open(version_file, "r") as fp:
            print(fp.read().strip())
    else:
        git_sha = (
            os.popen("/usr/bin/git rev-parse --short HEAD").read().strip()  # nosec B605
        )
        print(f"tamland-{git_sha}")


if __name__ == "__main__":
    app()
