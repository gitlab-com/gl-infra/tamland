from __future__ import annotations

import os
import re
import tempfile
from dataclasses import field, dataclass
from datetime import datetime, UTC, timedelta, timezone
from dateutil.parser import parse as parse_date
from enum import StrEnum
from functools import cached_property
from itertools import groupby
from textwrap import dedent
from typing import Protocol, cast, Any

import yaml
from jinja2 import Environment, FileSystemLoader
from pydantic import BaseModel, AwareDatetime
from pydantic_yaml import to_yaml_str

from tamland.core.alerting import AlertDecisionMaker, CapacityAlert, AlertStatus
from tamland.core.gitlab_api_client import GitLabAPI, GitLabAPIClient, DryRun
from tamland.core.logger import logger
from tamland.core.models import Component, SLO
from tamland.reporting.reporting import ForecastRunAccessor
from tamland.core.util import relative_path


def gitlab_client() -> GitLabAPI:
    if os.environ.get("APPLY_ISSUE_CHANGES") == "true":
        return GitLabAPIClient(
            os.environ.get("GITLAB_API_URL", "https://gitlab.com/api/v4"),
            os.environ.get("GITLAB_API_PRIVATE_TOKEN"),
        )
    else:
        return DryRun()


ISSUE_CLOSE_MINIMUM_AGE = 3
KEEP_OPEN_LABEL = "tamland:keep-open"


def manage_capacity_warnings(
    run_accessor: ForecastRunAccessor,
    config: IssueManagementConfig,
    client: GitLabAPI = gitlab_client(),
    decision_maker: AlertDecisionMaker = AlertDecisionMaker.default(),
):
    """Initiate issue management for the given forecast on a GitLab project"""

    tracker = IssueTracker(client, config, run_accessor)

    # Notice we iterate across all components here and don't filter on violations
    # to start with. This is so we can individually decide what to do and we're
    # also able to close issues with no warnings.
    for component in run_accessor.manifest().components():
        forecast = run_accessor.get_forecast(component)

        alert = decision_maker.decide(forecast)
        action = tracker.process(component, alert)

        action.execute(client, config.project_id)

    # Execute any cleanup actions, e.g. to close issues we stopped
    # forecasting on etc.
    for action in tracker.cleanup_actions():
        action.execute(client, config.project_id)


class IssueAction(Protocol):
    """An action to be carried out on a GitLab project's issue tracker"""

    def execute(self, client: GitLabAPI, project_id: int) -> None: ...


class CompositeAction(IssueAction):
    """Combines multiple actions into one (composite pattern)"""

    def __init__(self, *actions: IssueAction) -> None:
        self.actions = actions

    def execute(self, client: GitLabAPI, project_id: int) -> None:
        for action in self.actions:
            action.execute(client, project_id)


class NoAction(IssueAction):
    def execute(self, client: GitLabAPI, project_id: int) -> None: ...


@dataclass
class IssueContext:
    accessor: ForecastRunAccessor
    component: Component
    issue_management_config: IssueManagementConfig
    alert: CapacityAlert | None = None
    issue: Issue | None = None

    def summarize(self) -> str:
        if self.alert:
            return f"{self.component} -> {self.alert.summarize_outcomes()}"
        else:
            return f"{self.component} -> (no alert)"

    def metadata_in_place(self) -> bool:
        return bool(
            self.issue and self.issue.metadata.get("component_ident") is not None
        )


def universal_yaml_dump(o: Any | None) -> str | None:
    """
    Universal YAML dump function

    Supports dumping of
    1. pydantic objects
    3. Falls back to yaml.safe_duymp for everything else
    """
    if o is None:
        return None

    if isinstance(o, BaseModel):
        return to_yaml_str(o)
    else:
        return yaml.safe_dump(o)


def severity_label(sev: str | None) -> str | None:
    """Translates SaturationPoint severity to the correct label"""
    if sev is None:
        return None

    return "severity::%s" % sev.lstrip("s")


class WithIssueTemplatesMixin:
    def __init__(
        self,
        ctx: IssueContext,
        template_path: str = relative_path("issues"),
    ):
        self.ctx = ctx
        self.env = Environment(loader=FileSystemLoader(template_path), autoescape=True)
        self.env.filters["yaml"] = universal_yaml_dump
        self.env.filters["severity_label"] = severity_label

    def render(self, template: str, **vars) -> str:
        """Render the given template with default variables plus the explicitly passed ones"""
        tpl = self.env.get_template(f"{template}.jinja2")

        default_vars = dict(
            component=self.ctx.component,
            alert=self.ctx.alert,
            forecast=self.ctx.alert and self.ctx.alert.forecast,
            issue=self.ctx.issue,
            getenv=os.getenv,
            _metadata_in_place=self.ctx.metadata_in_place(),
        )

        return tpl.render(default_vars | vars)


class CreateIssue(IssueAction, WithIssueTemplatesMixin):
    """Creates a new issue on the tracker and only sets its title"""

    def execute(self, client: GitLabAPI, project_id: int) -> None:
        logger.info(f"Creating GitLab issue for {self.ctx.summarize()}")

        payload = client.create_issue(
            str(project_id),
            title=self.render("title"),
            description=self.render("description-initial"),
            labels=self.ctx.issue_management_config.scope_labels,
        )

        self.ctx.issue = Issue.from_payload(payload)


class UpdateIssue(IssueAction, WithIssueTemplatesMixin):
    """Updates an existing issue and sets title and description"""

    def execute(self, client: GitLabAPI, project_id: int) -> None:
        assert self.ctx.issue is not None

        logger.info(f"Updating GitLab issue for {self.ctx.summarize()}")

        self.ctx.issue.metadata["updated_at"] = str(datetime.now(tz=UTC))

        if self.ctx.issue.state == IssueState.CLOSED:
            self.ctx.issue.metadata["last_reopened_at"] = str(datetime.now(tz=UTC))

        # Transition to snake_case metadata keys (temporary fix)
        metadata = self.ctx.issue.metadata
        for key in ["updated-at", "last-reopened-at"]:
            snake_case_key = key.replace("-", "_")
            if metadata.get(key) and metadata.get(snake_case_key):
                del metadata[key]

        vars = dict(
            plot=self._upload_plot(client, project_id),
            history_video=self._upload_history_video(client, project_id),
        )
        new_labels = IssueLabeler(self.ctx).labels()

        client.update_issue(
            str(project_id),
            str(self.ctx.issue.iid),
            dict(
                title=self.render("title"),
                description=self.render("description", **vars),
                labels=new_labels,
                state_event="reopen",  # Always reopen, in case it is closed
            ),
        )

    def _upload_artifact(
        self,
        client: GitLabAPI,
        project_id: int,
        *,
        artifact_name: str,
        prefix: str,
        suffix: str,
    ) -> dict:
        with tempfile.NamedTemporaryFile(prefix=prefix, suffix=suffix) as fp:
            success = self.ctx.accessor.copy_forecast_artifact(
                self.ctx.component, artifact_name, fp.name
            )

            if success is False:
                return {}

            return client.upload_project_file(str(project_id), fp)

    def _upload_plot(self, client: GitLabAPI, project_id: int) -> dict:
        return self._upload_artifact(
            client,
            project_id,
            artifact_name="forecast_debug_wide.png",
            prefix=f"forecast_debug_wide-{self.ctx.component}",
            suffix=".png",
        )

    def _upload_history_video(self, client: GitLabAPI, project_id: int) -> dict:
        return self._upload_artifact(
            client,
            project_id,
            artifact_name="history.mp4",
            prefix=f"history-{self.ctx.component}",
            suffix=".mp4",
        )


class CloseIssue(IssueAction, WithIssueTemplatesMixin):
    """Close an existing issue"""

    def execute(self, client: GitLabAPI, project_id: int) -> None:
        assert self.ctx.issue is not None

        logger.info(f"Closing GitLab issue for {self.ctx.summarize()}")

        message = dedent(
            f"""
        Closing automatically.

        {self.ctx.summarize()}
        """
        )
        client.create_note(str(project_id), str(self.ctx.issue.iid), message)

        client.close_issue(str(project_id), str(self.ctx.issue.iid))


class CloseOrphanedIssue(IssueAction):
    """Close an orphaned issue"""

    def __init__(self, issue_iid: int) -> None:
        self.issue_iid = issue_iid

    def execute(self, client: GitLabAPI, project_id: int) -> None:
        logger.info(f"Closing orphaned GitLab issue with iid={self.issue_iid}")

        client.close_issue(str(project_id), str(self.issue_iid))


class IssueManagementConfig(BaseModel):
    """Knobs and switches to configure how to manage GitLab issues for capacity warnings"""

    project_id: int
    """The GitLab project id to use when managing issues"""

    scope_labels: list[str] = field(default_factory=list)
    """Additional labels to use to limit the search space for issues managed by us (optional)"""

    additional_labels: list[str] = field(default_factory=list)
    """Additional labels to use for issues (optional), not intended to limit the search space (use scope_labels instead)."""

    recent_issue_age_in_days: int = 30
    """The maximum age in days (since issue's closed_at date) to reopen an existing issue instead of creating a new one"""


class IssueTracker:
    def __init__(
        self,
        client: GitLabAPI,
        config: IssueManagementConfig,
        run_accessor: ForecastRunAccessor,
    ) -> None:
        self.client = client
        self.config = config
        self.accessor = run_accessor
        self.seen_issues: set[int] = set()

    @cached_property
    def all_issues(self) -> list[Issue]:
        """List of all issues in the tracker, irrespective of state"""
        issues = [
            Issue.from_payload(payload)
            for payload in self.client.get_issues(
                str(self.config.project_id),
                state="all",
                labels=self.config.scope_labels,
            )
        ]

        # Filter out issues we can't get a component ident from - these are not managed by us
        return [i for i in issues if i.component_ident is not None]

    @cached_property
    def index(self) -> dict[str | None, list[Issue]]:
        """
        A mapping of component_ident => list of Issue
        Sorted by created_at in descending order
        """
        return {
            key: list(sorted(issues, key=lambda i: i.created_at, reverse=True))
            for key, issues in groupby(
                # NB: groupby only groups consecutive items with the same key, so let's sort first!
                sorted(self.all_issues, key=lambda i: i.component_ident or ""),
                lambda i: i.component_ident,
            )
            if key is not None
        }

    def cleanup_actions(self) -> list[IssueAction]:
        """
        Returns a list of actions to be executed to clean up the issue tracker

        This is primarily intended to close any "orphaned issues", i.e. issues
        we created before but have stopped to track (e.g. because the component
        got removed from the manifest).
        """
        orphans = (
            set(
                issue.iid
                for issue in self.all_issues
                if issue.state == IssueState.OPENED
            )
            - self.seen_issues
        )

        return [CloseOrphanedIssue(orphan_iid) for orphan_iid in orphans]

    def process(self, component: Component, alert: CapacityAlert | None) -> IssueAction:
        """
        For the given Component, process the CapacityAlert

        This contains logic to decide how to react to the given situation.
        For example, we open a new issue for an open alert or update the
        existing one.

        Note that this only decide on and returns the IssueAction,
        but does not execute it.
        """
        issues: list[Issue] = self.index.get(str(component), [])

        def candidate_issues(issue: Issue) -> bool:
            """Qualify existing issues if their state is OPEN or they are recent enough"""
            age = issue.time_since_closed

            return issue.state == IssueState.OPENED or (
                age is not None and age.days < self.config.recent_issue_age_in_days
            )

        # Get most recent issue
        issue = next((i for i in filter(candidate_issues, issues)), None)

        # Mark issue as "seen"
        if issue is not None:
            self.seen_issues.add(issue.iid)

        state = (
            # Is there an issue?
            bool(issue),
            # Is there a capacity alert open?
            alert and alert.status,
            # Issue state
            issue and issue.state,
        )

        ctx = IssueContext(self.accessor, component, self.config, alert, issue)

        if issue and KEEP_OPEN_LABEL in issue.labels:
            """If the keep-open label is set, default to updating the issue"""
            return UpdateIssue(ctx)

        match state:
            case (False, AlertStatus.OPEN, _):
                return CompositeAction(CreateIssue(ctx), UpdateIssue(ctx))
            case (True, AlertStatus.OPEN, _):
                return UpdateIssue(ctx)
            case (True, AlertStatus.CLOSED, IssueState.OPENED):
                return self._prevent_close_jitter(ctx)
            case (True, None, IssueState.OPENED):
                return CloseIssue(ctx)
            case _:
                return NoAction()

    def _prevent_close_jitter(self, ctx: IssueContext) -> IssueAction:
        """Prevent "flapping" issues and only close ISSUE_CLOSE_MINIMUM_AGE days after issue creation"""
        assert ctx.issue is not None

        # Transitional fix while moving to a pydantic model for metadata
        # Move from key names with dashes to snake case and
        # support both string and datetime datatypes.
        metadata = ctx.issue.metadata
        last_reopened_at = metadata.get(
            "last_reopened_at", metadata.get("last-reopened-at")
        )
        match last_reopened_at:
            case str():
                last_reopened_at = parse_date(last_reopened_at)
            case datetime():
                ...
            case _:
                last_reopened_at = None

        last_opened = last_reopened_at or ctx.issue.created_at

        age = (datetime.now(tz=UTC) - last_opened.astimezone(tz=UTC)).days
        if age > ISSUE_CLOSE_MINIMUM_AGE:
            return CloseIssue(ctx)
        else:
            return UpdateIssue(ctx)


class Issue(BaseModel):
    @classmethod
    def from_payload(cls, payload: dict):
        """
        Parse the GitLab API payload into an instance of Issue

        This also parses out Tamland's metadata from the description,
        if it exists (see .parse_metadata).
        """
        return Issue(
            id=payload["id"],
            iid=payload["iid"],
            title=payload["title"],
            description=payload["description"],
            state=IssueState(payload["state"]),
            labels=payload.get("labels", []),
            created_at=payload["created_at"],
            closed_at=payload.get("closed_at", None),
            metadata=cls.parse_metadata(payload),
        )

    id: int
    iid: int
    title: str
    description: str | None
    state: IssueState
    labels: list[str] = field(default_factory=list)
    created_at: AwareDatetime
    closed_at: AwareDatetime | None
    metadata: dict = field(default_factory=dict)

    @property
    def component_ident(self) -> str | None:
        return self.metadata.get("component_ident")

    @property
    def time_since_closed(self) -> timedelta | None:
        """Time since the issue was closed, None if it's open"""
        if self.closed_at is None:
            return None

        return datetime.now(tz=timezone.utc) - self.closed_at

    @classmethod
    def parse_metadata(cls, payload: dict) -> dict[str, str]:
        """
        Parses metadata from the description of a GitLab issue

        This utilizes a particular section with YAML data:

        ```yaml
        # tamland
        foo: bar
        ```

        Replace `foo: bar` with the desired YAML - this is
        what gets parsed out of the description and
        returned as a dictionary.
        """
        description = payload.get("description")

        if description is None:
            return dict()

        # Temporary fix to address an autoescape-related regression
        # in how we produce the YAML in the jinja2 templates.
        # This resulted in escaping single-quotes and subsequently we couldn't parse
        # the YAML anymore (ScannerError).
        #
        # This can be removed once all issues have been updated accordingly
        # (essentially after one issue management run).
        description = description.replace("&#39;", "'")

        match = re.search(r"```yaml\n# tamland\n(.*)\n```", description, re.DOTALL)

        if match is None:
            return dict()

        yaml_payload = match.group(1)
        try:
            return cast(dict[str, str], yaml.safe_load(yaml_payload))

        except yaml.YAMLError as e:
            logger.error(f"Failed to parse metadata from issue description: {e}")
            return dict()


class IssueState(StrEnum):
    OPENED = "opened"
    CLOSED = "closed"


PRIORITY_LABEL = "capacity-planning::priority"
VIOLATION_LABEL = "violation"
SLO_SATURATION = SLO(name="saturation", threshold=1.0)


class IssueLabeler:
    """
    For a given IssueContext, generates a full list of labels to be applied for
    the issue.
    """

    def __init__(self, ctx: IssueContext) -> None:
        self.ctx = ctx

    def labels(self) -> list[str]:
        """
        Generates a full list of labels to be applied for the IssueContext

        This leaves any labels in place we don't manage here.
        """

        if self.ctx.issue is None:
            return []

        labels = set(self.ctx.issue.labels)

        self._scope_labels(labels)
        self._additional_labels(labels)
        self._violation_labels(labels)
        self._priority_label(labels)

        return list(labels)

    def _scope_labels(self, labels: set[str]) -> None:
        """
        Adds configured scope labels
        """
        labels.update(self.ctx.issue_management_config.scope_labels)

    def _additional_labels(self, labels: set[str]) -> None:
        """
        Adds additional labels from configuration
        """
        labels.update(self.ctx.issue_management_config.additional_labels)

    def _violation_labels(self, labels: set[str]) -> None:
        """
        Add violation labels: For each SLO violation, we add a violation label
        with the respective name of the SLO
        """
        forecast = self.ctx.accessor.get_forecast(self.ctx.component)

        if forecast is None:
            return

        violation_labels = set(
            "%s:%s" % (VIOLATION_LABEL, violation.slo.name)
            for violation in forecast.slo_violations
            if violation.predicts_violation()
        )

        self._remove_by_prefix(labels, "%s:" % VIOLATION_LABEL)
        labels.update(violation_labels)

    def _priority_label(self, labels: set[str]) -> None:
        """
        Update priority label according to this logic:

        if saturation SLO predicted to breach:
            non-horizontal => do
                horizontal => delegate
        elsif any SLO predicted to breach:
            non-horizontal => decide
                horizontal => deny
        """

        alert = self.ctx.alert
        saturation_point = self.ctx.component.saturation_point

        if alert is None:
            return

        matrix = {
            (True, False): "do",
            (True, True): "delegate",
            (False, False): "decide",
            (False, True): "deny",
        }

        impending_saturation = any(
            outcome
            for outcome in alert.rule_outcomes
            if outcome.slo == SLO_SATURATION and outcome.triggered
        )

        priority = matrix[
            (impending_saturation, saturation_point.horizontally_scalable)
        ]

        self._remove_by_prefix(labels, PRIORITY_LABEL)
        labels.add("%s::%s" % (PRIORITY_LABEL, priority))

    def _remove_by_prefix(self, labels: set[str], prefix: str):
        """Remove existing labels with prefix and add new label"""
        labels -= set(label for label in labels if label.startswith(prefix))
