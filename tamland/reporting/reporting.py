from __future__ import annotations

import os
import pathlib
import re
from datetime import datetime
from functools import cache, partial
from os.path import join
from typing import Type, Tuple, Iterable, Callable, Any
from collections import defaultdict
from dataclasses import dataclass

import humanize
import inflection
import yaml
from fs import open_fs
from fs.base import FS
from fs.copy import copy_dir
from fs.path import dirname
from jinja2 import Environment, Template, FileSystemLoader, select_autoescape
from mkdocs import config
from mkdocs.__main__ import _enable_warnings
from mkdocs.commands import build

from tamland.core.forecaster import PredictedSLOViolation
from tamland.core.logger import logger
from tamland.core.models import ManifestReader, ReportPage, Component, SaturationPoint
from tamland.core.persistence import (
    ForecastRun,
    RunPersistence,
    ForecastPersistence,
    ComponentForecastOnDisk,
)
from tamland.core.util import relative_path


class ForecastRunAccessor:
    """Provides data access methods to dig into the given ForecastRun"""

    def __init__(self, fs: FS, run: ForecastRun) -> None:
        self.run_persistence = RunPersistence(fs)
        self.forecast_persistence = ForecastPersistence(fs)
        self.run = run
        self._forecast_cache: dict[str, ComponentForecastOnDisk | None] = {}

    @cache
    def manifest(self) -> ManifestReader:
        """Open the Tamland manifest used for the given run (as retrieved from persistence)"""
        with self.run_persistence.open_artifact(self.run, "manifest.json") as fp:
            return ManifestReader.from_io(fp)

    def get_forecast(self, component: Component) -> ComponentForecastOnDisk | None:
        """Retrieve the forecast for the given Component (if any) and cache the result"""

        # Note that Component is not hashable, so we can't use @cache
        cache_key = str(component)
        forecast = self._forecast_cache.get(cache_key)

        if forecast is None:
            forecast = self.forecast_persistence.read(self.run, component)
            self._forecast_cache[cache_key] = forecast

        return forecast

    def copy_forecast_artifact(
        self, component: Component, artifact_name: str, destination_path: str
    ) -> bool:
        """Copy an artifact, e.g. a plot, from the component forecast to a local destination"""
        pathlib.Path(dirname(destination_path)).mkdir(parents=True, exist_ok=True)

        with self.forecast_persistence.open_artifact(
            self.run, component, artifact_name
        ) as fp_in:
            # If fp_in is None, the artifact was not found - we cannot copy it
            if fp_in is None:
                return False

            with open(destination_path, "wb") as fp_out:
                fp_out.write(fp_in.read())

            return True


class MkdocsPage:
    """A mkdocs Page rendered from a template"""

    def __init__(
        self,
        run_accessor: ForecastRunAccessor,
        env: Environment,
        *,
        template_name: str | None = None,
        target_path: str | None = None,
    ) -> None:
        self.env = env
        self.run_accessor = run_accessor

        if template_name is None:
            name = re.sub(
                r"(.+)\_page?$", r"\1", inflection.underscore(self.__class__.__name__)
            )
            template_name = "%s.jinja.md" % name

        if target_path is None:
            target_path = re.sub(r"(.+)\.jinja\.md$", r"\1.md", template_name)

        self.template_name = template_name
        self.target_path = target_path

        # Tasks registered here will be triggered from `generate_assets()`
        # We store the Callable and the local path, relative to the report root in a tuple here
        self.asset_generation_tasks: list[Tuple[Callable[[str], Any], str]] = []

    def register_asset_generation_task(self, generator: Callable, local_path: str):
        """
        Register a task to generate and store an asset into a path relative to the report root directory

        When we call this, we don't know the **full** destination path. Hence we delay generating the asset
        until we call `generate_asset` with the missing information.
        """
        self.asset_generation_tasks.append((generator, local_path))

    def generate_assets(self, path_to_report: str):
        """
        Trigger generation of registered asset generators
        and let them generate their assets relative to the given report root directory
        """
        for generator, local_path in self.asset_generation_tasks:
            generator(os.path.join(path_to_report, local_path.lstrip("/")))

    def default_template_variables(self) -> dict:
        """Variables passed to all templates"""
        return dict(
            run=self.run_accessor.run,
            page=self,
            getenv=os.getenv,
        )

    def _get_template(self, name: str) -> Template:
        """Get the template with the given name"""
        return self.env.get_template(name)

    def variables(self) -> dict:
        """Return relevant variables for the template"""
        return dict()

    def link_component(
        self, text: str | None, component: Component, tooltip: str = ""
    ) -> str:
        """Produce a link to the component detail page with the given text and tooltip (if given)"""
        if text is None:
            text = str(component)

        # Notice that `some-dir/here.md` will be rendered into `some-dir/here/index.html
        start = f"/{self.target_path}"
        path = os.path.relpath(f"/components/{component}", start=start)

        return f'[{text}]({path} "{tooltip}")'

    def render(self) -> str:
        return self._get_template(self.template_name).render(
            self.default_template_variables() | self.variables()
        )


class IndexPage(MkdocsPage):
    """Home/landing page"""

    pass


class InternalStatsPage(MkdocsPage):
    """Page for internal statistics (mostly for debugging purposes)"""

    def variables(self) -> dict:
        metrics = self.run_accessor.run.metrics_as_dataframe()

        # SLO violations, sorted by date asc
        slo_violations = sorted(
            metrics.loc[metrics["earliest_slo_violation"].notnull()].to_dict("records"),
            key=lambda x: x["earliest_slo_violation"],
        )

        # Confidence ranges, sorted by confidence range desc
        confidence_ranges = sorted(
            metrics[["component", "confidence_range"]].to_dict("records"),
            key=lambda x: x["confidence_range"],
            reverse=True,
        )

        return dict(
            slo_violations=slo_violations,
            confidence_ranges=confidence_ranges,
        )


@dataclass
class ComponentViolations:
    """
    Model to keep track of the violations and the forecasted threshold breach dates.
    This is very GitLab.com specific, as other deployments could be using different
    thesholds than the ones we list here.

    We're discussing the extraction of the reporting component out of the tamland-tool
    so we have a better place for these GitLab.com specifics:
    https://gitlab.com/gitlab-com/gl-infra/tamland/-/issues/141
    """

    component: Component
    soft: datetime | None
    hard: datetime | None
    saturation: datetime | None


@dataclass
class ComponentCounts:
    total: dict[str, int]
    violations: dict[str, int]


class HeadroomSummaryPage(MkdocsPage):
    """Page that describes our saturation points and describes the runway we have on them"""

    def variables(self) -> dict:
        vars = dict(
            non_horizontally_scalable_important_components_with_violations=list(
                self.important_components_predicting_saturation(
                    extraFilter=lambda s: not s.horizontally_scalable
                ),
            ),
            horizontally_scalable_important_components_with_violations=list(
                self.important_components_predicting_saturation(
                    extraFilter=lambda s: s.horizontally_scalable
                ),
            ),
            counts={
                "horizontally_scalable": self.counts(
                    filterFn=lambda c: c.saturation_point.horizontally_scalable
                ),
                "non_horizontally_scalable": self.counts(
                    filterFn=lambda c: not c.saturation_point.horizontally_scalable
                ),
                "total": self.counts(),
            },
        )

        return vars

    def counts(self, filterFn=lambda c: True) -> ComponentCounts:
        high_level_component_metrics = self.run_accessor.run.metrics
        manifest = self.run_accessor.manifest()
        totals: dict[str, int] = defaultdict(lambda: 0)
        violations_counter: dict[str, int] = defaultdict(lambda: 0)

        for metrics in high_level_component_metrics:
            component = manifest.component_by(metrics.component)
            if component is not None and filterFn(component):
                totals[component.saturation_point.severity] += 1
                component_violations_date = self._violations_for(component)
                if (
                    component_violations_date is not None
                    and metrics.earliest_slo_violation is not None
                ):
                    _, violations, _ = component_violations_date

                    has_mean_violation = any(
                        [violation.yhat_date for violation in violations.values()]
                    )

                    if has_mean_violation:
                        violations_counter[component.saturation_point.severity] += 1

        return ComponentCounts(totals, violations_counter)

    def important_components_predicting_saturation(
        self, extraFilter: Callable[[SaturationPoint], bool]
    ) -> Iterable[ComponentViolations]:
        manifest = self.run_accessor.manifest()
        important_components = [
            c for c in manifest.components() if self._include_component(c, extraFilter)
        ]
        components_with_violations = self._attach_violations(important_components)

        return filter(
            lambda item: any([item.soft, item.hard, item.saturation]),
            components_with_violations,
        )

    def _include_component(
        self, c: Component, extraFilter: Callable[[SaturationPoint], bool]
    ) -> bool:
        return extraFilter(c.saturation_point) and c.saturation_point.severity in [
            "s1",
            "s2",
        ]

    def _attach_violations(
        self, components: Iterable[Component]
    ) -> Iterable[ComponentViolations]:
        all_components = filter(
            None,
            [self._violations_for(c) for c in components],
        )

        # Sort by earliest SLO violation, with None values **last**
        return [
            ComponentViolations(
                t[0],
                self._extract_date(t[1], "soft"),
                self._extract_date(t[1], "hard"),
                self._extract_date(t[1], "saturation"),
            )
            for t in sorted(all_components, key=lambda t: (t[2] is None, t[2]))
        ]

    def _extract_date(self, violations, key):
        predicted_slo_violation = violations.get(key)
        if predicted_slo_violation is not None:
            return predicted_slo_violation.yhat_date
        else:
            None

    def _violations_for(
        self, component: Component
    ) -> None | Tuple[Component, dict[str, PredictedSLOViolation], datetime | None]:
        forecast = self.run_accessor.get_forecast(component)

        if forecast is None:
            return None

        violations = {
            v.slo.name: v
            for v in sorted(forecast.slo_violations, key=lambda v: v.slo.threshold)
            if v.predicts_violation()
        }

        return component, violations, forecast.earliest_slo_violation()


class ConfigFile(MkdocsPage):
    """Mkdocs config file (mkdocs.yml)"""

    def __init__(self, run_accessor: ForecastRunAccessor, env: Environment) -> None:
        super().__init__(
            run_accessor,
            env,
            template_name="mkdocs.jinja.yml",
            target_path=join("..", "mkdocs.yml"),
        )

    def variables(self) -> dict:
        return dict(report_pages=self.run_accessor.manifest().pages())


class ComponentDetailPage(MkdocsPage):
    """Overview page for a group of services"""

    def __init__(
        self,
        run_accessor: ForecastRunAccessor,
        env: Environment,
        *,
        component: Component,
    ) -> None:
        super().__init__(
            run_accessor,
            env,
            target_path=join("components", f"{component}.md"),
        )
        self.component = component

    def variables(self) -> dict:
        if forecast := self.run_accessor.get_forecast(self.component):
            violations = forecast.predicted_violations()
        else:
            violations = {}

        return dict(component=self.component, violations=violations)

    def forecast_artifact(self, artifact_name: str) -> str:
        """
        Copy forecast artifact into assets directory and return a relative path

        Notice that asset generation happens async later with an asset generation task through `generate_assets()`.
        """
        path = f"assets/{self.component}_{artifact_name}"
        task = partial(
            self.run_accessor.copy_forecast_artifact, self.component, artifact_name
        )

        self.register_asset_generation_task(task, path)

        return path


class ServiceGroupOverview(MkdocsPage):
    """Overview page for a group of services"""

    def __init__(
        self,
        run_accessor: ForecastRunAccessor,
        env: Environment,
        *,
        report_page: ReportPage,
    ) -> None:
        super().__init__(
            run_accessor,
            env,
            target_path=join("service_groups", f"{report_page.slug}.md"),
        )
        self.report_page = report_page

    def variables(self) -> dict:
        manifest = self.run_accessor.manifest()

        services = [
            s for s in manifest.services() if self.report_page.includes_service(s)
        ]

        components_on_page = [
            c
            for c in self.run_accessor.manifest().components()
            if self.report_page.includes_service(c.service)
        ]

        component_groups = {
            "horizontal": self._attach_violations(
                filter(
                    lambda x: x.saturation_point.horizontally_scalable,
                    components_on_page,
                )
            ),
            "non_horizontal": self._attach_violations(
                filter(
                    lambda x: not x.saturation_point.horizontally_scalable,
                    components_on_page,
                )
            ),
        }

        return dict(
            report_page=self.report_page,
            component_groups=component_groups,
            services=services,
        )

    def forecast_artifact(self, component: Component, artifact_name: str) -> str:
        """
        Copy forecast artifact into assets directory and return a relative path

        Notice that asset generation happens async later with an asset generation task through `generate_assets()`.
        """
        path = f"assets/{component}_{artifact_name}"
        task = partial(
            self.run_accessor.copy_forecast_artifact, component, artifact_name
        )

        self.register_asset_generation_task(task, path)

        return path

    def _attach_violations(
        self, components: Iterable[Component]
    ) -> Iterable[Tuple[Component, dict[str, PredictedSLOViolation]]]:
        all_components = filter(
            None,
            [self._violations_for(c) for c in components],
        )

        # Sort by earliest SLO violation, with None values **last**
        return [
            (t[0], t[1])
            for t in sorted(all_components, key=lambda t: (t[2] is None, t[2]))
        ]

    def _violations_for(
        self, component: Component
    ) -> None | Tuple[Component, dict[str, PredictedSLOViolation], datetime | None]:
        forecast = self.run_accessor.get_forecast(component)

        if forecast is None:
            return None

        violations = {
            v.slo.name: v
            for v in sorted(forecast.slo_violations, key=lambda v: v.slo.threshold)
            if v.predicts_violation()
        }

        return component, violations, forecast.earliest_slo_violation()


class ServiceGroups(MkdocsPage):
    """Index page to show all service groups defined"""

    def __init__(self, run_accessor: ForecastRunAccessor, env: Environment) -> None:
        super().__init__(
            run_accessor,
            env,
            target_path=join("service_groups", "index.md"),
        )

    def variables(self) -> dict:
        return dict(report_pages=self.run_accessor.manifest().pages())


def days_until(target: datetime) -> int:
    return (target - datetime.utcnow()).days


def datetime_format(
    value: datetime | None, format: str = "%Y-%m-%d %H:%M UTC"
) -> str | None:
    if value is None:
        return None
    return value.strftime(format)


def date_format(value: datetime | None, format: str = "%Y-%m-%d") -> str | None:
    return datetime_format(value, format)


def str_or_else(
    value: str | None, prefix: str = "", suffix: str = "", if_none: str = ""
) -> str:
    if value is None:
        return if_none

    return "%s%s%s" % (prefix, value, suffix)


def pretty_violation_date(
    value: datetime | None,
    component: Component | None = None,
    page: MkdocsPage | None = None,
    format: str = "%Y-%m-%d",
    if_none: str = ":ok:",
) -> str:
    if value is None:
        return if_none

    formatted_date = date_format(value, format=format)

    time_until_violation = value - datetime.utcnow()
    time_direction = "left" if (value > datetime.utcnow()) else "ago"
    time_delta = "%s %s" % (humanize.naturaldelta(time_until_violation), time_direction)

    if component and page:
        content = page.link_component(time_delta, component, tooltip=time_delta)
    else:
        content = time_delta

    return ':warning:{ title="%s" } %s' % (formatted_date, content)


class ReportGenerationError(Exception):
    """An error occurred when generating a report"""

    ...


class BasicReport:
    def __init__(self, run_accessor: ForecastRunAccessor, *, target_path: str) -> None:
        self.run_accessor = run_accessor
        self.fs = open_fs(target_path, create=True)
        self.target_path = target_path

        self.env = Environment(
            loader=FileSystemLoader(
                relative_path(join("reporting", "basic", "templates"))
            ),
            keep_trailing_newline=True,
            autoescape=select_autoescape(),
        )
        self.env.filters["days_until"] = days_until
        self.env.filters["datetime_format"] = datetime_format
        self.env.filters["pretty_violation_date"] = pretty_violation_date
        self.env.filters["humanize_timedelta"] = humanize.naturaldelta
        self.env.filters["yaml"] = yaml.safe_dump

    def generate(self, *, clean: bool = True) -> None:
        logger.info(
            f"Generating BasicReport for {self.run_accessor.run} into {self.target_path}"
        )

        self._prepare_target(clean=clean)

        self._copy_assets_and_static()

        self._render_and_write(self._create_page(ServiceGroups))
        self._render_and_write(self._create_page(IndexPage))
        self._render_and_write(self._create_page(ConfigFile))
        self._render_and_write(self._create_page(InternalStatsPage))
        self._render_and_write(self._create_page(HeadroomSummaryPage))

        for page in self.run_accessor.manifest().pages():
            self._render_and_write(
                ServiceGroupOverview(self.run_accessor, self.env, report_page=page)
            )

        for component in self.run_accessor.manifest().components():
            self._render_and_write(
                ComponentDetailPage(self.run_accessor, self.env, component=component)
            )

        self._generate_mkdocs_site()

    def _copy_assets_and_static(self):
        src = open_fs(relative_path(join("reporting", "basic")))
        copy_dir(src, "/assets", self.fs, "/docs/assets")
        copy_dir(src, "/static", self.fs, "/docs")

    def _create_page(self, page_type: Type[MkdocsPage]) -> MkdocsPage:
        return page_type(self.run_accessor, self.env)

    def _render_and_write(self, page: MkdocsPage) -> None:
        path = join(self.target_path, "docs", page.target_path)
        pathlib.Path(os.path.dirname(path)).mkdir(parents=True, exist_ok=True)

        with open(path, "w") as fp:
            fp.write(page.render())

        # After we rendered the page, also generate any assets registered
        page.generate_assets(dirname(path))

    def _prepare_target(self, *, clean: bool) -> None:
        root_dir = self.fs.listdir("/")
        if len(root_dir) > 0 and clean:
            if "mkdocs.yml" not in root_dir:
                raise ReportGenerationError(
                    f"Cowardly refusing to cleanup directory {self.target_path}, which doesn't look like a report directory (no mkdocs.yml directory ?)"
                )
            self.fs.removetree("/")

    def _generate_mkdocs_site(self) -> None:
        _enable_warnings()
        cfg = config.load_config(
            config_file=join(self.target_path, "mkdocs.yml"),
            site_dir=join("report"),
        )

        cfg["plugins"].run_event("startup", command="build", dirty=False)
        try:
            build.build(cfg, dirty=False)
        finally:
            cfg["plugins"].run_event("shutdown")
