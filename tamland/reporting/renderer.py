import re
import shlex
import shutil
import subprocess  # nosec B404
import tempfile
from contextlib import contextmanager
from typing import IO, Generator

from fs import open_fs
from fs.base import FS
from fs.copy import copy_file

from tamland.core.logger import logger
from tamland.core.models import Component
from tamland.core.persistence import ForecastRun
from tamland.reporting import reporting


def render_history_video(fs_path: str, run: ForecastRun, component: Component) -> None:
    """
    Render history video for given Component (using data from the given ForecastRun)

    This is designed to be pickle friendly, so we can use it e.g. in a process pool.
    For that reason, we pass in the ForecastRun and construct the ForecastRunAccessor
    (which is not pickle-friendly on its own) ourselves.
    """
    fs = open_fs(fs_path)

    # This is a pickle friendly function. We pass in the ForecastRun
    # and construct the ForecastRunAccessor ourselves because of this.
    run_accessor = reporting.ForecastRunAccessor(fs, run)

    with (
        HistoryVideoRenderer(fs).render(component) as history_fp,
        run_accessor.forecast_persistence.save_artifact(
            run_accessor.run, component, "history.mp4"
        ) as artifact_fp,
    ):
        artifact_fp.write(history_fp.read())


class FfmpegUnavailable(Exception):
    """ffmpeg binary was not found"""

    ...


class HistoryVideoRenderer:
    def __init__(self, fs: FS):
        self.fs = fs
        self._ffmpeg = shutil.which("ffmpeg")

    def ffmpeg_available(self) -> bool:
        return self._ffmpeg is not None

    @contextmanager
    def render(
        self, component: Component, *, artifact: str = "forecast_debug_wide.png"
    ) -> Generator[IO, None, None]:
        """Uses ffmpeg to render a MP4 video with this component's forecast history"""

        if not self.ffmpeg_available():
            raise FfmpegUnavailable("Cannot render video, ffmpeg binary not found.")

        with tempfile.TemporaryDirectory() as tempdir:
            tempfs = open_fs(tempdir, create=True)

            regex = re.compile("([0-9]{8})-[0-9]{6}[^/]*/%s" % re.escape(artifact))

            fs = self.fs.opendir(
                "/forecasts/components/%s" % str(component).replace(".", "/")
            )

            for object in fs.glob(f"*/{artifact}"):
                if m := regex.search(object.path):
                    copy_file(fs, object.path, tempfs, "%s.png" % m[1])

            logger.info("Rendering history video for %s" % component)
            self._render_video(tempdir)

            with tempfs.open("output.mp4", "rb") as fp:
                yield fp

    def _render_video(self, tempdir: str) -> None:
        ffmpeg = (
            f'{self._ffmpeg} -framerate 8 -pattern_type glob -i "{tempdir}/*.png" -c:v libx264 '
            f"-profile:v high -crf 20 -pix_fmt yuv420p {tempdir}/output.mp4"
        )

        args = shlex.split(ffmpeg)
        subprocess.run(args, check=True, capture_output=True)  # nosec B603
