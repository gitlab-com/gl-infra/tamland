# Forecasting

## Prerequisites

In order to execute forecasting with Tamland, we setup the following components:

1. A [Prometheus connection](./prometheus.md) to retrieve saturation metrics from
2. A [Tamland manifest](./manifest.md)

## Environment variables

The following environment variables can be set for forecasting, e.g. when running `bin/shell forecast`:

| variable                       | description                                                       |                                | default                   |
|--------------------------------|-------------------------------------------------------------------|--------------------------------|---------------------------|
| PROMETHEUS_QUERY_URL           | Prometheus base URL                                               | optional                       | http://localhost:10902    |
| PROMETHEUS_HEADER_PROVIDER     | Path to header provider executable                                | optional                       | None                      |
| PROMETHEUS_HEADER_PROVIDER_TTL | Maximum interval between header refreshes (seconds)               | optional                       | 1800                      |

## Execute forecasting

In order to execute forecasting with a given manifest, we use the `bin/shell forecast` command:

```bash
bin/shell forecast ./forecasts --manifest-file=path/to/manifest.json
```

This executes forecasting for the components specified in the manifest and stores results in the `./forecasts` directory.
The same target directory can be re-used across multiple runs.

We can forecast a single component or a subset of all components by using a regex filter applied to the component name.
For example, we can use `--pattern postgres.*` to match all components for the `postgres` service.
