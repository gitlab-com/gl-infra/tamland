# Reporting

Tamland uses a mkdocs templating system to generate a full report.

Example report:

```bash
TAMLAND_REPORT_SITE_NAME="Testing only!" bin/shell report tests/data/result-full
```

This generates a mkdocs site into `./_report`, which can be viewed by locally serving the site:

```bash
cd _report
mkdocs serve

# Browse to http://127.0.0.1:8000/
```

## Environment variables

The following environment variables can be set for managing a GitLab issue tracker, i.e. when running `bin/shell manage-issues`:

| variable                   | description                                   |          | default                  |
|----------------------------|-----------------------------------------------|----------|--------------------------|
| TAMLAND_REPORT_SITE_NAME   | The site_name passed to mkdocs for the report | optional | Capacity Planning Report |
| TAMLAND_REPORT_BASE_URL    | The base URL the report can be accessed from  | optional | $CI_PAGES_URL            |
