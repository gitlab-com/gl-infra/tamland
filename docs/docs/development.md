# Development

Tamland is built in Python.

## Setup development environment

This project uses [mise](https://github.com/jdx/mise) (formerly known as rtx) to manage tool dependencies for development.

Install mise through below command or read about [other methods of installation](https://mise.jdx.dev/getting-started.html).

Tool dependencies and their versions are managed through `.tool-versions`.

```bash
curl https://mise.jdx.dev/install.sh | sh
```

Setup the local development environment and prepare all tool and python dependencies:

```bash
make prepare
```

Refer to GitLab's [developer setup guide](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md) at for more information.

### Python dependencies

Python dependencies are managed through `poetry`, which can be installed through pip.
This happens automatically with `make prepare`.

## Testing

Tamland includes pytest unit and integration tests, which can be run with `make test`.

To run all tests, lints, etc. run `make test-all`.

## Forecasting during development

Experimenting locally provides us with a shorter feedback loop to test new features and ideas.
There are a few steps to be done to set up your development environment to do that.

First, copy the sample environment variable file:

```bash
cp .env.example .env
```
Update the environment variables accordingly:

1. `PROMETHEUS_USERNAME`
1. `PROMETHEUS_PASSWORD`
1. `PROMETHEUS_QUERY_URL`
1. `PROMETHEUS_HEADER_PROVIDER`
1. `ALL_PROXY`

Most likely, only username and password need to be tweaked while the other values will be kept with the default value.

Evaluate the `.env` file in your shell:

```bash
source .env
```

Set up the ssh tunnel through the bastion host:

```bash
make ssh-tunnel

scripts/ssh-tunnel.sh
Connected! Press Enter to disconnect.
```

The username defaults to your system user, optionally set a specific username like below:

```bash
USER=tamland-ssh-user make ssh-tunnel
```

The connection with the Mimir backend can be tested using a script:

```bash
scripts/mimir-status.sh

{"status":"success","data":{"yaml": "global:\n  external_labels:\n    source: mimir"}}
```

Now everything should be ready to run a forecast:

```bash
bin/shell forecast . --manifest=manifest.json --pattern=ai-gateway.gcp_quota_limit_vertex_ai

Starting forecast for 1 components:
patroni-embedding.pg_table_bloat
2024-07-23 11:38:15,985   Starting forecast for patroni-embedding.pg_table_bloat
2024-07-23 11:38:15,987   Refreshing prometheus headers from: scripts/prometheus-header-provider.sh
2024-07-23 11:39:24,666   Saving forecast for patroni-embedding.pg_table_bloat to /forecasts/components/patroni-embedding/pg_table_bloat/20240723-093815-4973bf36-48d7-11ef-8c08-0ae32442bf55
```
