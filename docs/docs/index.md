# Tamland - Infrastructure Capacity Forecaster

Tamland forecasts capacity for infrastructure components and predicts saturation events.
Its intention is to prevent infrastructure-related incidents and provide a less obstrusive and earlier warning mechanism compared to sending out alerts when saturation occurs.

Tamland is implemented in Python and consumes Prometheus saturation metrics and uses [Facebook Prophet](https://facebook.github.io/prophet/) to forecast future saturation.

GitLab uses Tamland to forecast infrastructure capacity and assist with capacity planning purposes.
For GitLab specific documentation on how to use Tamland, please refer to the available [documentation in the GitLab Handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/observability/tamland/).

## Installation

Tamland can be used from a [development environment](./development.md) or installed through Docker (see below).
A [Helm chart](https://gitlab.com/gitlab-com/gl-infra/tamland/-/blob/main/chart/Chart.yaml) is also available for Kubernetes.

### Docker

```
# Latest release:
registry.gitlab.com/gitlab-com/gl-infra/tamland:latest

# Versioned:
registry.gitlab.com/gitlab-com/gl-infra/tamland:4.0.0
```

## Usage: Tamland Shell

Tamland comes with a CLI tool `shell`, which can be invoked through executing `bin/shell`.

A help command is available, which gives an overview over available commands.
Similarly, help is available for any of the commands implemented, e.g. invoke `bin/shell forecast --help` to get usage information
for the `forecast` command.

```
bin/shell --help
requirements.txt updated, running pip install

 Usage: shell.py [OPTIONS] COMMAND [ARGS]...

[...]
```

Please refer to [the forecasting page](./forecasting.md) to get started with forecasting.

### Environment variables

The following table contains commonly used environment variables.

| variable                                 | description                                                             |          | default   |
|------------------------------------------|-------------------------------------------------------------------------|----------|-----------|
| LOGLEVEL                                 | Log-level to be used for file based logging (tamland.log)               | optional | INFO      |
| TAMLAND_DISABLE_DYNAMIC_DIMENSION_LOOKUP | Disable dynamic dimension lookups through Prometheus (set to any value) | optional | (not set) |

## Trivia and History

![Brick Tamland, weather forecaster](media/tamland.jpg)

The project name was based on a character [Brick Tamland](https://anchorman.fandom.com/wiki/Brick_Tamland#:~:text=Brick%20Tamland%20is%20the%20weatherman,is%20portrayed%20by%20Steve%20Carell.), the weather forecaster from the movie Anchorman.

### Presentations

[![PromCon EU 2022: Tamland: How GitLab.Com Uses Long-Term Monitoring Data For Capacity Forecasting](http://img.youtube.com/vi/2R42jW98MXg/0.jpg)](http://www.youtube.com/watch?v=2R42jW98MXg "PromCon EU 2022: Tamland: How GitLab.Com Uses Long-Term Monitoring Data For Capacity Forecasting")
