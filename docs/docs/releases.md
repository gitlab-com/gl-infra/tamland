## Releases

### Releasing docker images and helm charts

Docker images are built and released with a `:tool-` tag for now. The latest version can be found here:

```
registry.gitlab.com/gitlab-com/gl-infra/tamland:tool-latest
```

We also maintain a Helm chart for Tamland under `./chart` and publish charts into this project's package repository.
Releasing a new version of the Helm chart happens automatically from CI.

#### Non-merged changes

For development changes on branches and MRs, we

1. only push a docker image to the registry and
2. tag it with the latest release plus a git SHA, for example `3.26.0-2-gc027698` (essentially `git describe --tags`).

The docker image is published in the `docker:publish` job ([example](https://gitlab.com/gitlab-com/gl-infra/tamland/-/jobs/7032988853#L476)).

To test the changes in an operational project, use the published docker image in the respective project.

#### Official releases

Following a Tamland release, we

1. Build and push a docker image tagged with `tool-latest` and `tool-${release_version}`, e.g. `tool-3.2.26` and
1. push a Helm chart tagged with `${release_version}` into the `stable` channel of the package registry.

Note that version tags are not prefixed with `v` and follow semantic versioning.
