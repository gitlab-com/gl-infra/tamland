# Issue management

The following environment variables can be set for managing a GitLab issue tracker, i.e. when running `bin/shell manage-issues`:

| variable                       | description                                                       |                                | default                   |
|--------------------------------|-------------------------------------------------------------------|--------------------------------|---------------------------|
| APPLY_ISSUE_CHANGES            | If set to "true", actual changes are made to the issue tracker    | optional                       | false                     |
| GITLAB_API_URL                 | URL for the GitLab v4 API to use for managing issues              | optional                       | https://gitlab.com/api/v4 |
| GITLAB_API_PRIVATE_TOKEN       | Private access token to use when accessing the GitLab API         | required for `manage-issues`   | None                      |                     |

## How does Tamland produce capacity alerts?

Tamland fetches the historical 180 days of data from Prometheus. It generates the next 3-month
forecast.

The forecasting includes includes different numbers reflecting different confidences, especially 80%
(worse case scenario) and mean (median scenario).

Each component may have a threshold, defined in the service catalog and fetched from thanos
with [this link](https://thanos.gitlab.net/graph?g0.expr=%20%20%20%20%20%20%20%20avg(%0A%20%20%20%20%20%20%20%20%20%20%20%20slo%3Amax%3Ahard%3Agitlab_component_saturation%3Aratio%7Benv%3D%22gprd%22%2C%20monitor!%3D%22global%22%2C%20component%3D%22gcp_quota_limit%22%7D%0A%20%20%20%20%20%20%20%20%20%20%20%20or%0A%20%20%20%20%20%20%20%20%20%20%20%20slo%3Amax%3Ahard%3Agitlab_component_saturation%3Aratio%7Benv%3D%22%22%2C%20monitor%3D%22global%22%2C%20component%3D%22gcp_quota_limit%22%7D%0A%20%20%20%20%20%20%20%20)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

We create issues on the [capacity planning project](https://gitlab.com/gitlab-com/gl-infra/capacity-planning/-/issues) when we predict a threshold violation.

## Configuration

When executing issue management, we specify `--config` with the path to a YAML file which provides configuration.

```bash
bin/shell manage-issues /data --config=issue-management.yml
```

A basic YAML configuration only needs to specify the GitLab project id.

```yaml
---
project_id: 12345
```

## Using scope labels

Scope labels narrow the search space for issues managed by Tamland.
For example, this can be used to create capacity warnings for many different environments on the same tracker.
In this example, each environment uses its own scope label which allows to differentiate environments on the tracker.

In order to use scope labels, we can specify one or more labels in the issue management configuration.

```yaml
---
project_id: 12345
scope_labels:
  - environment::xyz
```

More than one scope label can be used per configuration.

## Configuring maximum age for reopening an existing issue

Tamland reopens an existing, but closed issue for the same component if it's recent enough.
A closed issue is recent enough, if it was closed within a `recent_issue_age_in_days` number of days.
This defaults to a maximum time of 30 days.
A setting of `recent_issue_age_in_days=0` disables the functionality to re-open existing issues and will always result in new issues created.

```yaml
---
project_id: 12345
recent_issue_age_in_days: 60
```
