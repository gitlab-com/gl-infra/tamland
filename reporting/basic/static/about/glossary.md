# Glossary

This page explains commonly used terms in Tamland and this report.

| Term          | Description                                                         |
|---------------|---------------------------------------------------------------------|
| SLO           | Service Level Objective                                             |
| SLO warning   | Predicts a date in the future when the SLO is going to be violated. |
| SLO violation | The resource saturation is higher than the SLO threshold.           |
