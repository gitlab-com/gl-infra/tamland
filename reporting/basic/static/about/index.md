# About Tamland

Like a weather forecast for infrastructure, Tamland forecasts resource capacity and saturation events to support capacity planning processes and prevent saturation-based incidents.

Refer to the [Tamland project documentation](https://gitlab-com.gitlab.io/gl-infra/tamland/) for more information and implementation details.

### Presentations

[![PromCon EU 2022: Tamland: How GitLab.Com Uses Long-Term Monitoring Data For Capacity Forecasting](http://img.youtube.com/vi/2R42jW98MXg/0.jpg)](http://www.youtube.com/watch?v=2R42jW98MXg "PromCon EU 2022: Tamland: How GitLab.Com Uses Long-Term Monitoring Data For Capacity Forecasting")

### Trivia

The project name was based on a character [Brick Tamland](https://anchorman.fandom.com/wiki/Brick_Tamland#:~:text=Brick%20Tamland%20is%20the%20weatherman,is%20portrayed%20by%20Steve%20Carell.), the weather forecaster from the movie Anchorman.

![Tamland Logo](../assets/tamland-logo.jpg)
