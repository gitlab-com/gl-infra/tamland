# Internal statistics

This page gives an overview across forecasting metadata and component statistics.

## Forecast metadata

| Metadata    |                        |
| ----------- | ---------------------- |
| Run ID      | {{ run.uuid }}         |
| Started at  | {{ run.started_at }}   |
| Finished at | {{ run.finished_at }} |
| Duration    | {{ (run.finished_at - run.started_at).seconds }}s |
| Result path | {{ run.directory_name }} |

## Component overview

| Overview statistics |                |
| ----------- | ---------------------- |
| Number of components |  {{ run.components|count }} |
| Predicted SLO violations | {{ slo_violations|count }} |

## SLO violations

| Component | SLO violation date | Days left |
| ----------| -------------------|------------- |
{% for row in slo_violations -%}
| `{{ row['component'] }}` | {{ row['earliest_slo_violation'] }} | {{ row['earliest_slo_violation']|days_until }} |
{% endfor %}

## Confidence ranges

| Component | Confidence range |
| ----------| -------------------|
{% for row in confidence_ranges -%}
| `{{ row['component'] }}` | {{ row['confidence_range']|round(2) }} |
{% endfor %}
