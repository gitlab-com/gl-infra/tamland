# Component: {{ component }}

## Forecast

![]({{ page.forecast_artifact("forecast.png") }}){ loading=lazy, width=900 }

## SLO violations and capacity warnings


{% if violations|count > 0 %}
| SLO         | 80% confidence                                    | mean prediction |
|-------------|---------------------------------------------------|-----------------|
{% for slo_name, violation in violations.items() -%}
| {{ slo_name }} | {{ violation.yhat_upper_date|pretty_violation_date }} | {{ violation.yhat_date|pretty_violation_date }} |
{% endfor %}
{%- else %}
:white_check_mark: No violation predicted
{%- endif %}

## Component information

Service: {{ component.service }}

!!! note "Saturation Point"
    {{ component.saturation_point.description|indent(width=8) }}

## Debug forecast

### Plot with debug information

This forecast plot contains information useful for analyzing and debugging the forecasting model.

![]({{ page.forecast_artifact("forecast_debug_wide.png") }}){ loading=lazy }

### Forecast: components and seasonalities

![]({{ page.forecast_artifact("forecast_components.png") }}){ loading=lazy }
