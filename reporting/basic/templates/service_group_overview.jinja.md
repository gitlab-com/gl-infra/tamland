{% macro _service_group(components) -%}
{% for component, violations in components -%}
### {{ ':warning:' if violations|count > 0 else ':white_check_mark:' }} {{ component.title }}

=== "Forecast"

    ![]({{ page.forecast_artifact(component, "forecast.png") }}){ loading=lazy, width=500, align=left }

    {% if violations|count > 0 %}
    | SLO         | 80% confidence                                    | mean prediction |
    |-------------|---------------------------------------------------|-----------------|
    {% for slo_name, violation in violations.items() -%}
    | {{ slo_name }} | {{ violation.yhat_upper_date|pretty_violation_date }} | {{ violation.yhat_date|pretty_violation_date }} |
    {% endfor %}
    {%- else %}
    :white_check_mark: No violation predicted
    {%- endif %}

    {{ page.link_component("See component page for more details", component) }}

=== "Debug forecast"

    !!! note
        This forecast plot contains information useful for analyzing and debugging the forecasting model.

    ![]({{ page.forecast_artifact(component, "forecast_debug_wide.png") }}){ loading=lazy, width=700 }

=== "Description"

    !!! note
        {{ component.saturation_point.description|indent(width=8) }}

=== "Dashboards/Links"

    * {{ component.service.overview_dashboard }}
    * {{ component.service.resource_dashboard.get(component.saturation_point.name) }}

{% endfor %}

{%- endmacro %}

# Service Group: {{ report_page.title }}
This page contains components for the below services. The components shown are sorted by their earliest SLO violation (if any).

Services:

{% for service in services -%}
* {{ service.name }}
{% endfor %}

## Non-horizontally scalable components

There are {{ component_groups['non_horizontal']|count }} components in this category.

{{ _service_group(component_groups['non_horizontal']) }}

## Horizontally scalable components

There are {{ component_groups['horizontal']|count }} components in this category.

{{ _service_group(component_groups['horizontal']) }}
