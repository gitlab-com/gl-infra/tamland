# GitLab.com system headroom

This highlights the amount of headroom we have S1 and S2 saturation
points an their forecasted saturation dates.

It also counts how many components we are currently forecasting
utilization for.

Below information is entirely based on the mean prediction, not the
80% confidence interval of the prediction.

## S1 and S2 components forecasting saturation

We predict three dates for each component as shown below:

1. `SLO: capacity planning`: Forecasted date when the `soft` SLO will be breached (if any). When this SLO is predicted to breach with enough confidence, a capacity planning issue is created, and the priority determined.
2. `SLO: alerting`: Forecasted date when the `hard` SLO will be breached (if any). When this SLO is breached, the Engineer-On-Call starts to receive alerts for this components.
3. `Saturation date`: Forecasted date when the component will be at 100% saturation (if any).

### Non-horizontally scalable

Non-horizontally scalable components are components that are hard to
scale. We might need considerable time and engineering effort to move
these dates forward.

| Service | Component | Severity | SLO: capacity planning | SLO: alerting | Saturation date | Forecast horizon |
|---------|-----------|----------|------------------------|---------------|-----------------| ---------------- |
{% for component_violation in non_horizontally_scalable_important_components_with_violations -%}
| {{ component_violation.component.service.name }} | {{ page.link_component(component_violation.component.saturation_point.name, component_violation.component) }} | {{component_violation.component.saturation_point.severity}} | {{ component_violation.soft|pretty_violation_date(component=component_violation.component, page=page) }} | {{ component_violation.hard|pretty_violation_date(component=component_violation.component, page=page) }} | {{ component_violation.saturation|pretty_violation_date(component=component_violation.component, page=page) }} | {{ component_violation.component.capacity_planning.forecast_days }} days ||
{% endfor %}

### Horizontally scalable

Horizontally scalable components are components that we can scale by
adding more resources. We generally do this when capacity planning
notifies of violations of the thresholds.

| Service | Component | Severity | SLO: capacity planning | SLO: alerting | Saturation date | Forecast horizon |
|---------|-----------|----------|------------------------|---------------|-----------------| ---------------- |
{% for component_violation in horizontally_scalable_important_components_with_violations -%}
| {{ component_violation.component.service.name }} | {{ page.link_component(component_violation.component.saturation_point.name, component_violation.component) }} | {{component_violation.component.saturation_point.severity}} | {{ component_violation.soft|pretty_violation_date(component=component_violation.component, page=page) }} | {{ component_violation.hard|pretty_violation_date(component=component_violation.component, page=page) }} | {{ component_violation.saturation|pretty_violation_date(component=component_violation.component, page=page) }} | {{ component_violation.component.capacity_planning.forecast_days }} days ||
{% endfor %}



## All saturation components

This lists all components we are currently forecasting utilization
for. We count the number of components that forecast a breach of the
soft SLO for the mean prediction as well as the total number of
components per severity.

|                           | S1                                                                       | S2   | S3 | S4 |
|---------------------------|--------------------------------------------------------------------------|------|----|----|
| Non Horizontally scalable     | {{ counts['non_horizontally_scalable'].violations['s1'] }}/{{ counts['non_horizontally_scalable'].total['s1'] }} | {{ counts['non_horizontally_scalable'].violations['s2'] }}/{{ counts['non_horizontally_scalable'].total['s2'] }} | {{ counts['non_horizontally_scalable'].violations['s3'] }}/{{ counts['non_horizontally_scalable'].total['s3'] }} | {{ counts['non_horizontally_scalable'].violations['s4'] }}/{{ counts['non_horizontally_scalable'].total['s4'] }} |
| Horizontally scalable     | {{ counts['horizontally_scalable'].violations['s1'] }}/{{ counts['horizontally_scalable'].total['s1'] }} | {{ counts['horizontally_scalable'].violations['s2'] }}/{{ counts['horizontally_scalable'].total['s2'] }} | {{ counts['horizontally_scalable'].violations['s3'] }}/{{ counts['horizontally_scalable'].total['s3'] }} | {{ counts['horizontally_scalable'].violations['s4'] }}/{{ counts['horizontally_scalable'].total['s4'] }} |
| Totals                    | {{ counts['total'].violations['s1'] }}/{{ counts['total'].total['s1'] }} | {{ counts['total'].violations['s2'] }}/{{ counts['total'].total['s2'] }} | {{ counts['total'].violations['s3'] }}/{{ counts['total'].total['s3'] }} | {{ counts['total'].violations['s4'] }}/{{ counts['total'].total['s4'] }} |
