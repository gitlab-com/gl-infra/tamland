# Tamland - Infrastructure Capacity Forecaster

Tamland forecasts capacity for infrastructure components and predicts saturation events.
Its intention is to prevent infrastructure-related incidents and provide a less obstrusive and earlier warning mechanism compared to sending out alerts when saturation occurs.

![Brick Tamland, weather forecaster](img/tamland.jpg)

GitLab uses Tamland to forecast infrastructure capacity and assist with capacity planning purposes.
For GitLab specific documentation on how to use Tamland, please refer to the available [documentation in the GitLab Handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/observability/tamland/).

Please refer to documentation in [./docs](https://gitlab.com/gitlab-com/gl-infra/tamland/-/tree/main/docs) or browse the live documentation here:

https://gitlab-com.gitlab.io/gl-infra/tamland
