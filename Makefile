.PHONY: prepare
prepare:
	scripts/prepare-dev-env.sh

jupyter_build := jupyter-book build --path-output=. -v

.PHONY: build-jupyter
build-jupyter: export PYDEVD_DISABLE_FILE_VALIDATION ?= 1
build-jupyter:
	bin/tamland-run $(jupyter_build) $(file)

.PHONY: manage-issues
manage-issues:
	bin/tamland-run python src-jupyter/issues.py

.PHONY: populate-cache
populate-cache:
	bin/tamland-run python -u src-jupyter/populate_cache.py

.PHONY: test-all
test-all: test lint formatting security-scan

.PHONY: test
test:
	bin/tamland-run pytest --cov --cov-report term --cov-report xml:coverage.xml tests

.PHONY: lint
lint:
	bin/tamland-run flake8

.PHONY: formatting
formatting:
	bin/tamland-run black --check --diff --color --verbose .

.PHONY: fix-formatting
fix-formatting:
	bin/tamland-run black .

.PHONY: security-scan
security-scan:
	bin/tamland-run bandit -c .bandit.yml -r .

.PHONY: clean-logs
clean-logs:
	rm *.log

.PHONY: clean-cache
clean-cache:
	rm -r data/

.PHONY: clean-all
clean-all: clean clean-logs clean-cache

.PHONY: ssh-tunnel
ssh-tunnel:
	scripts/ssh-tunnel.sh
