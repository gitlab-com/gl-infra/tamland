import os
from unittest import mock

from tamland.core.executors import (
    factory,
    LocalExecutorProvider,
    ParallelExecutorProvider,
    LocalExecutor,
)
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor


class TestFactory:
    @mock.patch.dict(os.environ, clear=True)
    def test_factory_with_defaults(self):
        assert isinstance(factory(), LocalExecutorProvider)

    @mock.patch.dict(os.environ, {"TAMLAND_PARALLEL_FORECASTING": "1"}, clear=True)
    def test_factory_for_parallel_processing(self):
        assert isinstance(factory(), ParallelExecutorProvider)


class TestLocalExecutor:
    def test_submit(self):
        def dummy(subject: str) -> str:
            return f"hello {subject}"

        future = LocalExecutor().submit(dummy, "world")

        assert future.result() == "hello world"


class TestLocalExecutorProvider:
    def test_forecasting_executor(self):
        local_executor_provider = LocalExecutorProvider()
        assert isinstance(local_executor_provider.forecasting_executor(), LocalExecutor)

    def test_writing_executor(self):
        local_executor_provider = LocalExecutorProvider()
        assert isinstance(local_executor_provider.writing_executor(), LocalExecutor)


class TestParallelExecutorProvider:
    @mock.patch("multiprocessing.cpu_count")
    def test_providers_with_defaults(self, mock_cpu_count):
        mock_cpu_count.return_value = 2
        parallel_executor_provider = ParallelExecutorProvider()

        forecasting_executor = parallel_executor_provider.forecasting_executor()
        assert isinstance(forecasting_executor, ProcessPoolExecutor)
        assert forecasting_executor._max_workers == 2

        writing_executor = parallel_executor_provider.writing_executor()
        assert isinstance(writing_executor, ThreadPoolExecutor)
        assert writing_executor._max_workers == 10
