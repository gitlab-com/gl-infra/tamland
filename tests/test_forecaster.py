from datetime import datetime
from functools import partial
from unittest import mock
from unittest.mock import patch, Mock

import datatest as dt
import numpy as np
import pytest
from freezegun.api import FakeDatetime
from pandas import read_csv, DataFrame, read_parquet
import pandas as pd


from tamland.core.forecaster import (
    HistoricalData,
    Forecaster,
    EventAnnotator,
    PredictedSLOViolation,
    SLOViolationPredictor,
)
from tamland.core.models import Component, Outlier, SLO
from tamland.core.util import relative_path


class TestHistoricalData:
    @pytest.fixture
    def history(self, component: Component) -> HistoricalData:
        return HistoricalData(component)

    @pytest.mark.freeze_time("2023-08-10")
    def test_load(self, history: HistoricalData, history_dataframe: DataFrame):
        with patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = history_dataframe

            data = history.load()

            mock_batched_query_range.assert_called_once_with(
                'max(quantile_over_time(0.95, gitlab_component_saturation:ratio{component="cpu",type="patroni",env="gprd",environment="gprd",stage=~"main|"}[1h]))',  # noqa:E501
                FakeDatetime(2022, 8, 10, 0, 0),
                FakeDatetime(2023, 8, 10, 0, 0),
            )

            dt.validate(data.columns, ["y", "ds", "cap", "floor", "y_outlier"])

            assert all(data["cap"] == 1)
            assert all(data["floor"] == 0)

            assert data["y"].sum() == history_dataframe["y"].sum()

    def test_load_is_cached(
        self, history: HistoricalData, history_dataframe: DataFrame
    ):
        with patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = history_dataframe

            # Call twice
            history.load()
            history.load()

            mock_batched_query_range.assert_called_once()

    def test_generate_changepoints(
        self, history: HistoricalData, history_dataframe: DataFrame
    ):
        with patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = history_dataframe

            cp = history.component.capacity_planning
            cp.changepoints_count = 20
            cp.changepoint_range = 0.8
            cp.changepoints = [datetime(2023, 1, 24), datetime(2023, 4, 21)]

            assert history.generate_changepoints() == [
                "2022-08-24",
                "2022-09-07",
                "2022-09-21",
                "2022-10-05",
                "2022-10-19",
                "2022-11-04",
                "2022-11-18",
                "2022-12-02",
                "2022-12-17",
                "2022-12-31",
                "2023-01-14",
                "2023-01-24",  # custom changepoint
                "2023-01-28",
                "2023-02-11",
                "2023-02-25",
                "2023-03-11",
                "2023-03-25",
                "2023-04-09",
                "2023-04-21",  # custom changepoint
                "2023-04-23",
                "2023-05-07",
                "2023-05-21",
            ]

    def test_default_changepoints_with_only_five(
        self, history: HistoricalData, history_dataframe: DataFrame
    ):
        with patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = history_dataframe

            cp = history.component.capacity_planning
            cp.changepoints_count = 5
            cp.changepoint_range = 0.8
            cp.changepoints = []

            assert history.generate_changepoints() == [
                "2022-10-05",
                "2022-12-02",
                "2023-01-28",
                "2023-03-25",
                "2023-05-21",
            ]

    def test_outliers(self, history: HistoricalData, history_dataframe: DataFrame):
        with patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = history_dataframe

            outliers = [
                Outlier(start=datetime(2023, 4, 1), end=datetime(2023, 4, 10)),
                Outlier(start=datetime(2023, 5, 1), end=datetime(2023, 5, 10)),
            ]
            history.component.capacity_planning.ignore_outliers = outliers

            original_history = history_dataframe.copy()

            df = history.load()

            for outlier in outliers:
                values_in_actual_series = np.unique(
                    df.loc[(df["ds"] >= outlier.start) & (df["ds"] < outlier.end), "y"]
                )

                # outliers are removed from y series
                assert len(values_in_actual_series) == 1
                assert np.isnan(values_in_actual_series)

                outlier_series = df.loc[
                    (df["ds"] >= outlier.start) & (df["ds"] < outlier.end), "y_outlier"
                ]

                # outliers are present in y_outlier series
                original_series_for_outlier_range = original_history.loc[
                    (original_history["ds"] >= outlier.start)
                    & (original_history["ds"] < outlier.end),
                    "y",
                ]
                assert all(original_series_for_outlier_range.eq(outlier_series))


class TestForecaster:
    @pytest.fixture
    def forecaster(self, component: Component) -> Forecaster:
        return Forecaster(component, min_samples_for_prediction=10)

    @pytest.fixture
    def historical_data_frame(self) -> DataFrame:
        return read_csv(
            relative_path("tests/data/fixture_historical_data.csv"), parse_dates=["y"]
        )

    @mock.patch("tamland.core.forecaster.HistoricalData.load")
    def test_skip_forecast_with_short_history(
        self, mock_load, forecaster: Forecaster, historical_data_frame: DataFrame
    ):
        mock_load.return_value = historical_data_frame.head(5)

        assert forecaster.generate() is None

    @mock.patch("tamland.core.forecaster.HistoricalData.load")
    def test_skip_forecast_lacking_enough_data(
        self,
        mock_load,
        forecaster: Forecaster,
    ):
        # DataFrame with only one non-NaN row
        timeframe = pd.date_range(start="2024-09-01", end="2024-09-24", freq="D")
        mostly_nan_array = [np.nan] * (len(timeframe) - 1)
        mostly_nan_array.append(0.5)
        historical_data_frame = pd.DataFrame({"ds": timeframe, "y": mostly_nan_array})

        mock_load.return_value = historical_data_frame

        assert forecaster.generate() is None

    @mock.patch("tamland.core.forecaster.HistoricalData.load")
    @mock.patch("tamland.core.forecaster.HistoricalData.generate_changepoints")
    @mock.patch("tamland.core.forecaster.SLOViolationPredictor.predict_violations")
    def test_generate_forecast(
        self,
        mock_predict_violations,
        mock_generate_changepoints,
        mock_load,
        forecaster: Forecaster,
        historical_data_frame: DataFrame,
    ):
        violations = Mock("violations")
        mock_predict_violations.return_value = violations
        mock_load.return_value = historical_data_frame

        changepoints = ["2022-08-21", "2022-11-08"]
        mock_generate_changepoints.return_value = changepoints

        # We don't mock Prophet here, so this actually executes the Prophet forecast which can take a few seconds
        forecast = forecaster.generate()

        assert forecast.component == forecaster.component

        model = forecast.model

        assert model.daily_seasonality is True
        assert model.weekly_seasonality is True
        assert model.yearly_seasonality is False

        assert model.changepoint_prior_scale == 0.05
        assert list(model.changepoints) == [
            datetime.strptime(c, "%Y-%m-%d") for c in changepoints
        ]

        assert model.country_holidays == "US"

        series = forecast.series

        assert (
            len(series) - len(historical_data_frame)
            == forecaster.component.capacity_planning.forecast_days * 24
        )  # hourly

        assert {
            "ds",
            "trend",
            "yhat_lower",
            "yhat_upper",
            "trend_lower",
            "trend_upper",
            "yhat",
        } <= set(series.columns)

        for slo in forecaster.component.saturation_point.slos:
            dt.validate(series[f"{slo.name}_threshold"], slo.threshold)

        assert forecast.slo_violations == violations

        assert (
            forecast.metrics.confidence_range
            == series.iloc[-1]["yhat_upper"] - series.iloc[-1]["yhat_lower"]
        )

    @mock.patch("tamland.core.forecaster.HistoricalData.load")
    @mock.patch("tamland.core.forecaster.HistoricalData.generate_changepoints")
    def test_generate_forecast_seasonalities(
        self,
        mock_generate_changepoints,
        mock_load,
        forecaster: Forecaster,
        historical_data_frame: DataFrame,
        component: Component,
    ):
        forecaster.component.capacity_planning.yearly_seasonality = "auto"
        forecaster.component.capacity_planning.daily_seasonality = False

        mock_load.return_value = historical_data_frame

        changepoints = []
        mock_generate_changepoints.return_value = changepoints

        forecast = forecaster.generate()

        assert not forecast.model.daily_seasonality
        assert forecast.model.yearly_seasonality == "auto"


class TestSLOViolationPredictor:
    @pytest.fixture
    def forecast_series(self) -> DataFrame:
        return read_parquet(
            relative_path("tests/data/fixture_forecast_dataframe.parquet")
        )

    @pytest.mark.freeze_time("2023-08-30")
    def test_predict_violations(self, component: Component, forecast_series: DataFrame):
        violations = SLOViolationPredictor(component).predict_violations(
            forecast_series
        )

        assert violations == [
            PredictedSLOViolation(
                slo=SLO(name="soft", threshold=0.8),
                yhat_date=datetime(2024, 9, 12, 15, 0),
                yhat_upper_date=datetime(2024, 3, 7, 15, 0),
            ),
            PredictedSLOViolation(
                slo=SLO(name="hard", threshold=0.9),
                yhat_date=datetime(2024, 12, 5, 15, 0),
                yhat_upper_date=datetime(2024, 4, 11, 15, 0),
            ),
        ]


class TestPredictedSLOViolation:
    def test_predicts_violation(self, slo: SLO):
        cons = partial(PredictedSLOViolation, slo=slo)

        assert cons(
            yhat_date=datetime(2024, 12, 5, 15, 0),
            yhat_upper_date=datetime(2024, 4, 11, 15, 0),
        ).predicts_violation()

        assert cons(
            yhat_date=datetime(2024, 12, 5, 15, 0), yhat_upper_date=None
        ).predicts_violation()

        assert cons(yhat_date=None, yhat_upper_date=None).predicts_violation() is False


@pytest.mark.freeze_time("2023-08-30")
class TestEventAnnotator:
    def test_add_to_figure(self, component_with_events: Component):
        forecast = Mock(
            "forecast",
            component=component_with_events,
            history=Mock(series=DataFrame(data=dict(cap=[1], floor=[0]))),
        )

        vline_calls = []

        def remember_vline_call(*, x, **_):
            vline_calls.append(x)

        annotation_calls = []

        def remember_annotation_call(*, x, y, text, **_):
            annotation_calls.append(dict(x=x, y=y, text=text))

        add_trace_calls = []

        def remember_trace_call(scatter):
            add_trace_calls.append(scatter)

        fig = Mock(
            "fig",
            add_vline=Mock(side_effect=remember_vline_call),
            add_annotation=Mock(side_effect=remember_annotation_call),
            add_trace=Mock(side_effect=remember_trace_call),
        )

        EventAnnotator(forecast).add_to_figure(fig)

        # Idea here is to test for the relevant parts, not for the styling
        assert vline_calls == [e.date for e in component_with_events.events]
        assert annotation_calls == [
            dict(x=e.date, y=1, text=f" <b>{i + 1}</b> ")
            for i, e in enumerate(component_with_events.events)
        ]

        # Verify traces were added for events to show up in the legend
        assert len(add_trace_calls) == len(component_with_events.events)
        for scatter in add_trace_calls:
            assert scatter.showlegend
            assert scatter.legendgroup == "events"
        assert [s.x for s in add_trace_calls] == [
            (e.date, e.date) for e in component_with_events.events
        ]
        assert [s.name for s in add_trace_calls] == [
            f"<b>({i + 1})</b>: {e.name}"
            for i, e in enumerate(component_with_events.events)
        ]
