import os
from unittest import mock
from unittest.mock import Mock

import pytest
import requests

from tamland.core.prom_provider import (
    factory,
    DefaultPrometheusProvider,
    PrometheusProviderWithDynamicHeaders,
    PrometheusUnavailableError,
)
from tamland.core.util import relative_path


class TestFactory:
    @mock.patch.dict(os.environ, clear=True)
    def test_factory_with_defaults(self):
        provider = factory()

        assert isinstance(provider, DefaultPrometheusProvider)
        assert provider.url == "http://localhost:10902"

    @mock.patch.dict(
        os.environ, {"PROMETHEUS_QUERY_URL": "http://prometheus.local"}, clear=True
    )
    def test_factory_with_custom_url(self):
        provider = factory()

        assert isinstance(provider, DefaultPrometheusProvider)
        assert provider.url == "http://prometheus.local"

    @mock.patch.dict(
        os.environ, {"THANOS_QUERY_URL": "http://prometheus.local"}, clear=True
    )
    def test_factory_with_custom_thanos_url(self):
        provider = factory()

        assert isinstance(provider, DefaultPrometheusProvider)
        assert provider.url == "http://prometheus.local"

    @mock.patch.dict(
        os.environ,
        {
            "PROMETHEUS_QUERY_URL": "http://prometheus.local",
            "PROMETHEUS_HEADER_PROVIDER": "test-program.sh",
        },
        clear=True,
    )
    def test_factory_with_header_provider(self):
        provider = factory()

        assert isinstance(provider, PrometheusProviderWithDynamicHeaders)
        assert provider.url == "http://prometheus.local"
        assert provider.header_provider == "test-program.sh"


class TestDefaultPrometheusProvider:
    @mock.patch("requests.Session")
    @mock.patch("prometheus_pandas.query.Prometheus")
    def test_prometheus(self, mock_prom, mock_session):
        mocked_session = Mock()
        mock_session.return_value = mocked_session

        mocked_prom = Mock()
        mock_prom.return_value = mocked_prom

        prov = DefaultPrometheusProvider(url="http://prometheus.local")
        prom = prov.prometheus()

        assert prom == mocked_prom
        mock_prom.assert_called_once_with("http://prometheus.local", mocked_session)


class TestPrometheusProviderWithDynamicHeaders:
    @mock.patch("prometheus_pandas.query.Prometheus")
    def test_prometheus(self, mock_prom):
        session = requests.Session()

        with mock.patch("requests.Session") as mock_session:
            mock_session.return_value = session

            mocked_prom = Mock()
            mock_prom.return_value = mocked_prom

            header_provider = relative_path("tests/data/fixture_header_provider.sh")

            prov = PrometheusProviderWithDynamicHeaders(
                url="http://prometheus.local", header_provider=header_provider
            )
            prom = prov.prometheus()

            assert prom == mocked_prom
            mock_prom.assert_called_once_with("http://prometheus.local", session)
            assert session.headers["Authorization"] == "Bearer foo-bar-test"

    def test_prometheus_with_header_provider_failure(self):
        header_provider = relative_path("tests/data/fixture_header_provider.sh --fail")

        prov = PrometheusProviderWithDynamicHeaders(
            url="http://prometheus.local", header_provider=header_provider
        )

        with pytest.raises(
            PrometheusUnavailableError, match="returned non-zero exit status"
        ):
            prov.prometheus()
