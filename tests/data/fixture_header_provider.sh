#!/usr/bin/env bash
flag="$1"

set -euo pipefail

if [ "${flag}" == "--fail" ]; then
  echo "Things are so broken today, failing!"
  exit 2
fi

cat <<EOF
{
  "headers": {
    "Authorization": "Bearer foo-bar-test"
  }
}
EOF
