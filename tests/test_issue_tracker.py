import os
from datetime import datetime, timezone, UTC
from textwrap import dedent
from unittest import mock
from unittest.mock import Mock, patch

import pytest
import yaml
from callee import Contains, Any
from freezegun import freeze_time

from tamland.core.alerting import (
    AlertStatus,
    AlertingRuleOutcome,
    AlertingRule_ConfidenceIntervalPrediction,
)
from tamland.core.gitlab_api_client import DryRun, GitLabAPIClient
from tamland.reporting.issue_tracker import (
    gitlab_client,
    manage_capacity_warnings,
    IssueTracker,
    CompositeAction,
    WithIssueTemplatesMixin,
    CreateIssue,
    IssueState,
    Issue,
    UpdateIssue,
    CloseIssue,
    NoAction,
    IssueContext,
    CloseOrphanedIssue,
    IssueLabeler,
    SLO_SATURATION,
    severity_label,
    IssueManagementConfig,
)
from tamland.core.models import SLO
from tamland.core.util import relative_path


class TestGitlabClientFactory:
    @mock.patch.dict(os.environ, clear=True)
    def test_dry_run(self):
        assert type(gitlab_client()) is DryRun

    @mock.patch.dict(
        os.environ,
        {"APPLY_ISSUE_CHANGES": "true", "GITLAB_API_PRIVATE_TOKEN": "token"},
        clear=True,
    )
    def test_with_real_client(self):
        client = gitlab_client()
        assert type(client) is GitLabAPIClient
        assert client.base_url == "https://gitlab.com/api/v4"
        assert client.headers.get("Private-Token") == "token"


class TestManageCapacityWarnings:
    @mock.patch("tamland.reporting.issue_tracker.IssueTracker")
    def test_flow(self, issue_tracker_class, component):
        run_accessor = Mock()
        manifest = Mock()
        run_accessor.manifest.return_value = manifest
        manifest.components.return_value = [component]

        config = IssueManagementConfig(project_id=4711)
        client = Mock()
        decision_maker = Mock()
        tracker = Mock()

        forecast = Mock()

        issue_tracker_class.return_value = tracker
        run_accessor.get_forecast.return_value = forecast

        alert = Mock()
        decision_maker.decide.return_value = alert

        action = Mock()
        tracker.process.return_value = action

        cleanup_action = Mock()
        tracker.cleanup_actions.return_value = [cleanup_action]

        manage_capacity_warnings(run_accessor, config, client, decision_maker)

        issue_tracker_class.assert_called_once_with(client, config, run_accessor)
        run_accessor.get_forecast.assert_called_once_with(component)
        decision_maker.decide.assert_called_once_with(forecast)
        tracker.process.assert_called_once_with(component, alert)
        action.execute.assert_called_once_with(client, config.project_id)
        cleanup_action.execute.assert_called_once_with(client, config.project_id)


class TestCompositeAction:
    def test_execute(self):
        project_id = 4711
        client = Mock()

        action1 = Mock()
        action2 = Mock()

        CompositeAction(action1, action2).execute(client, project_id)

        action1.execute.assert_called_once_with(client, project_id)
        action2.execute.assert_called_once_with(client, project_id)


class TestWithIssueTemplates:
    def test_render(self, mocked_issue_context):
        mixin = WithIssueTemplatesMixin(
            mocked_issue_context, relative_path("tests/issue_tracker/issue-templates")
        )
        assert mixin.render("my-template") == "Foo bar test"

    def test_passing_in_vars(self, mocked_issue_context):
        mixin = WithIssueTemplatesMixin(mocked_issue_context)

        with patch("jinja2.environment.Environment.get_template") as mock_get_template:
            template = Mock()
            mock_get_template.return_value = template

            mixin.render("my-template", foo="bar")

            vars = dict(
                component=mocked_issue_context.component,
                alert=mocked_issue_context.alert,
                forecast=mocked_issue_context.alert.forecast,
                issue=mocked_issue_context.issue,
                getenv=os.getenv,
                foo="bar",
                _metadata_in_place=mocked_issue_context.metadata_in_place(),
            )
            template.render.assert_called_with(vars)


@pytest.fixture
def issue_payload(description) -> dict:
    return dict(
        id=12345,
        iid=1,
        title="Test Issue",
        description=description,
        state=IssueState.OPENED,
        labels=["one", "two", "three"],
        created_at=datetime.now(tz=timezone.utc),
        closed_at=None,
    )


@pytest.fixture
def description():
    return dedent(
        """
    Some issue description here

    ```yaml
    # tamland
    foo: bar
    component_ident: test
    ```

    And here also
    """
    )


@pytest.fixture
def mocked_issue_context(component, capacity_alert, forecast, issue_management_config):
    accessor = Mock()
    accessor.get_forecast.return_value = forecast
    return IssueContext(accessor, component, issue_management_config, capacity_alert)


@pytest.fixture
def forecast():
    forecast = Mock()
    forecast.slo_violations = []
    return forecast


@pytest.fixture
def mock_issue():
    issue = Mock()
    issue.iid = 123
    issue.labels = []
    issue.metadata = {"component_ident": "test"}

    return issue


ContainsTamlandMetadata = Contains("```yaml\n# tamland")


class TestCreateIssue:
    def test_execute(self, project_id, issue_payload, mocked_issue_context):
        client = Mock()
        client.create_issue.return_value = issue_payload

        action = CreateIssue(mocked_issue_context)

        action.execute(client, project_id)

        client.create_issue.assert_called_once_with(
            str(project_id),
            title=Contains("Capacity warning for"),
            description=ContainsTamlandMetadata,
            labels=mocked_issue_context.issue_management_config.scope_labels,
        )

        assert mocked_issue_context.issue == Issue.from_payload(issue_payload)


class TestUpdateIssue:
    @pytest.mark.freeze_time
    @mock.patch("tamland.reporting.issue_tracker.IssueLabeler")
    def test_execute(
        self,
        issue_labeler_class,
        project_id,
        issue_payload,
        mocked_issue_context,
        mock_issue,
    ):
        client = Mock()

        client.upload_project_file.return_value = dict(
            markdown="![something](/uploads/something.png)"
        )

        mocked_issue_context.issue = mock_issue

        new_labels = Mock()

        labeler = Mock()
        issue_labeler_class.return_value = labeler
        labeler.labels.return_value = new_labels

        action = UpdateIssue(mocked_issue_context)

        action.execute(client, project_id)

        assert mocked_issue_context.issue.metadata["updated_at"] == str(
            datetime.now(tz=UTC)
        )

        # Verify we include the metadata YAML
        client.update_issue.assert_called_with(
            str(project_id),
            str(mocked_issue_context.issue.iid),
            dict(
                title=Contains("Capacity warning for"),
                description=ContainsTamlandMetadata,
                labels=new_labels,
                state_event="reopen",
            ),
        )

        # Let's also make sure we include the markdown from the uploaded image
        client.update_issue.assert_called_with(
            str(project_id),
            str(mocked_issue_context.issue.iid),
            dict(
                title=Contains("Capacity warning for"),
                description=Contains("![something](/uploads/something.png)"),
                labels=new_labels,
                state_event="reopen",
            ),
        )

        client.upload_project_file.assert_called_with(str(project_id), Any())

    @pytest.mark.freeze_time
    @mock.patch("tamland.reporting.issue_tracker.IssueLabeler")
    def test_record_last_reopened_at_time(
        self,
        project_id,
        mocked_issue_context,
        mock_issue,
    ):
        mock_issue.state = IssueState.CLOSED
        mocked_issue_context.issue = mock_issue

        action = UpdateIssue(mocked_issue_context)
        action.execute(Mock(), project_id)

    @pytest.mark.freeze_time
    @mock.patch("tamland.reporting.issue_tracker.IssueLabeler")
    def test_remove_dashed_keys_from_metadata(
        self,
        project_id,
        mocked_issue_context,
        mock_issue,
    ):
        mock_issue.state = IssueState.OPENED
        mocked_issue_context.issue = mock_issue

        mock_issue.metadata["last-reopened-at"] = "one"
        mock_issue.metadata["updated-at"] = "two"

        action = UpdateIssue(mocked_issue_context)
        action.execute(Mock(), project_id)

        # Not updated
        assert mock_issue.metadata.get("last-reopened-at") == "one"
        assert mock_issue.metadata.get("last_reopened_at") is None

        # Updated and dashed version removed
        assert mock_issue.metadata.get("updated-at") is None
        assert mock_issue.metadata.get("updated_at") == str(datetime.now(tz=UTC))

    @pytest.mark.freeze_time
    @mock.patch("tamland.reporting.issue_tracker.IssueLabeler")
    def test_yaml_autoescape_regression(
        self,
        issue_labeler_class,
        project_id,
        issue_payload,
        mocked_issue_context,
        mock_issue,
    ):
        client = Mock()

        client.upload_project_file.return_value = dict(
            markdown="![something](/uploads/something.png)"
        )

        mocked_issue_context.issue = mock_issue

        new_labels = Mock()

        labeler = Mock()
        issue_labeler_class.return_value = labeler
        labeler.labels.return_value = new_labels

        action = UpdateIssue(mocked_issue_context)

        action.execute(client, project_id)

        assert mocked_issue_context.issue.metadata["updated_at"] == str(
            datetime.now(tz=UTC)
        )

        # Verify we include the metadata YAML
        client.update_issue.assert_called_with(
            str(project_id),
            str(mocked_issue_context.issue.iid),
            dict(
                title=Any(),
                # This is where the regression happened:
                # If we autoescape the single-quotes, the YAML becomes unparseable.
                description=Contains(f"'{datetime.now(tz=UTC)}'"),
                labels=Any(),
                state_event="reopen",
            ),
        )


class TestCloseOrphanedIssue:
    def test_execute(self, project_id):
        client = Mock()
        action = CloseOrphanedIssue(42)

        action.execute(client, project_id)

        client.close_issue.assert_called_once_with(str(project_id), str(42))


class TestCloseIssue:
    def test_execute(self, project_id, mocked_issue_context, mock_issue):
        client = Mock()
        mocked_issue_context.issue = mock_issue
        action = CloseIssue(mocked_issue_context)

        action.execute(client, project_id)

        client.create_note.assert_called_once_with(
            str(project_id), str(mock_issue.iid), Contains("Closing automatically.")
        )
        client.close_issue.assert_called_once_with(str(project_id), str(mock_issue.iid))


class TestIssue_ParseMetadata:
    def test_parse_metadata(self, description):
        result = Issue.parse_metadata(dict(description=description))

        assert result["foo"] == "bar"

    def test_parse_empty(self):
        assert Issue.parse_metadata(dict()) == dict()

    def test_parse_no_match(self):
        assert Issue.parse_metadata(dict(description="no match here")) == dict()

    def test_parse_with_yaml_error(self, description):
        with patch("yaml.safe_load", side_effect=yaml.YAMLError("test exception")):
            assert Issue.parse_metadata(dict(description=description)) == dict()


class TestIssue:
    def test_from_payload(self, issue_payload):
        issue = Issue.from_payload(issue_payload)

        actual = issue.model_dump()

        for key in ("id", "iid", "title", "description", "state", "labels"):
            assert actual[key] == issue_payload[key]

        assert actual["metadata"]["component_ident"] == "test"

    def test_from_payload_regression(self, issue_payload):
        issue_payload["description"] = dedent(
            """
        We failed to parse the metadata from this example

        ```yaml
        # tamland
        component_ident: sidekiq.kube_container_rss.shard=urgent-other
        updated-at: &#39;2023-12-13 10:44:51.367188&#39;

        ```
        """
        )

        issue = Issue.from_payload(issue_payload)

        assert (
            issue.metadata.get("component_ident")
            == "sidekiq.kube_container_rss.shard=urgent-other"
        )

    def test_issue_age(self, issue_payload):
        issue_payload["closed_at"] = datetime(
            2024, 1, 28, 12, 52, 16, 887461, tzinfo=timezone.utc
        )
        issue = Issue.from_payload(issue_payload)

        with freeze_time("2024-03-25 15:50"):
            assert issue.time_since_closed.days == 57


class TestIssueTracker:
    def read_issue_payloads(self, project_id: str, labels: list[str], **_):
        with open(relative_path("tests/issue_tracker/issue-examples.yaml")) as fp:
            all = yaml.safe_load(fp)

        return [
            issue for issue in all if set(labels).issubset(set(issue.get("labels", [])))
        ]

    @pytest.fixture
    def client(self):
        client = Mock()
        client.get_issues = Mock(side_effect=self.read_issue_payloads)
        return client

    @pytest.fixture
    def tracker(self, client, issue_management_config, forecast_run_accessor):
        return IssueTracker(client, issue_management_config, forecast_run_accessor)

    def test_all_issues(self, tracker):
        all_issues = tracker.all_issues

        tracker.client.get_issues.assert_called_with(
            str(tracker.config.project_id),
            labels=tracker.config.scope_labels,
            state="all",
        )

        assert len(all_issues) == 7
        assert [issue.iid for issue in all_issues] == [1, 2, 4, 5, 7, 6, 8]

    def test_index(self, tracker):
        index = tracker.index

        assert {
            key: [issue.iid for issue in issues] for key, issues in index.items()
        } == {
            "test": [2, 1],
            "immortal": [8],
            "another_test": [4],
            "lonely_component": [5, 7],
            "purple": [6],
            # does not include unrelated issue iid 3
            # Does not include issues with different scope labels
            # (see tests/issue_tracker/issue_management_config.yml)
        }

    def make_component(self, str):
        component = Mock()
        component.__str__ = lambda _: str
        return component

    def test_process_single(self, tracker, capacity_alert):
        capacity_alert.status = AlertStatus.OPEN
        action = tracker.process(self.make_component("test"), capacity_alert)

        assert type(action) is UpdateIssue

    def test_process_multi(self, tracker, capacity_alert):
        # Alert open, issue open
        capacity_alert.status = AlertStatus.OPEN
        action = tracker.process(self.make_component("test"), capacity_alert)
        assert type(action) is UpdateIssue

        # Alert closed, issue open
        capacity_alert.status = AlertStatus.CLOSED
        action = tracker.process(self.make_component("another_test"), capacity_alert)
        assert type(action) is CloseIssue

        # A new component with an alert
        capacity_alert.status = AlertStatus.OPEN
        action = tracker.process(self.make_component("the_surprise"), capacity_alert)
        assert type(action) is CompositeAction
        assert [type(a) for a in action.actions] == [CreateIssue, UpdateIssue]

        # Issue open, alert unknown (= no forecast available)
        capacity_alert.status = AlertStatus.OPEN
        action = tracker.process(self.make_component("test"), None)
        assert type(action) is CloseIssue

        # Alert closed, issue open but with tamland:keep-open label
        capacity_alert.status = AlertStatus.CLOSED
        action = tracker.process(self.make_component("immortal"), capacity_alert)
        assert type(action) is UpdateIssue

        # No issue, no alert
        capacity_alert.status = AlertStatus.CLOSED
        action = tracker.process(
            self.make_component("some_unknown_issue"), capacity_alert
        )
        assert type(action) is NoAction

    def test_dont_close_issue_shortly_within_creation(self, tracker, capacity_alert):
        # Alert closed, issue open
        # Note this is 2 days after the created_at date of the issue, see issue-examples.yaml

        # FreezeGun and Pydantic don't work well together here.
        # We work around this by building the list of issues (including the Pydantic parsing)
        # first and then freeze time to run the tests.
        tracker.all_issues

        with freeze_time("2023-09-09 15:50"):
            capacity_alert.status = AlertStatus.CLOSED
            action = tracker.process(
                self.make_component("another_test"), capacity_alert
            )
            assert type(action) is UpdateIssue

    def test_dont_close_issue_shortly_after_reopening(self, tracker, capacity_alert):
        tracker.all_issues

        with freeze_time("2023-10-02 15:50"):
            capacity_alert.status = AlertStatus.CLOSED
            action = tracker.process(
                self.make_component("another_test"), capacity_alert
            )
            assert type(action) is UpdateIssue

    def test_reopen_recent_enough_issue(self, tracker, capacity_alert):
        # Closed issue recent enough, so we reopen it

        # FreezeGun and Pydantic don't work well together here.
        # We work around this by building the list of issues (including the Pydantic parsing)
        # first and then freeze time to run the tests.
        tracker.all_issues

        capacity_alert.status = AlertStatus.OPEN
        with freeze_time("2023-10-15 15:50:44.036044"):
            action = tracker.process(self.make_component("purple"), capacity_alert)
        assert action.ctx.issue.iid == 6
        assert type(action) is UpdateIssue

        # Does not re-open issue if it's closed_at date is too old
        with freeze_time("2023-11-15 15:50:44.036044"):
            action = tracker.process(self.make_component("purple"), capacity_alert)
        assert type(action) is CompositeAction
        assert [type(a) for a in action.actions] == [CreateIssue, UpdateIssue]

    def test_leaves_closed_issues_alone_without_new_warning(
        self, tracker, capacity_alert
    ):
        tracker.all_issues

        capacity_alert.status = AlertStatus.CLOSED
        with freeze_time("2023-10-15 15:50:44.036044"):
            action = tracker.process(self.make_component("purple"), capacity_alert)
        assert type(action) is NoAction

        capacity_alert = None
        with freeze_time("2023-10-15 15:50:44.036044"):
            action = tracker.process(self.make_component("purple"), capacity_alert)
        assert type(action) is NoAction

    def test_cleanup_actions(self, tracker, capacity_alert):
        self.test_process_multi(tracker, capacity_alert)

        actions = tracker.cleanup_actions()

        # Implicitly also verifies that we don't close already closed issues
        assert [(type(action), action.issue_iid) for action in actions] == [
            # Close for a component we don't know anymore
            (CloseOrphanedIssue, 5),
            # Close the older duplicate
            (CloseOrphanedIssue, 7),
        ]


class TestIssueLabeler:
    @pytest.fixture
    def ctx(self, mocked_issue_context, forecast, issue_management_config):
        mocked_issue_context.issue = Mock()
        mocked_issue_context.issue.labels = [
            "violation:foobar",
            "capacity-planning::priority::foobar",
            "some-other-label",
        ]
        mocked_issue_context.issue_management_config = issue_management_config
        mocked_issue_context.component.saturation_point.horizontally_scalable = False
        mocked_issue_context.alert = Mock()
        mocked_issue_context.alert.status = AlertStatus.OPEN
        mocked_issue_context.alert.rule_outcomes = []

        mocked_issue_context.accessor.get_forecast.return_value = forecast

        return mocked_issue_context


class TestIssueLabeler_PriorityLabels(TestIssueLabeler):
    def common_asserts(self, ctx):
        assert "capacity-planning::priority::foobar" not in IssueLabeler(ctx).labels()
        assert "some-other-label" in IssueLabeler(ctx).labels()

    def test_priority_do(self, ctx):
        ctx.alert.rule_outcomes.append(
            AlertingRuleOutcome(
                forecast=Mock(),
                triggered=True,
                description=Mock(),
                rule=AlertingRule_ConfidenceIntervalPrediction,
                slo=SLO_SATURATION,
            )
        )
        assert "capacity-planning::priority::do" in IssueLabeler(ctx).labels()
        self.common_asserts(ctx)

    def test_priority_delegate(self, ctx):
        ctx.alert.rule_outcomes.append(
            AlertingRuleOutcome(
                forecast=Mock(),
                triggered=True,
                description=Mock(),
                rule=Mock(),
                slo=SLO_SATURATION,
            )
        )
        ctx.component.saturation_point.horizontally_scalable = True
        assert "capacity-planning::priority::delegate" in IssueLabeler(ctx).labels()
        self.common_asserts(ctx)

    def test_priority_decide(self, ctx):
        ctx.alert.rule_outcomes.append(
            AlertingRuleOutcome(
                forecast=Mock(),
                triggered=True,
                description=Mock(),
                rule=Mock(),
                slo=SLO_SATURATION,
            )
        )
        ctx.component.saturation_point.horizontally_scalable = True
        assert "capacity-planning::priority::delegate" in IssueLabeler(ctx).labels()
        self.common_asserts(ctx)

    def test_priority_deny(self, ctx):
        ctx.alert.rule_outcomes.append(
            AlertingRuleOutcome(
                forecast=Mock(),
                triggered=True,
                description=Mock(),
                rule=Mock(),
                slo=SLO(name="notimportant", threshold=0.5),
            )
        )
        ctx.component.saturation_point.horizontally_scalable = True
        assert "capacity-planning::priority::deny" in IssueLabeler(ctx).labels()
        self.common_asserts(ctx)


class TestIssueLabeler_ScopeLabels(TestIssueLabeler):
    def test_scope_labels(self, ctx):
        assert "tenant::xyz" in IssueLabeler(ctx).labels()
        assert "another_scope_label" in IssueLabeler(ctx).labels()


class TestIssueLabeler_AdditionalLabels(TestIssueLabeler):
    def test_additional_labels(self, ctx):
        ctx.issue_management_config.additional_labels.append("additional-test-label")

        assert "additional-test-label" in IssueLabeler(ctx).labels()


class TestIssueLabeler_ViolationLabels(TestIssueLabeler):
    def common_asserts(self, ctx):
        assert "violation:foobar" not in IssueLabeler(ctx).labels()
        assert "some-other-label" in IssueLabeler(ctx).labels()

    def test_violation_soft(self, ctx, forecast):
        soft = Mock()
        soft.predicts_violations.return_value = True
        soft.slo.name = "soft"

        forecast.slo_violations = [soft]

        assert "violation:soft" in IssueLabeler(ctx).labels()
        self.common_asserts(ctx)

    def test_violation_soft_and_hard(self, ctx, forecast):
        soft = Mock()
        soft.predicts_violations.return_value = True
        soft.slo.name = "soft"

        hard = Mock()
        hard.predicts_violations.return_value = True
        hard.slo.name = "hard"

        forecast.slo_violations = [soft, hard]

        assert "violation:soft" in IssueLabeler(ctx).labels()
        assert "violation:hard" in IssueLabeler(ctx).labels()
        self.common_asserts(ctx)


def test_severity_label():
    assert severity_label("s1") == "severity::1"
    assert severity_label("s2") == "severity::2"
    assert severity_label(None) is None
