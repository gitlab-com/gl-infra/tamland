from datetime import datetime

import pytest
from dateutil.parser import parse as parse_date
from fs import open_fs
from fs.base import FS
from pandas import DataFrame, read_parquet
from pydantic_yaml import parse_yaml_raw_as

from tamland.core.alerting import (
    CapacityAlert,
    AlertStatus,
    AlertingRuleOutcome,
    AlertingRule,
)
from tamland.core.models import (
    Component,
    Service,
    SaturationPoint,
    CapacityPlanningParameters,
    SLO,
    Event,
    Reference,
    ManifestReader,
    PromDefaults,
)
from tamland.core.persistence import (
    RunPersistence,
    ForecastRun,
    ComponentForecastOnDisk,
    ForecastPersistence,
)
from tamland.core.prom_query import PromSelectors
from tamland.reporting.issue_tracker import IssueManagementConfig
from tamland.reporting.reporting import ForecastRunAccessor
from tamland.core.util import relative_path


@pytest.fixture(name="manifest_file")
def fixture_manifest_file() -> str:
    return relative_path("tests/data/fixture_manifest_example2.json")


@pytest.fixture(name="prometheus")
def prom_defaults(manifest_file: str) -> PromDefaults:
    return ManifestReader.from_file(manifest_file).defaults().prometheus


@pytest.fixture
def component(
    service: Service,
    saturation_point: SaturationPoint,
    capacity_planning: CapacityPlanningParameters,
    prometheus: PromDefaults,
) -> Component:
    return Component(
        service=service,
        saturation_point=saturation_point,
        capacity_planning=capacity_planning,
        saturation_ratio_query_template=prometheus.query_templates[
            capacity_planning.strategy
        ],
    )


@pytest.fixture
def component_with_events(
    service: Service,
    saturation_point: SaturationPoint,
    capacity_planning: CapacityPlanningParameters,
    prometheus: PromDefaults,
) -> Component:
    return Component(
        service=service,
        saturation_point=saturation_point,
        capacity_planning=capacity_planning,
        events=[
            Event(
                date=parse_date("2023-08-05"),
                name="TestEvent",
                references=[Reference(title="Test", ref="http://test.com")],
            ),
            Event(
                date=parse_date("2023-08-25"),
                name="TestEvent2",
                references=[Reference(title="Test", ref="http://test.com")],
            ),
        ],
        saturation_ratio_query_template=prometheus.query_templates[
            capacity_planning.strategy
        ],
    )


@pytest.fixture
def service(selectors: PromSelectors) -> Service:
    return Service(name="patroni", selectors=selectors, resource_dashboard={})


@pytest.fixture
def selectors() -> PromSelectors:
    return dict(env="gprd", environment="gprd", stage=["main", ""])


@pytest.fixture
def saturation_point(capacity_planning: CapacityPlanningParameters) -> SaturationPoint:
    return SaturationPoint(
        name="cpu",
        title="CPU usage",
        description="It's really only about CPU usage",
        severity="s2",
        horizontally_scalable=False,
        slos=[SLO(name="soft", threshold=0.8), SLO(name="hard", threshold=0.9)],
        capacity_planning=capacity_planning,
    )


@pytest.fixture(name="slo")
def fixture_slo() -> SLO:
    return SLO(name="soft", threshold=0.8)


@pytest.fixture
def capacity_planning() -> CapacityPlanningParameters:
    return CapacityPlanningParameters(
        strategy="quantile95_1h",
        to_dt=datetime.now(),
    )


@pytest.fixture
def history_dataframe() -> DataFrame:
    """A prometheus dataframe"""
    return read_parquet(
        relative_path("tests/data/fixture_prometheus_dataframe.parquet")
    )


@pytest.fixture
def memfs() -> FS:
    return open_fs("mem://")


@pytest.fixture
def result_examples_path() -> str:
    return relative_path("tests/data/result-examples")


@pytest.fixture
def run_persistence(result_examples_path) -> RunPersistence:
    return RunPersistence(fs=open_fs(result_examples_path))


@pytest.fixture
def forecast_persistence(result_examples_path) -> ForecastPersistence:
    return ForecastPersistence(fs=open_fs(result_examples_path))


@pytest.fixture
def forecast_run(run_persistence) -> ForecastRun:
    run = run_persistence.latest_run()
    assert run is not None
    return run


@pytest.fixture(name="forecast_run_accessor")
def fixture_forecast_run_accessor(run_persistence) -> ForecastRunAccessor:
    return ForecastRunAccessor(run_persistence._fs, run_persistence.latest_run())


@pytest.fixture(name="forecast_run_accessor_multiple_results")
def fixture_forecast_run_accessor_multiple_results() -> ForecastRunAccessor:
    """
    This is currently a huge figure loaded from production data with a date in the past.
    We should automate generating a fixture that has predictable content in
    https://gitlab.com/gitlab-com/gl-infra/tamland/-/issues/145
    """
    persistence = RunPersistence(fs=open_fs(relative_path("tests/data/result-full")))
    run = persistence.latest_run()
    assert run is not None
    return ForecastRunAccessor(persistence._fs, run)


@pytest.fixture
def forecast_on_disk(forecast_run_accessor) -> ComponentForecastOnDisk:
    manifest = forecast_run_accessor.manifest()
    component = next(
        c for c in manifest.components() if str(c) == "monitoring.cloudsql_disk"
    )
    return forecast_run_accessor.get_forecast(component)


@pytest.fixture
def capacity_alert(forecast_on_disk) -> CapacityAlert:
    outcome = AlertingRuleOutcome(
        forecast=forecast_on_disk,
        triggered=True,
        description="Most clever rule ever",
        rule=AlertingRule,
    )
    return CapacityAlert(
        forecast=forecast_on_disk, rule_outcomes=[outcome], status=AlertStatus.OPEN
    )


@pytest.fixture
def project_id() -> int:
    return 12345


@pytest.fixture
def issue_management_config():
    with open(
        relative_path("tests/issue_tracker/issue_management_config.yml"), "r"
    ) as fp:
        return parse_yaml_raw_as(IssueManagementConfig, fp.read())
