import os
from datetime import datetime, timedelta
from typing import AnyStr
from unittest import mock
from unittest.mock import patch, Mock

import pytest
from fs import open_fs
from fs.path import abspath, join, dirname
from pandas import DataFrame
from pydantic_yaml import parse_yaml_raw_as

from tamland.core.forecaster import (
    PredictedSLOViolation,
    ComponentForecastMetrics,
)
from tamland.core.models import Component, SLO
from tamland.core.persistence import (
    RunPersistence,
    ComponentForecastOnDisk,
    ForecastRun,
    ForecastRunner,
    HighLevelComponentMetrics,
    find_earliest_slo_violation,
    ForecastPersistence,
)


@pytest.mark.freeze_time("2023-08-10")
class TestForecastRunner:
    @mock.patch("tamland.core.forecaster.Forecaster.generate")
    @mock.patch("tamland.core.persistence.ForecastPersistence.save")
    @mock.patch("tamland.core.persistence.RunPersistence.save_run")
    def test_forecast_and_persist(
        self,
        mock_run_persistence,
        mock_forecast_persistence,
        mock_generate,
        component: Component,
        memfs,
        manifest_file,
    ):
        runner = ForecastRunner(fs=memfs)

        forecast = Mock("forecast")
        forecast.component = component
        forecast.metrics = ComponentForecastMetrics(confidence_range=0.4)
        forecast.slo_violations = [
            PredictedSLOViolation(
                slo=SLO(name="test", threshold=0.5),
                yhat_date=datetime(2023, 10, 1),
                yhat_upper_date=datetime(2023, 9, 25),
            ),
            PredictedSLOViolation(
                slo=SLO(name="test2", threshold=0.5),
                yhat_date=datetime(2023, 10, 12),
                yhat_upper_date=datetime(2023, 9, 24),
            ),
        ]
        mock_generate.return_value = forecast

        path = "foo/bar/test/path"
        mock_forecast_persistence.return_value = path

        run = runner.forecast_and_persist([component], manifest_path=manifest_file)

        assert run.finished_at is not None
        assert run.components[str(component)] == path
        assert run.metrics == [
            HighLevelComponentMetrics(
                component=str(component),
                confidence_range=0.4,
                earliest_slo_violation=datetime(2023, 9, 24),
            )
        ]

        mock_forecast_persistence.assert_called_once_with(run, forecast)
        with open(manifest_file, "r") as fp:
            mock_run_persistence.assert_called_once_with(
                run, {"manifest.json": fp.read()}
            )


@pytest.mark.freeze_time("2023-08-10")
class TestForecastAndPersistIntegrationTest:
    def test_forecast_and_persist(
        self,
        component: Component,
        history_dataframe: DataFrame,
        tmp_path,
        manifest_file,
    ):
        with patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = history_dataframe
            fs = open_fs("mem://")

            run = ForecastRunner(fs=fs).forecast_and_persist(
                [component], manifest_path=manifest_file
            )

            # Component forecast files
            forecast_dir = fs.opendir(run.components[str(component)])

            assert forecast_dir.isfile("forecast.yml")
            assert forecast_dir.isfile("forecast.png")
            assert forecast_dir.isfile("forecast_components.png")
            assert forecast_dir.isfile("forecast_debug.png")
            assert forecast_dir.isfile("history.parquet")
            assert forecast_dir.isfile("prophet-model.json.gz")

            with forecast_dir.open("forecast.yml") as fp:
                fod = parse_yaml_raw_as(ComponentForecastOnDisk, fp.read())

            assert fod.service == component.service.name
            assert fod.saturation_point == component.saturation_point.name
            assert fod.forecast_date == component.capacity_planning.to_dt
            assert fod.slo_violations == [
                PredictedSLOViolation(
                    slo=SLO(name="soft", threshold=0.8),
                    yhat_date=None,
                    yhat_upper_date=None,
                ),
                PredictedSLOViolation(
                    slo=SLO(name="hard", threshold=0.9),
                    yhat_date=None,
                    yhat_upper_date=None,
                ),
            ]

            # Uncertainty of prediction range is based on statistical simulation
            assert abs(fod.metrics.confidence_range - 0.53) <= 0.1

            forecast_run_manifest_dir = os.path.join(
                "forecasts",
                "manifests",
                "%s-%s" % (run.started_at.strftime("%Y%m%d-%H%M%S"), run.uuid),
            )

            # ForecastRun
            run_file = abspath(
                os.path.join(forecast_run_manifest_dir, "run-manifest.yml")
            )
            assert fs.isfile(run_file)
            with fs.open(run_file) as fp:
                run_from_disk = parse_yaml_raw_as(ForecastRun, fp.read())

                assert run_from_disk == run

            # ForecastRun: Artifact (manifest.json)
            artifact_file = abspath(
                os.path.join(forecast_run_manifest_dir, "manifest.json")
            )
            assert fs.isfile(artifact_file)

            # Inverse op: Use persistence to read ForecastRun from disk
            persistence = RunPersistence(fs=fs)
            available_runs = persistence.list_runs()
            assert run_file in available_runs
            assert persistence.read_run(run_file) == run
            assert persistence.latest_run() == run

            # Copy and read artifacts
            with persistence.open_artifact(run, "manifest.json") as fp, open(
                manifest_file, "r"
            ) as fp_fixture:
                assert fp.read() == fp_fixture.read()

            # Copy and read component forecast artifacts
            forecast_persistence = ForecastPersistence(fs=fs)
            with forecast_persistence.open_artifact(
                run, component, "forecast.png"
            ) as fp:
                assert len(fp.read()) > 0

            with forecast_persistence.open_artifact(run, component, "not_a_file") as fp:
                # This file does not exist
                assert fp is None


def test_find_earliest_slo_violation(slo):
    violations = [
        PredictedSLOViolation(
            slo=slo, yhat_date=datetime(2023, 10, 1), yhat_upper_date=None
        ),
        PredictedSLOViolation(
            slo=slo,
            yhat_date=datetime(2023, 9, 15),
            yhat_upper_date=datetime(2023, 11, 10),
        ),
    ]
    assert find_earliest_slo_violation(violations) == datetime(2023, 9, 15)

    violations = [PredictedSLOViolation(slo=slo, yhat_date=None, yhat_upper_date=None)]
    assert find_earliest_slo_violation(violations) is None

    assert find_earliest_slo_violation([]) is None


class TestComponentForecastOnDisk:
    @pytest.mark.freeze_time("2023-08-10")
    def test_predicted_violations(self):
        violations = [
            PredictedSLOViolation(
                slo=SLO(name="slo1", threshold=0.4),
                yhat_date=datetime.utcnow(),
                yhat_upper_date=None,
            ),
            PredictedSLOViolation(
                slo=SLO(name="slo2", threshold=0.2),
                yhat_date=datetime.utcnow(),
                yhat_upper_date=None,
            ),
            PredictedSLOViolation(
                slo=SLO(name="slo3", threshold=0.1),
                yhat_date=None,
                yhat_upper_date=None,
            ),
        ]

        forecast = ComponentForecastOnDisk(
            service="test",
            saturation_point="foo",
            forecast_date=datetime.utcnow(),
            slo_violations=violations,
        )

        assert forecast.predicted_violations() == {
            "slo2": PredictedSLOViolation(
                slo=SLO(name="slo2", threshold=0.2),
                yhat_date=datetime.utcnow(),
                yhat_upper_date=None,
            ),
            "slo1": PredictedSLOViolation(
                slo=SLO(name="slo1", threshold=0.4),
                yhat_date=datetime.utcnow(),
                yhat_upper_date=None,
            ),
        }


class TestRunPersistence:
    @pytest.fixture
    def persistence(self) -> RunPersistence:
        return RunPersistence(fs=open_fs("mem://"))

    @pytest.fixture
    def artifacts(self, manifest_file) -> dict[str, AnyStr]:
        with open(manifest_file) as fp:
            manifest = fp.read()

        return {"manifest.json": manifest}

    def test_latest_run(self, persistence: RunPersistence, artifacts):
        run1 = ForecastRun(started_at=datetime.utcnow() - timedelta(days=1))
        run2 = ForecastRun(started_at=datetime.utcnow())

        persistence.save_run(run1, artifacts)
        persistence.save_run(run2, artifacts)

        assert persistence.latest_run() == run2

    def test_latest_run_without_any_runs(self, persistence: RunPersistence):
        assert persistence.latest_run() is None

    def test_list_runs(self, persistence: RunPersistence, artifacts):
        run1 = ForecastRun(started_at=datetime.utcnow() - timedelta(days=1))
        run2 = ForecastRun(started_at=datetime.utcnow())

        persistence.save_run(run1, artifacts)
        persistence.save_run(run2, artifacts)

        assert persistence.list_runs() == [
            f"/forecasts/manifests/{run1.directory_name}/run-manifest.yml",
            f"/forecasts/manifests/{run2.directory_name}/run-manifest.yml",
        ]

    def test_read_run(self, persistence: RunPersistence, artifacts):
        run = ForecastRun(started_at=datetime.utcnow() - timedelta(days=1))
        path = persistence.save_run(run, artifacts)

        assert persistence.read_run(path) == run

    def test_save_run_copies_manifest(self, persistence: RunPersistence):
        run = ForecastRun(started_at=datetime.utcnow() - timedelta(days=1))
        path = persistence.save_run(run, artifacts={"manifest.json": "test"})

        assert persistence._fs.readtext(join(dirname(path), "manifest.json")) == "test"

    def test_save_raise_os_error(self, persistence: RunPersistence):
        error_msg = """
        [Errno 36] File name too long:
        '/tmp/forecast_debug_wide-ci-runners.cpu.shard!~"private|saas-linux-2xlarge-amd6
        4|saas-linux-large-amd64|saas-linux-large-arm64|saas-linux-medium-amd64|saas-lin
        ux-medium-amd64-gpu-standard|saas-linux-medium-arm64|saas-linux-small-amd64|saas
        -linux-xlarge-amd64|saas-macos-large-m2pro|saas-macos-medium-m1|shared-gitlab-or
        g|windows-shared"2m3ndupt.png'
        """.strip()
        run = ForecastRun(started_at=datetime.utcnow() - timedelta(days=1))

        with mock.patch.object(
            persistence._fs, "makedirs", side_effect=OSError(error_msg)
        ):
            with pytest.raises(OSError) as exc_info:
                persistence.save_run(run, artifacts={"manifest.json": "test"})

            assert str(exc_info.value) == error_msg
