from json import JSONDecodeError
from unittest import mock

import pytest
from callee import InstanceOf
from fs import open_fs

from tamland.reporting.issue_tracker import IssueManagementConfig
from tamland.reporting.reporting import ForecastRunAccessor
from tamland.shell import validate_manifest, manage_issues
from tamland.core.util import relative_path


class TestValidateManifest:
    def test_validate_manifest(self, manifest_file, capsys):
        validate_manifest(manifest=manifest_file)

        captured = capsys.readouterr()
        assert "OK" in captured.out

    def test_validate_manifest_invalid(self, manifest_file, capsys):
        with pytest.raises(JSONDecodeError):
            validate_manifest(manifest=__file__)
        captured = capsys.readouterr()
        assert "Invalid" in captured.out


class TestManageIssues:
    @mock.patch("tamland.shell.manage_capacity_warnings", autospec=True)
    def test_manage_issues(
        self, mock_manage_capacity_warnings, issue_management_config
    ):
        manage_issues(
            fs=relative_path("tests/data/result-examples"),
            config=relative_path("tests/issue_tracker/issue_management_config.yml"),
        )

        mock_manage_capacity_warnings.assert_called_once_with(
            InstanceOf(ForecastRunAccessor), issue_management_config
        )

        _, args, _ = mock_manage_capacity_warnings.mock_calls[0]

        run_accessor: ForecastRunAccessor = args[0]
        run_accessor.run_persistence._fs == open_fs(
            relative_path("tests/data/result-examples")
        )

    @mock.patch("tamland.shell.manage_capacity_warnings", autospec=True)
    def test_manage_issues_without_config(
        self,
        mock_manage_capacity_warnings,
        project_id,
    ):
        manage_issues(
            fs=relative_path("tests/data/result-examples"),
            project_id=project_id,
        )

        mock_manage_capacity_warnings.assert_called_once_with(
            InstanceOf(ForecastRunAccessor),
            IssueManagementConfig(project_id=project_id),
        )

        _, args, _ = mock_manage_capacity_warnings.mock_calls[0]

        run_accessor: ForecastRunAccessor = args[0]
        run_accessor.run_persistence._fs == open_fs(
            relative_path("tests/data/result-examples")
        )
