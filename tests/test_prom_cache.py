import datetime
import os
import pathlib
import unittest
from hashlib import sha256
from io import BytesIO
from unittest.mock import Mock

import fs
import pandas as pd
import pytest
from fs import open_fs

from tamland.core.prom_cache import (
    FSPromCacheManager,
    instant_series_column_name,
    PromQuery,
    MultiLevelCache,
)


class FSPromCacheManagerTestSuite(unittest.TestCase):
    """FSPromCacheManager unit test"""

    def setUp(self) -> None:
        self.query = """
            avg(quantile_over_time(0.95, gitlab_component_saturation:ratio{component="disk_inodes",env="gprd",environment="gprd",tier="db",type="patroni"}[1h]))
        """.strip()
        self.instant_query = "topk(3, tamland_forecast_date_timestamp_seconds)"
        self.cache_dir = os.path.join(
            pathlib.Path(__file__).parent.resolve(), "data/prom_cache"
        )
        self.query_cache_dir = sha256(self.query.encode("utf-8")).hexdigest()
        self.query_cache_dir_instant = sha256(
            self.instant_query.encode("utf-8")
        ).hexdigest()

        self.fs = open_fs("osfs://" + self.cache_dir)

    def read(self, query, from_dt, to_dt):
        return FSPromCacheManager(fs=self.fs).read(
            PromQuery(query=query, from_dt=from_dt, to_dt=to_dt)
        )

    def test_read_cache_hit(self):
        from_dt = datetime.date(2022, 2, 20)
        to_dt = datetime.date(2022, 2, 28)
        df = self.read(self.query, from_dt, to_dt)
        self.__assertPrometheusDataFrame(df, from_dt, to_dt)

    def test_read_cache_hit_instant(self):
        now = datetime.date(2022, 10, 19)
        df = self.read(self.instant_query, now, now)
        self.__assertPrometheusSeries(df, 3)

    def test_read_cache_missed_left(self):
        from_dt = datetime.date(2022, 2, 19)
        to_dt = datetime.date(2022, 2, 28)
        df = self.read(self.query, from_dt, to_dt)

        self.assertIsNone(df)

    def test_read_cache_missed_middle(self):
        from_dt = datetime.date(2022, 2, 18)  # 2022-02-19 is missing
        to_dt = datetime.date(2022, 2, 28)
        self.assertIsNone(self.read(self.query, from_dt, to_dt))

    def test_read_cache_missed_right(self):
        from_dt = datetime.date(2022, 2, 20)
        to_dt = datetime.date(2022, 3, 1)
        self.assertIsNone(self.read(self.query, from_dt, to_dt))

    def test_read_cache_hit_empty_instant(self):
        now = datetime.date(2002, 10, 20)
        df = self.read(self.instant_query, now, now)
        self.assertTrue(df.empty)

    def test_write_cache(self):
        from_dt = datetime.date(2022, 2, 20)
        to_dt = datetime.date(2022, 2, 22)
        df = self.read(self.query, from_dt, to_dt)

        prom_query = PromQuery(query=self.query, from_dt=from_dt, to_dt=to_dt)

        _fs = open_fs("mem://")
        manager = FSPromCacheManager(fs=_fs)
        manager.write(prom_query, df)

        self.assertListEqual(
            sorted(_fs.listdir(self.query_cache_dir)),
            [
                "2022-02-20_step3600",
                "2022-02-21_step3600",
                "2022-02-22_step3600",
                "_query",
            ],
        )

        with _fs.open(fs.path.combine(self.query_cache_dir, "_query")) as f:
            actual_query = f.read()
            self.assertEqual(actual_query, self.query)

        for day in pd.date_range(from_dt, to_dt, freq="D"):
            with _fs.open(
                fs.path.combine(
                    self.query_cache_dir, "%s_step3600" % day.strftime("%Y-%m-%d")
                ),
                "rb",
            ) as f:
                day_df = pd.read_parquet(BytesIO(f.read()))

            self.__assertPrometheusDataFrame(day_df, day, day)

    def test_write_cache_instant(self):
        now = datetime.date(2022, 10, 19)
        df = self.read(self.instant_query, now, now)

        prom_query = PromQuery(query=self.instant_query, from_dt=now, to_dt=now)

        _fs = open_fs("mem://")
        manager = FSPromCacheManager(fs=_fs)
        manager.write(prom_query, df)

        self.assertListEqual(
            sorted(_fs.listdir(self.query_cache_dir_instant)),
            ["2022-10-19_step3600_instant", "_query"],
        )

        with _fs.open(fs.path.combine(self.query_cache_dir_instant, "_query")) as f:
            actual_query = f.read()
            self.assertEqual(actual_query, self.instant_query)

        with _fs.open(
            fs.path.combine(
                self.query_cache_dir_instant, "2022-10-19_step3600_instant"
            ),
            "rb",
        ) as f:
            parquet_df = pd.read_parquet(f)

        self.__assertPrometheusSeries(parquet_df[instant_series_column_name], 3)

    def __assertPrometheusDataFrame(self, df, from_dt, to_dt):
        self.assertIsNotNone(df)

        hours = list(
            pd.date_range(
                from_dt, to_dt + datetime.timedelta(days=1), freq="h", inclusive="left"
            )
        )
        self.assertEqual(df.shape[0], len(hours))
        self.assertListEqual(list(df.index), hours)

    def __assertPrometheusSeries(self, series, rows):
        self.assertIsNotNone(series)

        self.assertEqual(series.shape, (rows,))
        self.assertEqual(series.name, instant_series_column_name)


class TestMultiLevelCache:
    @pytest.fixture
    def query(self) -> PromQuery:
        return PromQuery(
            query="something",
            from_dt=datetime.datetime(2023, 1, 1),
            to_dt=datetime.datetime(2023, 2, 1),
        )

    def test_read_with_backfill(self, query: PromQuery):
        result = Mock()

        cache1 = Mock()
        cache1.read.return_value = None
        cache2 = Mock()
        cache2.read.return_value = result

        cache = MultiLevelCache()
        cache.add(cache1)
        cache.add(cache2)

        assert cache.read(query) == result

        cache1.read.assert_called_once_with(query)
        cache2.read.assert_called_once_with(query)

        # Backfill happening
        cache1.write.assert_called_once_with(query, result)
        cache2.write.assert_not_called

    def test_read_with_hit_on_first_level(self, query: PromQuery):
        result = Mock()

        cache1 = Mock()
        cache1.read.return_value = result
        cache2 = Mock()

        cache = MultiLevelCache()
        cache.add(cache1)
        cache.add(cache2)

        assert cache.read(query) == result

        cache1.read.assert_called_once_with(query)
        cache2.read.assert_not_called

        # No backfill happening
        cache1.write.assert_not_called
        cache2.write.assert_not_called

    def test_write(self, query: PromQuery):
        df = Mock()

        cache1 = Mock()
        cache2 = Mock()

        cache = MultiLevelCache()
        cache.add(cache1)
        cache.add(cache2)

        cache.write(query, df)

        cache1.write.assert_called_once_with(query, df)
        cache2.write.assert_called_once_with(query, df)
