# noqa: E501
import math
import os
from datetime import timedelta, datetime, timezone
from unittest import mock
import pytest
from freezegun.api import FakeDatetime
from tamland.core import forecasting
from tamland.core import prom_query
import pydantic
from tamland.core.models import (
    ManifestReader,
    SaturationDimension,
    SaturationDimensionLookup,
    Service,
    SaturationPoint,
    Component,
    CapacityPlanningParameters,
    Event,
    Reference,
    ReportPage,
    MalformedManifest,
    ManifestParser,
    Outlier,
    Defaults,
    PromDefaults,
    Team,
)
from tamland.core.util import relative_path
from tests.test_helpers import (
    saturation_dimensions_lookup_time_series,
    each_is_instance_of_saturation_dimension,
)


class TestCapacityPlanningParameters:
    strategy = "quantile95_1h"

    def test_ignores_unknown_fields(self):
        CapacityPlanningParameters(
            strategy=self.strategy, unknown_field="foo", one_more_random_field="bar"
        )

    def test_forecast_days_default(self):
        assert (
            CapacityPlanningParameters(strategy=self.strategy).forecast_days
            == CapacityPlanningParameters.FORECAST_DAYS_DEFAULT
        )

    @mock.patch.dict(os.environ, {"TAMLAND_FORECAST_DAYS": "10"})
    def test_forecast_days_override(self):
        assert CapacityPlanningParameters(strategy=self.strategy).forecast_days == 10

    def test_forecast_days(self):
        assert (
            CapacityPlanningParameters(
                strategy=self.strategy, forecast_days=15
            ).forecast_days
            == 15
        )

    def test_historical_days_default(self):
        assert (
            CapacityPlanningParameters(strategy=self.strategy).historical_days
            == CapacityPlanningParameters.HISTORICAL_DAYS_DEFAULT
        )

    @mock.patch.dict(os.environ, {"TAMLAND_HISTORICAL_DAYS": "10"})
    def test_historical_days_override(self):
        assert CapacityPlanningParameters(strategy=self.strategy).historical_days == 10

    def test_historical_days(self):
        assert (
            CapacityPlanningParameters(
                strategy=self.strategy, historical_days=15
            ).historical_days
            == 15
        )

    def test_changepoints_count(self):
        assert (
            CapacityPlanningParameters(
                strategy=self.strategy, changepoints_count=5
            ).changepoints_count
            == 5
        )

    def test_changepoints_count_default(self):
        assert (
            CapacityPlanningParameters(strategy=self.strategy).changepoints_count
            == CapacityPlanningParameters.CHANGEPOINTS_COUNT_DEFAULT
        )

    def test_changepoints_default(self):
        assert CapacityPlanningParameters(strategy=self.strategy).changepoints == []

    def test_changepoints(self):
        changepoints = [datetime(2012, 1, 14), datetime(2012, 2, 20)]
        assert (
            CapacityPlanningParameters(
                strategy=self.strategy, changepoints=changepoints
            ).changepoints
            == changepoints
        )

    def test_to_dt(self):
        assert (
            CapacityPlanningParameters(strategy=self.strategy).to_dt
            == forecasting.find_now()
        )

    # Property methods below

    def test_from_dt(self):
        params = CapacityPlanningParameters(strategy=self.strategy, historical_days=10)
        assert params.from_dt == params.to_dt - timedelta(days=10)

    def test_forecast_to_dt(self):
        params = CapacityPlanningParameters(strategy=self.strategy, forecast_days=20)
        assert params.forecast_to_dt == params.to_dt + timedelta(days=20)

    def test_total_range(self):
        params = CapacityPlanningParameters(strategy=self.strategy)
        assert params.total_range == (params.from_dt, params.forecast_to_dt)

    def test_history_range(self):
        params = CapacityPlanningParameters(strategy=self.strategy)
        assert params.history_range == (params.from_dt, params.to_dt)

    def test_is_dynamic_dimensions_over_limit_given_default(self):
        params = CapacityPlanningParameters(strategy=self.strategy)
        assert params.is_dynamic_dimensions_over_limit(10) is False
        assert params.is_dynamic_dimensions_over_limit(20) is True

    def test_is_dynamic_dimensions_over_limit_given_custom_limit(self):
        params = CapacityPlanningParameters(
            strategy=self.strategy, saturation_dimension_dynamic_lookup_limit=42
        )
        assert params.is_dynamic_dimensions_over_limit(10) is False
        assert params.is_dynamic_dimensions_over_limit(43) is True

    def test_is_dynamic_dimensions_over_limit_given_unlimited_threshold(self):
        params = CapacityPlanningParameters(
            strategy=self.strategy, saturation_dimension_dynamic_lookup_limit=0
        )
        assert params.is_dynamic_dimensions_over_limit(666) is False
        assert params.is_dynamic_dimensions_over_limit(math.nan) is False

    def test_heterogeneous_saturation_dimensions(self):
        params = CapacityPlanningParameters(
            strategy=self.strategy,
            saturation_dimensions=[
                dict(selector='place="swamp"'),
                dict(selector='place="worcestershire-academy"', label="school"),
                SaturationDimension(selector='place="duloc"'),
                SaturationDimension(selector='place="dragons-keep"', label="fortress"),
            ],
        )
        assert each_is_instance_of_saturation_dimension(params.saturation_dimensions)

    def test_unhashable_saturation_dimension(self):
        params = CapacityPlanningParameters(
            strategy=self.strategy,
            saturation_dimensions=[
                dict(selector='shard="private"'),
                dict(selector='shard="shared-gitlab-org"'),
                dict(selector='shard="saas-linux-small-amd64"'),
                dict(selector='shard="saas-linux-medium-amd64"'),
                dict(selector='shard="saas-linux-medium-arm64"'),
                dict(selector='shard="saas-linux-medium-amd64-gpu-standard"'),
                dict(selector='shard="saas-linux-large-amd64"'),
                dict(selector='shard="saas-linux-large-arm64"'),
                dict(selector='shard="saas-linux-xlarge-amd64"'),
                dict(selector='shard="saas-linux-2xlarge-amd64"'),
                dict(selector='shard="saas-macos-medium-m1"'),
                dict(selector='shard="saas-macos-large-m2pro"'),
                dict(selector='shard="windows-shared"'),
                {
                    "label": "shard=rest-aggregated",
                    "selector": 'shard!~"private|saas-linux-2xlarge-amd64|saas-linux-large-amd64|saas-linux-large-arm64|saas-linux-medium-amd64|saas-linux-medium-amd64-gpu-standard|saas-linux-medium-arm64|saas-linux-small-amd64|saas-linux-xlarge-amd64|saas-macos-large-m2pro|saas-macos-medium-m1|shared-gitlab-org|windows-shared"',  # noqa:E501,
                },
            ],
        )
        assert each_is_instance_of_saturation_dimension(params.saturation_dimensions)


class TestComponent:
    @pytest.fixture(name="service")
    def fixture_service(self) -> Service:
        return Service(name="test-service", selectors=dict(env="gprd"))

    @pytest.fixture(name="saturation_point")
    def fixture_saturation_point(self) -> SaturationPoint:
        return SaturationPoint(
            name="memory",
            title="",
            description="",
            slos=[],
            severity="s3",
            capacity_planning=CapacityPlanningParameters(strategy="quantile95_1h"),
            horizontally_scalable=True,
            raw_query="some_query(%(selector)s)",
        )

    @pytest.fixture(name="component")
    def fixture_component(self, service, saturation_point) -> Component:
        return Component(
            service=service,
            saturation_point=saturation_point,
            capacity_planning=CapacityPlanningParameters(strategy="quantile95_1h"),
            saturation_ratio_query_template="max(quantile_over_time(0.95, gitlab_component_saturation:ratio{%s}[1h]))",
        )

    def test_selectors_dict(self, component):
        assert component.selectors == dict(
            component="memory", type="test-service", env="gprd"
        )

    def test_selectors_with_dimension(self, component_with_dimension):
        expected_selectors = dict(
            component="memory",
            type="test-service",
            env="gprd",
        )

        assert component_with_dimension.selectors == expected_selectors

    def test_saturation_ratio_query(self, component):
        assert (
            component.saturation_ratio_query
            != component.saturation_ratio_query_template
        )
        series = prom_query.selector_for(component.selectors)
        assert series in component.saturation_ratio_query

    def test_saturation_ratio_query_includes_custom_dimension(
        self, component_with_dimension
    ):
        assert (
            f",{component_with_dimension.dimension}"
            in component_with_dimension.saturation_ratio_query
        )

    def test_saturation_ratio_query_using_raw_promql_query(
        self, service, saturation_point
    ):
        saturation_ratio_query_template = "mischievous_metric{%s}"
        component = Component(
            service=service,
            saturation_point=saturation_point,
            capacity_planning=CapacityPlanningParameters(
                strategy="quantile95_1h", experiment_saturation_ratio_raw_query=True
            ),
            saturation_ratio_query_template=saturation_ratio_query_template,
            dimension=SaturationDimension(selector='region="us-east-1"'),
        )
        assert "mischievous_metric" not in component.saturation_ratio_query
        assert component.raw_query in component.saturation_ratio_query

    def test_raw_query(self, component):
        assert component.raw_query == 'some_query(type="test-service",env="gprd")'

    def test_raw_query_includes_custom_dimension(self, component_with_dimension):
        assert (
            component_with_dimension.raw_query
            == 'some_query(type="test-service",env="gprd",quotaregion="us-east-1")'
        )

    def test_title(self, component):
        assert component.title == "test-service Service, memory"

    def test_title_with_saturation_dimension(self, component_with_dimension):
        assert (
            component_with_dimension.title
            == 'test-service Service, memory.quotaregion="us-east-1"'
        )

    @pytest.fixture(name="component_with_dimension")
    def fixture_component_with_dimension(self, service, saturation_point) -> Component:
        return Component(
            service=service,
            saturation_point=saturation_point,
            capacity_planning=CapacityPlanningParameters(strategy="quantile95_1h"),
            saturation_ratio_query_template="some_metric{%s}",
            dimension=SaturationDimension(selector='quotaregion="us-east-1"'),
        )

    def test_title_with_dimension(self, component_with_dimension):
        assert (
            component_with_dimension.title
            == 'test-service Service, memory.quotaregion="us-east-1"'
        )

    @pytest.fixture(name="component_with_labeled_dimension")
    def fixture_component_with_labeled_dimension(
        self, service, saturation_point
    ) -> Component:
        return Component(
            service=service,
            saturation_point=saturation_point,
            capacity_planning=CapacityPlanningParameters(strategy="quantile95_1h"),
            saturation_ratio_query_template="some_metric{%s}",
            dimension=SaturationDimension(
                label="shard=*", selector="shard!~shard1|shard2|shard3|shard4"
            ),
        )

    def test_title_with_labeled_dimension(self, component_with_labeled_dimension):
        assert (
            component_with_labeled_dimension.title
            == "test-service Service, memory.shard=*"
        )


class TestReportPage:
    def test_slug(self):
        assert ReportPage(title="test", service_pattern="", path="foo.md").slug == "foo"


class TestManifestReaderWithFixture:
    @pytest.fixture(name="components")
    def fixture_components(self, manifest_file):
        return {
            (c.service.name, c.saturation_point.name): c
            for c in ManifestReader.from_file(manifest_file).components()
        }

    @pytest.fixture(name="all_components")
    def fixture_all_components(self, manifest_file):
        return [c for c in ManifestReader.from_file(manifest_file).components()]

    @pytest.fixture(name="manifest_file_duplicate_component")
    def fixture_manifest_file_duplicate_component(self) -> str:
        return relative_path("tests/data/fixture_manifest_duplicate_component.json")

    def test_reader_from_io(self, manifest_file):
        with open(manifest_file) as fp:
            ManifestReader.from_io(fp).data == ManifestReader.from_file(
                manifest_file
            ).data

    def test_defaults(self, manifest_file):
        expected_defaults = Defaults(
            prometheus=PromDefaults(
                baseURL="http://localhost:9292",
                serviceLabel="type",
                defaultSelectors={"env": "gprd", "stage": "main"},
                queryTemplates={
                    "quantile95_1h": "max(quantile_over_time(0.95, gitlab_component_saturation:ratio{%s}[1h]))",
                    "quantile95_1w": "max(gitlab_component_saturation:ratio_quantile95_1w{%s})",
                    "quantile99_1w": "max(gitlab_component_saturation:ratio_quantile99_1w{%s})",
                },
            ),
        )
        assert ManifestReader.from_file(manifest_file).defaults() == expected_defaults

    def test_duplicate_components(self, manifest_file_duplicate_component):
        with pytest.raises(pydantic.ValidationError) as exc:
            ManifestReader.from_file(manifest_file_duplicate_component)
        assert exc.value is not None

    def test_pages(self, manifest_file):
        pages = list(ManifestReader.from_file(manifest_file).pages())

        assert pages == [
            ReportPage(
                path="api-git-web.md",
                title="API, Git, and Web",
                service_pattern="api|git|internal-api|web|websockets",
            ),
            ReportPage(
                path="ci-runners.md", title="CI Runners", service_pattern="ci-runners"
            ),
        ]

    def test_load_services(self, manifest_file):
        services = [
            (s.name, s.selectors)
            for s in ManifestReader.from_file(manifest_file).services()
        ]

        assert services == [
            ("patroni", dict(env="gprd", stage="main")),
            ("patroni-ci", dict(env="gprd", stage="main")),
            ("monitoring", dict(env="gprd", stage="main")),
            # env=thanos overriden in service, stage comes from defaults
            ("some-other-service", dict(env="thanos", stage="main")),
        ]

    def test_teams(self, manifest_file):
        teams = list(ManifestReader.from_file(manifest_file).teams())

        assert teams == [
            Team(name="sre_reliability"),
            Team(
                name="reliability_database_reliability",
                manager="foobar",
                label="team::Database Reliability",
            ),
        ]

    def test_team_lookup(self, manifest_file):
        manifest = ManifestReader.from_file(manifest_file)

        team = Team(
            name="reliability_database_reliability",
            manager="foobar",
            label="team::Database Reliability",
        )

        assert manifest.team("reliability_database_reliability") == team
        assert manifest.team("unknown") is None

    def test_services_label(self, manifest_file):
        services = [
            (s.name, s.label)
            for s in ManifestReader.from_file(manifest_file).services()
        ]

        assert services == [
            ("patroni", "Patroni"),
            ("patroni-ci", "Patroni"),
            ("monitoring", "Prometheus"),
            ("some-other-service", "SomeOtherService"),
        ]

    def test_get_single_service(self, manifest_file):
        assert (
            ManifestReader.from_file(manifest_file).service("patroni").name == "patroni"
        )
        assert ManifestReader.from_file(manifest_file).service("notfound") is None

    def test_service_events(self, manifest_file):
        services = {
            s.name: s for s in ManifestReader.from_file(manifest_file).services()
        }

        assert services["patroni"].events == [
            Event(
                date=datetime(2023, 6, 10),
                name="Upgrade of PG database cluster",
                references=[
                    Reference(
                        title="Production change",
                        ref="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/11375",
                    )
                ],
            )
        ]

    def test_service_owner(self, manifest_file):
        services = {
            s.name: s.owner for s in ManifestReader.from_file(manifest_file).services()
        }

        assert services == {
            "patroni": Team(
                name="reliability_database_reliability",
                manager="foobar",
                label="team::Database Reliability",
            ),
            "patroni-ci": Team(
                name="reliability_database_reliability",
                manager="foobar",
                label="team::Database Reliability",
            ),
            "monitoring": None,
            "some-other-service": Team(
                name="sre_reliability", manager=None, label=None
            ),
        }

    def test_saturation_point_with_exclusion(self, manifest_file):
        """All points loaded, except excluded ones (strategy == "exclude")"""
        points = [
            p.name
            for p, _ in ManifestReader.from_file(manifest_file).saturation_points()
        ]
        assert points == [
            "pg_btree_bloat",
            "pg_int4_id",
            "node_schedstat_waiting",
            "kube_horizontalpodautoscaler_desired_replicas",
            "gcp_quota_limit",
        ]

    def test_saturation_point_details(self, manifest_file):
        points = {
            p.name: (p, a)
            for p, a in ManifestReader.from_file(manifest_file).saturation_points()
        }
        point, applies_to = points["pg_btree_bloat"]

        slos = {slo.name: slo.threshold for slo in point.slos}

        assert slos["soft"] == 0.5
        assert slos["hard"] == 0.7

        # This is auto-added, it's not in the manifest
        assert slos["saturation"] == 1.0

        assert point.title == "Postgres btree bloat"
        assert (
            "This estimates the total bloat in Postgres Btree indexes"
            in point.description
        )
        assert point.horizontally_scalable is False

        assert point.raw_query == "some_query(%(selector)s})[1h]))"

        params = point.capacity_planning

        assert params == CapacityPlanningParameters(
            strategy="quantile95_1h",
            changepoints_count=5,
            forecast_days=90,
            historical_days=365,
        )

        assert applies_to == [
            "patroni",
            "patroni-registry",
            "patroni-embedding",
            "patroni-ci",
            "postgres-archive",
        ]

    def test_components_high_level(self, components):
        assert set(components.keys()) == set(
            [
                ("patroni", "pg_btree_bloat"),
                ("patroni-ci", "pg_btree_bloat"),
                ("patroni", "pg_int4_id"),
                ("patroni", "node_schedstat_waiting"),
                ("patroni-ci", "node_schedstat_waiting"),
                ("monitoring", "gcp_quota_limit"),
                ("monitoring", "kube_horizontalpodautoscaler_desired_replicas"),
                ("monitoring", "node_schedstat_waiting"),
            ]
        )

    def test_components_with_custom_dimensions(self, all_components):
        components_with_dimensions = {
            str(c) for c in all_components if c.dimension is not None
        }
        service_dimensions = {
            'monitoring.gcp_quota_limit.region="us-central1"',
            'monitoring.gcp_quota_limit.region="us-east1"',
            'monitoring.node_schedstat_waiting.region="us-central1"',
            'monitoring.node_schedstat_waiting.region="us-east1"',
            "monitoring.node_schedstat_waiting.region=aggregated",
            'monitoring.kube_horizontalpodautoscaler_desired_replicas.region="us-central1"',
            'monitoring.kube_horizontalpodautoscaler_desired_replicas.region="us-east1"',
            'patroni.node_schedstat_waiting.shard="shard1"',
            'patroni.node_schedstat_waiting.shard="shard2"',
            "patroni.node_schedstat_waiting.region=aggregated",
            "patroni-ci.node_schedstat_waiting.region=aggregated",
            'patroni.pg_btree_bloat.shard="shard1"',
            'patroni.pg_btree_bloat.shard="shard2"',
            'patroni.pg_int4_id.shard="shard1"',
            'patroni.pg_int4_id.shard="shard2"',
        }
        saturation_point_dimensions = {
            "monitoring.gcp_quota_limit.not-gecko-us-east",
            "monitoring.gcp_quota_limit.gecko-us-east",
        }
        expected = service_dimensions | saturation_point_dimensions

        assert components_with_dimensions == expected

    @pytest.fixture(name="dynamic_lookup_manifest")
    def dynamic_lookup_manifest(self):
        return relative_path("tests/data/fixture_manifest_dynamic_lookup.json")

    @pytest.mark.freeze_time("2024-09-20")
    def test_components_with_saturation_dimensions_dynamically_lookedup(
        self, dynamic_lookup_manifest
    ):
        with mock.patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = (
                saturation_dimensions_lookup_time_series()
            )

            components = {
                str(c)
                for c in ManifestReader.from_file(dynamic_lookup_manifest).components()
            }

            mock_batched_query_range.assert_called_with(
                'sum by(shard) (\n  last_over_time(gitlab_component_saturation:ratio{component="sidekiq_thread_contention", env="gprd",environment="gprd",stage=~"main|"}[1w])\n)\n',  # noqa:E501
                FakeDatetime(2023, 9, 21, 0, 0, tzinfo=timezone.utc),
                FakeDatetime(2024, 9, 20, 0, 0, tzinfo=timezone.utc),
            )

            assert components == {
                'sidekiq.kube_container_cpu.shard="urgent-other"',
                'sidekiq.kube_container_cpu.shard="memory-bound"',
                # statically defined dimension:
                'sidekiq.sidekiq_thread_contention.shard="urgent-other"',
                # dynamically looked up dimension:
                'sidekiq.sidekiq_thread_contention.shard="catchall"',
                # deduplicated dimension present both on looked up dimension and statically defined one:
                'sidekiq.sidekiq_thread_contention.shard="memory-bound"',
            }

    @pytest.mark.freeze_time("2024-09-20")
    def test_components_with_saturation_dimensions_dynamically_lookedup_over_limit(
        self, dynamic_lookup_manifest
    ):
        with mock.patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = (
                saturation_dimensions_lookup_time_series(
                    time_series_default=[
                        {'{shard="disk1"}': 1},
                        {'{shard="disk2"}': 1},
                        {'{shard="disk3"}': 1},
                        {'{shard="disk4"}': 1},
                        {'{shard="disk5"}': 1},
                        {'{shard="disk6"}': 1},
                        {'{shard="disk7"}': 1},
                        {'{shard="disk8"}': 1},
                        {'{shard="disk9"}': 1},
                        {'{shard="disk10"}': 1},
                        {'{shard="disk11"}': 1},
                        {'{shard="disk12"}': 1},
                    ]
                )
            )

            components = {
                str(c)
                for c in ManifestReader.from_file(dynamic_lookup_manifest).components()
            }

            mock_batched_query_range.assert_called_with(
                'sum by(shard) (\n  last_over_time(gitlab_component_saturation:ratio{component="sidekiq_thread_contention", env="gprd",environment="gprd",stage=~"main|"}[1w])\n)\n',  # noqa:E501
                FakeDatetime(2023, 9, 21, 0, 0, tzinfo=timezone.utc),
                FakeDatetime(2024, 9, 20, 0, 0, tzinfo=timezone.utc),
            )

            static_dimensions = {
                'sidekiq.kube_container_cpu.shard="urgent-other"',
                'sidekiq.kube_container_cpu.shard="memory-bound"',
                'sidekiq.sidekiq_thread_contention.shard="urgent-other"',
                'sidekiq.sidekiq_thread_contention.shard="memory-bound"',
            }
            assert static_dimensions.issubset(components)

            non_skipped_dynamic_dimensions = {
                'sidekiq.sidekiq_thread_contention.shard="disk1"',
                'sidekiq.sidekiq_thread_contention.shard="disk10"',
                'sidekiq.sidekiq_thread_contention.shard="disk11"',
                'sidekiq.sidekiq_thread_contention.shard="disk12"',
                'sidekiq.sidekiq_thread_contention.shard="disk2"',
                'sidekiq.sidekiq_thread_contention.shard="disk3"',
                'sidekiq.sidekiq_thread_contention.shard="disk4"',
                'sidekiq.sidekiq_thread_contention.shard="disk5"',
                'sidekiq.sidekiq_thread_contention.shard="disk6"',
                'sidekiq.sidekiq_thread_contention.shard="disk7"',
                # 'sidekiq.sidekiq_thread_contention.shard="disk8"', -- we expect to skip this one!
                # 'sidekiq.sidekiq_thread_contention.shard="disk9"', -- and this one too!
            }

            expected_dimensions = static_dimensions | non_skipped_dynamic_dimensions
            assert components == expected_dimensions

    def test_components_without_aggregated_service_dimensions(self, all_components):
        components = {
            str(c)
            for c in all_components
            if c.service.name == "monitoring"
            and c.saturation_point.name == "gcp_quota_limit"
        }
        expected = {
            'monitoring.gcp_quota_limit.region="us-central1"',
            'monitoring.gcp_quota_limit.region="us-east1"',
            "monitoring.gcp_quota_limit.gecko-us-east",
            "monitoring.gcp_quota_limit.not-gecko-us-east",
        }
        assert components == expected

    def test_component_override(self, components):
        assert (
            components[("patroni", "pg_btree_bloat")].capacity_planning.forecast_days
            == 180
        )  # component specific override, later overrides in the doc have precedence
        assert (
            components[("patroni-ci", "pg_btree_bloat")].capacity_planning.forecast_days
            == 90
        )  # saturation point default

    def test_component_events(self, components):
        assert components[("patroni-ci", "pg_btree_bloat")].events == [
            Event(
                date=datetime(2023, 3, 20),
                name="Something happened in history",
                references=[
                    Reference(
                        title="More details here", ref="https://gitlab.com/foo/bar"
                    )
                ],
            ),
            Event(
                date=datetime(2023, 5, 1),
                name="Another event but in a different object",
            ),
        ]

    def test_get_single_component(self, manifest_file):
        manifest_reader = ManifestReader.from_file(manifest_file)
        assert (
            manifest_reader.component_by(
                key="patroni-ci.node_schedstat_waiting"
            ).saturation_point.title
            == "Node Scheduler Waiting Time"
        )
        assert manifest_reader.component_by(key="does not exist") is None

    def test_saturation_point_applies_to(self, components):
        assert components.get(("patroni", "pg_int4_id")) is not None
        assert components.get(("patroni-ci", "pg_int4_id")) is None

    def test_title_fallback(self, components):
        assert (
            components.get(("patroni", "pg_btree_bloat")).saturation_point.title
            == "Postgres btree bloat"
        )
        assert (
            components.get(("patroni", "pg_int4_id")).saturation_point.title
            == "pg_int4_id saturation"
        )


class TestManifestParser:
    def test_invalid_changepoints(self):
        invalid_changepoints = ["2023-14-26", "2023-12-32", "2o23-01-01", "x", ""]
        for input in invalid_changepoints:
            with pytest.raises(MalformedManifest) as invalidness_error:
                ManifestParser.changepoint(input)
            assert f"Invalid changepoint={input}" in str(invalidness_error)

    def test_valid_changepoint(self):
        assert ManifestParser.changepoint("2023-01-01") == datetime(2023, 1, 1)

    def test_invalid_outliers(self):
        invalid_outliers = [
            {"start": "2023-14-26", "end": "2023-12-27"},
            {"start": "2023-12-26", "end": "2023-12-32"},
            {"start": "2o23-01-01", "end": "2023-01-01"},
            {"start": "", "end": "2023-01-01"},
            {"start": "2023-01-01", "end": "x"},
        ]
        for input in invalid_outliers:
            with pytest.raises(MalformedManifest) as invalidness_error:
                ManifestParser.outlier(input)
            assert f"Invalid outlier={input}" in str(invalidness_error)

    def test_valid_outlier(self):
        values = {"start": "2023-01-01", "end": "2023-01-15"}
        assert ManifestParser.outlier(values) == Outlier(
            start=datetime(2023, 1, 1), end=datetime(2023, 1, 15)
        )


class TestSaturationDimension:
    def test_saturation_dimension_from_str(self):
        selector = 'shard="shard1"'
        dimension = SaturationDimension.model_validate(dict(selector=selector))
        assert isinstance(dimension, SaturationDimension)
        assert dimension.selector == selector
        assert dimension.label is None

    def test_saturation_dimension_from_dict(self):
        input_dict = {"selector": 'shard="*"', "label": "shard=catchall"}
        dimension = SaturationDimension.model_validate(input_dict)
        assert isinstance(dimension, SaturationDimension)
        assert dimension.selector == 'shard="*"'
        assert dimension.label == "shard=catchall"

    def test_saturation_dimension_invalid_input(self):
        with pytest.raises(ValueError):
            SaturationDimension.model_validate(123)

    def test_saturation_dimension_str(self):
        selector = 'shard="shard1"'
        label = 'shard="catchall"'
        assert str(SaturationDimension(selector=selector)) == selector
        assert str(SaturationDimension(selector=selector, label=label)) == label

    def test_saturation_dimension_equality_against_same_object_type(self):
        selector = 'shard="shard1"'
        dim_a = SaturationDimension(selector=selector)
        dim_b = SaturationDimension(selector=selector)
        assert dim_a == dim_b

    def test_saturation_dimension_equality_against_str(self):
        selector = 'shard="shard1"'
        assert SaturationDimension(selector=selector) == selector

    def test_saturation_dimension_hash(self):
        selector = 'shard="*"'
        label = 'shard="catchall"'
        assert hash(SaturationDimension(selector=selector)) == hash(selector)
        assert hash(SaturationDimension(selector=selector, label=label)) == hash(label)


class TestSaturationDimensionLookup:
    @pytest.fixture(name="dynamic_dimension_promql")
    def fixture_dynamic_dimension_promql(self):
        return 'sum by(shard) (\n  last_over_time(gitlab_component_saturation:ratio{component="sidekiq_thread_contention", env="gprd",environment="gprd",stage=~"main|"}[1w])\n)\n'  # noqa:E501

    def saturation_point(self, params: CapacityPlanningParameters):
        return SaturationPoint(
            name="test-point",
            title="Test Point",
            description="A test saturation point",
            severity="s3",
            horizontally_scalable=True,
            slos=[],
            capacity_planning=params,
        )

    def capacity_planning(self, input: dict = {}):
        input.setdefault("strategy", "quantile95_1h")
        input.setdefault(
            "saturation_dimensions",
            [dict(selector="shard=1"), dict(selector="shard=2")],
        )
        return CapacityPlanningParameters(**input)

    def test_query_without_dynamic_dimensions(self, service):
        params = self.capacity_planning()
        lookup = SaturationDimensionLookup(service, self.saturation_point(params))
        assert lookup.query(params) == params.saturation_dimensions

    @pytest.mark.freeze_time("2024-09-20")
    @mock.patch.dict(os.environ, {"TAMLAND_DISABLE_DYNAMIC_DIMENSION_LOOKUP": "TRUE"})
    def test_query_with_dynamic_lookup_disabled(
        self, service, dynamic_dimension_promql
    ):
        params = self.capacity_planning(
            {"saturation_dimension_dynamic_lookup_query": dynamic_dimension_promql}
        )
        lookup = SaturationDimensionLookup(service, self.saturation_point(params))

        # We don't expect to see any dimensions added from the dynamic lookup
        assert lookup.query(params) == params.saturation_dimensions

    @pytest.mark.freeze_time("2024-09-20")
    def test_query_with_dynamic_dimensions_within_limit(
        self, service, dynamic_dimension_promql
    ):
        params = self.capacity_planning(
            {"saturation_dimension_dynamic_lookup_query": dynamic_dimension_promql}
        )
        lookup = SaturationDimensionLookup(service, self.saturation_point(params))

        with mock.patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = (
                saturation_dimensions_lookup_time_series()
            )

            dimensions = lookup.query(params)

            mock_batched_query_range.assert_called_with(
                dynamic_dimension_promql,
                FakeDatetime(2023, 9, 21, 0, 0, tzinfo=timezone.utc),
                FakeDatetime(2024, 9, 20, 0, 0, tzinfo=timezone.utc),
            )

            expected_dimensions = params.saturation_dimensions | {
                'shard="catchall"',
                'shard="memory-bound"',
            }
            assert dimensions == expected_dimensions
            assert each_is_instance_of_saturation_dimension(dimensions)

    @pytest.mark.freeze_time("2024-09-20")
    def test_query_with_dynamic_dimensions_above_limit(
        self, service, dynamic_dimension_promql
    ):
        params = self.capacity_planning(
            {"saturation_dimension_dynamic_lookup_query": dynamic_dimension_promql}
        )
        lookup = SaturationDimensionLookup(service, self.saturation_point(params))

        with mock.patch(
            "tamland.core.prom_query.batched_query_range"
        ) as mock_batched_query_range:
            mock_batched_query_range.return_value = (
                saturation_dimensions_lookup_time_series(
                    time_series_default=[
                        {'{shard="disk1"}': 1},
                        {'{shard="disk2"}': 1},
                        {'{shard="disk3"}': 1},
                        {'{shard="disk4"}': 1},
                        {'{shard="disk5"}': 1},
                        {'{shard="disk6"}': 1},
                        {'{shard="disk7"}': 1},
                        {'{shard="disk8"}': 1},
                        {'{shard="disk9"}': 1},
                        {'{shard="disk10"}': 1},
                        {'{shard="disk11"}': 1},
                        {'{shard="disk12"}': 1},
                    ]
                )
            )

            dimensions = lookup.query(params)

            mock_batched_query_range.assert_called_with(
                dynamic_dimension_promql,
                FakeDatetime(2023, 9, 21, 0, 0, tzinfo=timezone.utc),
                FakeDatetime(2024, 9, 20, 0, 0, tzinfo=timezone.utc),
            )

            expected_dimensions = params.saturation_dimensions | {
                'shard="disk1"',
                'shard="disk10"',
                'shard="disk11"',
                'shard="disk12"',
                'shard="disk2"',
                'shard="disk3"',
                'shard="disk4"',
                'shard="disk5"',
                'shard="disk6"',
                'shard="disk7"',
                # 'shard="disk8"', -- we expect to skip this one!
                # 'shard="disk9"', -- and this one too!
            }
            assert dimensions == expected_dimensions
            assert each_is_instance_of_saturation_dimension(dimensions)
