import os
from unittest.mock import patch

import pytest
from tamland.core import feature_flag


class TestIsEnabled:
    @pytest.mark.parametrize("test_input", ["1", "t", "true", "TRUE", "True"])
    def test_flag_is_enabled(self, test_input):
        with patch.dict(os.environ, {"FEATURE_MY_FLAG": test_input}):
            assert feature_flag.is_enabled("MY_FLAG")

    @pytest.mark.parametrize("test_input", ["1", "t", "true", "TRUE", "True"])
    def test_flag_is_enabled_given_custom_value(self, test_input):
        with patch.dict(os.environ, {"FEATURE_MY_FLAG": test_input}):
            assert feature_flag.is_enabled("MY_FLAG", test_input)

    @pytest.mark.parametrize(
        "test_input", ["0", "f", "false", "False", "FALSE", "random-asdf"]
    )
    def test_flag_is_disabled(self, test_input):
        with patch.dict(os.environ, {"FEATURE_MY_FLAG": test_input}):
            assert not feature_flag.is_enabled("MY_FLAG")
