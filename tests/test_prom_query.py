from unittest.mock import patch, call
from tamland.core import prom_query
import unittest
import pandas as pd
import datetime

from tamland.core.prom_cache import PromQuery


class PromBatchedQueryRangeTestSuite(unittest.TestCase):
    """batched_query_range unit test"""

    def setUp(self):
        self.query = "query does not matter here"
        # fmt: off
        self.test_data = [
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 27), datetime.date(2022, 2, 28), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 26), datetime.date(2022, 2, 27), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 25), datetime.date(2022, 2, 26), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 24), datetime.date(2022, 2, 25), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 23), datetime.date(2022, 2, 24), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 22), datetime.date(2022, 2, 23), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 21), datetime.date(2022, 2, 22), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 20), datetime.date(2022, 2, 21), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 19), datetime.date(2022, 2, 20), freq='h', inclusive="left")),
            pd.Series([1] * 24, index=pd.date_range(datetime.date(2022, 2, 18), datetime.date(2022, 2, 19), freq='h', inclusive="left"))
        ]
        # fmt: on

    def test_query_batches_until_exhausted(self):
        with patch("tamland.core.prom_query.query_range_with_cache") as mock_cache:
            mock_cache.side_effect = self.test_data

            df = prom_query.batched_query_range(
                self.query, datetime.date(2022, 2, 18), datetime.date(2022, 2, 28)
            )
            self.assertListEqual(
                list(df.index),
                list(
                    pd.date_range(
                        datetime.date(2022, 2, 18),
                        datetime.date(2022, 2, 28),
                        freq="h",
                        inclusive="left",
                    )
                ),
            )

            # fmt: off
            mock_cache.assert_has_calls([
                call(self.query, datetime.datetime(2022, 2, 27, 0, 0), datetime.datetime(2022, 2, 27, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 26, 0, 0), datetime.datetime(2022, 2, 26, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 25, 0, 0), datetime.datetime(2022, 2, 25, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 24, 0, 0), datetime.datetime(2022, 2, 24, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 23, 0, 0), datetime.datetime(2022, 2, 23, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 22, 0, 0), datetime.datetime(2022, 2, 22, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 21, 0, 0), datetime.datetime(2022, 2, 21, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 20, 0, 0), datetime.datetime(2022, 2, 20, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 19, 0, 0), datetime.datetime(2022, 2, 19, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 18, 0, 0), datetime.datetime(2022, 2, 18, 23, 0), ignore_cache=False)
            ])
            # fmt: off

    def test_halt_if_three_empty_dfs_in_a_row(self):
        with patch("tamland.core.prom_query.query_range_with_cache") as mock_cache:
            mock_cache.side_effect = self.test_data + [
                pd.Series([], dtype="float64"),
                pd.Series([], dtype="float64"),
                pd.Series([], dtype="float64"),
            ]

            df = prom_query.batched_query_range(
                self.query, datetime.date(1999, 1, 1), datetime.date(2022, 2, 28)
            )
            self.assertListEqual(
                list(df.index),
                list(
                    pd.date_range(
                        datetime.date(2022, 2, 18),
                        datetime.date(2022, 2, 28),
                        freq="h",
                        inclusive="left",
                    )
                ),
            )

            # fmt: off
            mock_cache.assert_has_calls([
                call(self.query, datetime.datetime(2022, 2, 27, 0, 0), datetime.datetime(2022, 2, 27, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 26, 0, 0), datetime.datetime(2022, 2, 26, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 25, 0, 0), datetime.datetime(2022, 2, 25, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 24, 0, 0), datetime.datetime(2022, 2, 24, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 23, 0, 0), datetime.datetime(2022, 2, 23, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 22, 0, 0), datetime.datetime(2022, 2, 22, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 21, 0, 0), datetime.datetime(2022, 2, 21, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 20, 0, 0), datetime.datetime(2022, 2, 20, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 19, 0, 0), datetime.datetime(2022, 2, 19, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 18, 0, 0), datetime.datetime(2022, 2, 18, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 17, 0, 0), datetime.datetime(2022, 2, 17, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 16, 0, 0), datetime.datetime(2022, 2, 16, 23, 0), ignore_cache=False),
                call(self.query, datetime.datetime(2022, 2, 15, 0, 0), datetime.datetime(2022, 2, 15, 23, 0), ignore_cache=False)
            ])
            # fmt: on
            self.assertEqual(mock_cache.call_count, len(self.test_data) + 3)

    def test_ignores_cache_for_future(self):
        with patch("tamland.core.prom_query.query_range_with_cache") as mock_cache:
            mock_cache.side_effect = self.test_data

            now = datetime.datetime.now()
            beginning_of_today = now.replace(hour=0, minute=0, second=0, microsecond=0)
            end_of_today = now.replace(hour=23, minute=0, second=0, microsecond=0)
            yesterday = now - datetime.timedelta(days=1)
            beginning_of_yesterday = yesterday.replace(
                hour=0, minute=0, second=0, microsecond=0
            )
            end_of_yesterday = yesterday.replace(
                hour=23, minute=0, second=0, microsecond=0
            )
            tomorrow = now + datetime.timedelta(days=1)
            beginning_of_tomorrow = tomorrow.replace(
                hour=0, minute=0, second=0, microsecond=0
            )
            end_of_tomorrow = tomorrow.replace(
                hour=23, minute=0, second=0, microsecond=0
            )

            the_future = now + datetime.timedelta(days=2)

            prom_query.batched_query_range(
                self.query, beginning_of_yesterday, the_future
            )

            mock_cache.assert_has_calls(
                [
                    call(
                        self.query,
                        beginning_of_tomorrow,
                        end_of_tomorrow,
                        ignore_cache=True,
                    ),
                    call(
                        self.query, beginning_of_today, end_of_today, ignore_cache=True
                    ),
                    call(
                        self.query,
                        beginning_of_yesterday,
                        end_of_yesterday,
                        ignore_cache=False,
                    ),
                ]
            )

    def test_invalid_times(self):
        with self.assertRaises(AssertionError):
            prom_query.batched_query_range(
                self.query,
                datetime.datetime(2022, 2, 16, 0, 0),
                datetime.datetime(2022, 2, 15, 0, 0),
            )


class PromQueryRangeWithCacheTestSuite(unittest.TestCase):
    """query_range_with_cache unit test"""

    def setUp(self):
        self.query = "query does not matter here"
        self.from_dt = datetime.date(2022, 2, 20)
        self.to_dt = datetime.date(2022, 2, 27)
        self.test_data = pd.Series(
            [2] * 24 * 7,
            index=pd.date_range(self.from_dt, self.to_dt, freq="h", inclusive="left"),
        )

    def test_ignore_mock(self):
        with patch("tamland.core.prom_cache.cache", autospec=True) as cache, patch(
            "tamland.core.prom_query.query_range_with_retry"
        ) as mock_query:
            mock_query.return_value = self.test_data

            df = prom_query.query_range_with_cache(
                self.query, self.from_dt, self.to_dt, True
            )
            self.assertListEqual(list(df), list(self.test_data))

            mock_query.assert_called_once_with(self.query, self.from_dt, self.to_dt)
            cache.read.assert_not_called
            cache.write.assert_not_called

    def test_cache_hit(self):
        with patch("tamland.core.prom_cache.cache", autospec=True) as cache, patch(
            "tamland.core.prom_query.query_range_with_retry"
        ) as mock_query:
            cache.read.return_value = self.test_data

            df = prom_query.query_range_with_cache(self.query, self.from_dt, self.to_dt)
            self.assertListEqual(list(df), list(self.test_data))

            mock_query.assert_not_called
            cache.read.assert_called_once_with(
                PromQuery(query=self.query, from_dt=self.from_dt, to_dt=self.to_dt)
            )
            cache.write.assert_not_called

    def test_cache_missed(self):
        with patch("tamland.core.prom_cache.cache", autospec=True) as cache, patch(
            "tamland.core.prom_query.query_range_with_retry"
        ) as mock_query:
            cache.read.return_value = None
            mock_query.return_value = self.test_data

            df = prom_query.query_range_with_cache(self.query, self.from_dt, self.to_dt)
            self.assertListEqual(list(df), list(self.test_data))

            mock_query.assert_called_once_with(self.query, self.from_dt, self.to_dt)
            cache.read.assert_called_once_with(
                PromQuery(query=self.query, from_dt=self.from_dt, to_dt=self.to_dt)
            )
            cache.read.assert_called_once
            cache.write.assert_called_once_with(
                PromQuery(query=self.query, from_dt=self.from_dt, to_dt=self.to_dt),
                self.test_data,
            )


class PromSeriesParserTestSuite(unittest.TestCase):
    """Saturation forecasting unit test"""

    def test_parse_series_basic(self):
        series = '{component="pgbouncer_client_conn",env="gprd",environment="gprd",tier="db",type="pgbouncer"}'
        dict = prom_query.parse_selector(series)

        self.assertEqual(
            dict,
            {
                "component": "pgbouncer_client_conn",
                "env": "gprd",
                "environment": "gprd",
                "tier": "db",
                "type": "pgbouncer",
            },
        )

    def test_parse_series_quoted(self):
        series = '{component="\\""}'
        dict = prom_query.parse_selector(series)

        self.assertEqual(dict, {"component": '"'})

    def test_parse_series_more_equals(self):
        series = '{component="a=b"}'
        dict = prom_query.parse_selector(series)

        self.assertEqual(dict, {"component": "a=b"})

    def test_serialize_series(self):
        dict = {
            "component": "pgbouncer_client_conn",
            "env": "gprd",
        }
        series = prom_query.selector_for(dict)
        self.assertEqual(series, 'component="pgbouncer_client_conn",env="gprd"')

    def test_serialize_empty(self):
        dict = {}
        series = prom_query.selector_for(dict)
        self.assertEqual(series, "")

    def test_serialize_multiple(self):
        dict = {
            "component": "pgbouncer_client_conn",
            "env": "gprd",
            "stage": ["main", ""],
        }
        series = prom_query.selector_for(dict)
        self.assertEqual(
            series, 'component="pgbouncer_client_conn",env="gprd",stage=~"main|"'
        )
