from unittest.mock import Mock, MagicMock, patch

import pytest
from callee import Contains
from fs import open_fs

from tamland.reporting.renderer import (
    render_history_video,
    HistoryVideoRenderer,
    FfmpegUnavailable,
)


def test_render_video(result_examples_path: str, forecast_run, component):
    with (
        patch("shutil.which") as which,
        patch("subprocess.run") as run,
        patch("tamland.reporting.reporting.ForecastRunAccessor") as accessor_class,
    ):
        which.return_value = "/usr/bin/ffmpeg"

        component.saturation_point.name = "cloudsql_disk"
        component.service.name = "monitoring"

        artifact_fp = Mock()
        forecast_persistence = MagicMock()
        forecast_persistence.save_artifact.return_value.__enter__.return_value = (
            artifact_fp
        )
        run_accessor = Mock(run=forecast_run, forecast_persistence=forecast_persistence)
        accessor_class.return_value = run_accessor

        def run_function(args, **_):
            with open(args[-1], "wb") as fp:
                fp.write(b"foo bar test")

        run.side_effect = run_function

        render_history_video(result_examples_path, forecast_run, component)

        run.assert_called_once_with(
            Contains("/usr/bin/ffmpeg"), check=True, capture_output=True
        )

        forecast_persistence.save_artifact.assert_called_once_with(
            run_accessor.run, component, "history.mp4"
        )
        artifact_fp.write.assert_called_once_with(b"foo bar test")


class TestVideoRenderer:
    def test_no_ffmpeg(self, result_examples_path, component):
        with patch("shutil.which") as which:
            which.return_value = None

            renderer = HistoryVideoRenderer(open_fs(result_examples_path))
            assert renderer.ffmpeg_available() is False

            with pytest.raises(FfmpegUnavailable):
                with renderer.render(component):
                    ...
