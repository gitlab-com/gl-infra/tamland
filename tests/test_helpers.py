from datetime import datetime, timedelta
import math
import pandas as pd

from tamland.core.models import SaturationDimension


def saturation_dimensions_lookup_time_series(
    to_dt=datetime(2024, 9, 20),
    time_series_default=None,
):
    """
    This function generates a pandas DataFrame to mock a 365 day series of time series
    of dynamic saturation dimensions.
    """
    if time_series_default:
        mocked_time_series = time_series_default
    else:
        mocked_time_series = [
            {'{shard="catchall"}': 1},
            {'{shard="memory-bound"}': 1},
        ]
    from_dt = to_dt - timedelta(days=365)
    delta = (to_dt - from_dt).days
    multiplier = math.ceil(delta / len(mocked_time_series))

    time_range = pd.date_range(start=from_dt, end=to_dt, freq="D")
    # multiply time series to fill the time delta with enough datapoints
    input = (mocked_time_series * multiplier)[: delta + 1]
    return pd.DataFrame(input, index=time_range)


def each_is_instance_of_saturation_dimension(dimensions):
    # making sure all dimensions are instances of SaturationDimension
    # str and SaturationDimension are comparable as the methods __str__ and __eq__ are implemented
    # for SaturationDimension and the __hash__ method is implemented as well to allow set operations.
    for dimension in dimensions:
        assert isinstance(dimension, SaturationDimension)
    return True
