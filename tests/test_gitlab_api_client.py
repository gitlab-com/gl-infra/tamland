import unittest
from unittest.mock import patch
from requests import HTTPError
import os
import tempfile
from tamland.core import gitlab_api_client


class GitLabAPIClientTestSuite(unittest.TestCase):
    """GitLabAPIClient unit tests"""

    def test_get_issues(self):
        with patch(
            "tamland.core.gitlab_api_client.requests.get",
            side_effect=self.mock_response,
        ) as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )

            client.get_issues("1")

            mock_request.assert_called_once_with(
                "http://example.com/api/v4/projects/1/issues",
                headers=client.headers,
                params={"state": "opened", "per_page": 100},
                timeout=60,
            )

    def test_get_issues_with_label_filter(self):
        with patch(
            "tamland.core.gitlab_api_client.requests.get",
            side_effect=self.mock_response,
        ) as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )

            client.get_issues("1", labels=["one", "two"])

            mock_request.assert_called_once_with(
                "http://example.com/api/v4/projects/1/issues",
                headers=client.headers,
                params={"state": "opened", "per_page": 100, "labels": "one,two"},
                timeout=60,
            )

    def test_create_issue(self):
        with patch("tamland.core.gitlab_api_client.requests.post") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            expected_data = {"title": "redis / primary_cpu potential saturation"}

            with patch.dict(os.environ, {"APPLY_ISSUE_CHANGES": "true"}):
                client.create_issue("1", **expected_data)

                mock_request.assert_called_once_with(
                    "http://example.com/api/v4/projects/1/issues",
                    headers=client.headers,
                    json=expected_data,
                    timeout=60,
                )

    def test_update_issue(self):
        with patch("tamland.core.gitlab_api_client.requests.put") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            expected_data = {"labels": "saturation-80%-confidence"}

            with patch.dict(os.environ, {"APPLY_ISSUE_CHANGES": "true"}):
                client.update_issue("1", "25", expected_data)

                mock_request.assert_called_once_with(
                    "http://example.com/api/v4/projects/1/issues/25",
                    headers=client.headers,
                    json=expected_data,
                    timeout=60,
                )

    def test_close_issue(self):
        with patch("tamland.core.gitlab_api_client.requests.put") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            expected_data = {"state_event": "close"}

            with patch.dict(os.environ, {"APPLY_ISSUE_CHANGES": "true"}):
                client.close_issue("1", "50")

                mock_request.assert_called_once_with(
                    "http://example.com/api/v4/projects/1/issues/50",
                    headers=client.headers,
                    json=expected_data,
                    timeout=60,
                )

    def test_upload_project_file(self):
        with patch("tamland.core.gitlab_api_client.requests.post") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            expected_data = {"file": tempfile.TemporaryFile}

            with patch.dict(os.environ, {"APPLY_ISSUE_CHANGES": "true"}):
                client.upload_project_file("1", expected_data["file"])

                mock_request.assert_called_once_with(
                    "http://example.com/api/v4/projects/1/uploads",
                    headers=client.headers,
                    files=expected_data,
                    timeout=60,
                )

    def test_create_issue_link(self):
        with patch("tamland.core.gitlab_api_client.requests.post") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            expected_data = {
                "issue_iid": "25",
                "target_project_id": "1",
                "target_issue_iid": "30",
            }

            with patch.dict(os.environ, {"APPLY_ISSUE_CHANGES": "true"}):
                client.create_issue_link(
                    "1",
                    expected_data["issue_iid"],
                    "1",
                    expected_data["target_issue_iid"],
                )

                mock_request.assert_called_once_with(
                    "http://example.com/api/v4/projects/1/issues/25/links",
                    headers=client.headers,
                    json=expected_data,
                    timeout=60,
                )

    def test_create_todo_item(
        self,
    ):  # NOTE: Endpoint is used only for internal triage duties script
        with patch("tamland.core.gitlab_api_client.requests.post") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            expected_data = {
                "issue_iid": "25",
            }

            client.create_todo_item("1", expected_data["issue_iid"])

            mock_request.assert_called_once_with(
                "http://example.com/api/v4/projects/1/issues/25/todo",
                headers=client.headers,
                json=expected_data,
                timeout=60,
            )

    def test_create_note(self):
        with patch("tamland.core.gitlab_api_client.requests.post") as mock_request:
            client = gitlab_api_client.GitLabAPIClient(
                "http://example.com/api/v4", "foobar"
            )
            project_id = "1"
            expected_data = {
                "issue_iid": "25",
                "body": "Saturation is no longer being forecast by Tamland :white_check_mark:",
            }

            with patch.dict(os.environ, {"APPLY_ISSUE_CHANGES": "true"}):
                client.create_note(
                    project_id, expected_data["issue_iid"], expected_data["body"]
                )

                mock_request.assert_called_once_with(
                    "http://example.com/api/v4/projects/1/issues/25/notes",
                    headers=client.headers,
                    json=expected_data,
                    timeout=60,
                )

    def mock_response(*args, **kwargs):
        class MockResponse:
            def __init__(self, data={}, status=200, headers={}):
                self.data = data
                self.status = status
                self.headers = headers

            def json(self):
                return self.data

            def raise_for_status(self):
                if self.status != 200:
                    raise HTTPError("Mock raised exception")

        return MockResponse()
