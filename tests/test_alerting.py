import os
from datetime import datetime
from unittest.mock import Mock

import pytest
from unittest import mock

from tamland.core.alerting import (
    AlertDecisionMaker,
    AlertingRule,
    AlertingRuleOutcome,
    AlertStatus,
    AlertingRule_MeanPrediction,
    AlertingRule_ConfidenceIntervalPrediction,
)
from tamland.core.forecaster import PredictedSLOViolation
from tamland.core.models import SLO
from tamland.core.persistence import ComponentForecastOnDisk


class TestAlertingRule_MeanPrediction:
    @pytest.mark.freeze_time("2023-10-10")
    def test_evaluate_positive(self):
        forecast = Mock()
        forecast.slo_violations = [
            PredictedSLOViolation(
                slo=SLO(name="soft", threshold=0.5), yhat_date=datetime(2023, 10, 20)
            ),
            PredictedSLOViolation(
                slo=SLO(name="hard", threshold=0.8),
                yhat_date=datetime(2024, 4, 20),
                yhat_date_upper=datetime(2023, 11, 20),
            ),
            PredictedSLOViolation(slo=SLO(name="saturation", threshold=1)),
        ]

        outcome = AlertingRule_MeanPrediction().evaluate(forecast)

        assert outcome.forecast == forecast
        assert outcome.triggered
        assert outcome.slo == SLO(name="soft", threshold=0.5)
        assert "Predicted mean violation within" in outcome.description

    @pytest.mark.freeze_time("2023-10-10")
    def test_evaluate_negative(self):
        forecast = Mock()
        forecast.slo_violations = [
            PredictedSLOViolation(
                slo=SLO(name="soft", threshold=0.5), yhat_date=datetime(2024, 4, 20)
            ),
            PredictedSLOViolation(
                slo=SLO(name="hard", threshold=0.8),
                yhat_date=datetime(2024, 4, 20),
                yhat_upper_date=datetime(2023, 11, 20),
            ),
            PredictedSLOViolation(slo=SLO(name="saturation", threshold=1)),
        ]

        outcome = AlertingRule_MeanPrediction().evaluate(forecast)

        assert outcome.forecast == forecast
        assert not outcome.triggered
        assert outcome.slo is None
        assert (
            "No violations predicted or beyond alerting horizon" in outcome.description
        )


class TestAlertingRule_ConfidenceIntervalPrediction:
    @pytest.mark.freeze_time("2023-10-10")
    def test_evaluate_positive(self):
        forecast = Mock()
        forecast.slo_violations = [
            PredictedSLOViolation(
                slo=SLO(name="soft", threshold=0.5), yhat_date=datetime(2023, 10, 20)
            ),
            PredictedSLOViolation(
                slo=SLO(name="hard", threshold=0.8),
                yhat_date=datetime(2024, 4, 20),
                yhat_upper_date=datetime(2023, 11, 20),
            ),
            PredictedSLOViolation(slo=SLO(name="saturation", threshold=1)),
        ]
        forecast.metrics.confidence_range = 0.5

        outcome = AlertingRule_ConfidenceIntervalPrediction().evaluate(forecast)

        assert outcome.forecast == forecast
        assert outcome.triggered
        assert outcome.slo == SLO(name="hard", threshold=0.8)
        assert "Predicted violation based on confidence interval" in outcome.description

    @pytest.mark.freeze_time("2023-10-10")
    def test_evaluate_negative_too_far_out(self):
        forecast = Mock()
        forecast.slo_violations = [
            PredictedSLOViolation(
                slo=SLO(name="soft", threshold=0.5), yhat_date=datetime(2023, 10, 20)
            ),
            PredictedSLOViolation(
                slo=SLO(name="hard", threshold=0.8),
                yhat_date=datetime(2024, 4, 20),
                yhat_upper_date=datetime(2024, 3, 20),
            ),
            PredictedSLOViolation(slo=SLO(name="saturation", threshold=1)),
        ]
        forecast.metrics.confidence_range = 0.5

        outcome = AlertingRule_ConfidenceIntervalPrediction().evaluate(forecast)

        assert outcome.forecast == forecast
        assert not outcome.triggered
        assert outcome.slo is None
        assert "No violations predicted" in outcome.description

    @pytest.mark.freeze_time("2023-10-10")
    @mock.patch.dict(os.environ, {"TAMLAND_MAX_CONFIDENCE_RANGE": "0.4"}, clear=True)
    def test_evaluate_negative_too_uncertain(self):
        forecast = Mock()
        forecast.slo_violations = [
            PredictedSLOViolation(
                slo=SLO(name="soft", threshold=0.5), yhat_date=datetime(2023, 10, 20)
            ),
            PredictedSLOViolation(
                slo=SLO(name="hard", threshold=0.8),
                yhat_date=datetime(2024, 4, 20),
                yhat_upper_date=datetime(2023, 11, 20),
            ),
            PredictedSLOViolation(slo=SLO(name="saturation", threshold=1)),
        ]
        forecast.metrics.confidence_range = 0.5

        outcome = AlertingRule_ConfidenceIntervalPrediction().evaluate(forecast)

        assert outcome.forecast == forecast
        assert not outcome.triggered
        assert outcome.slo is None
        assert "No violations predicted, not confident enough" in outcome.description


class SimpleRule(AlertingRule):
    def __init__(self, triggered: bool):
        self.triggered = triggered

    def evaluate(self, forecast: ComponentForecastOnDisk) -> AlertingRuleOutcome:
        return AlertingRuleOutcome(
            forecast=forecast,
            triggered=self.triggered,
            description="Simple Rule",
            rule=type(self),
        )


class TestAlertDecisionMaker:
    @pytest.mark.parametrize(
        "rules,alert_status",
        [
            [[SimpleRule(True), SimpleRule(False)], AlertStatus.OPEN],
            [[SimpleRule(False), SimpleRule(True)], AlertStatus.OPEN],
            [[SimpleRule(True), SimpleRule(True)], AlertStatus.OPEN],
            [[SimpleRule(False), SimpleRule(False)], AlertStatus.CLOSED],
            [[SimpleRule(False)], AlertStatus.CLOSED],
            [[SimpleRule(True)], AlertStatus.OPEN],
        ],
    )
    def test_decide_alert_status(
        self, rules: list[AlertingRule], alert_status: AlertStatus
    ):
        forecast = Mock()

        alert = AlertDecisionMaker(*rules).decide(forecast)

        assert alert.forecast == forecast
        assert alert.rule_outcomes == [rule.evaluate(forecast) for rule in rules]
        assert alert.status == alert_status

    def test_decide_without_forecast(self):
        assert AlertDecisionMaker(SimpleRule(True)).decide(None) is None

    def test_default_factory(self):
        rule_types = [type(rule) for rule in AlertDecisionMaker.default().rules]

        assert rule_types == [
            AlertingRule_MeanPrediction,
            AlertingRule_ConfidenceIntervalPrediction,
        ]
