import os
from os.path import basename, join
from pathlib import Path
from unittest.mock import Mock

import pytest
from pandas import Timestamp
import datetime

from tamland.core.models import Component
from tamland.reporting.reporting import (
    MkdocsPage,
    ConfigFile,
    ForecastRunAccessor,
    InternalStatsPage,
    HeadroomSummaryPage,
    BasicReport,
    ReportGenerationError,
    str_or_else,
    pretty_violation_date,
    ComponentCounts,
)


class DummyPage(MkdocsPage):
    def __init__(
        self, template_name: str | None = None, target_path: str | None = None
    ):
        accessor = Mock()
        accessor.run = Mock()
        super().__init__(
            run_accessor=accessor,
            env=Mock(),
            template_name=template_name,
            target_path=target_path,
        )

    def variables(self) -> dict:
        return dict(foo="bar")


class TestMkdocsPage:
    def test_default_template_variables(self):
        page = DummyPage()

        assert page.default_template_variables() == dict(
            run=page.run_accessor.run,
            page=page,
            getenv=os.getenv,
        )

    def test_render(self):
        page = DummyPage()
        env = page.env

        content = Mock()
        template = Mock()
        template.render.return_value = content
        env.get_template.return_value = template

        result = page.render()

        assert result == content
        env.get_template.assert_called_once_with("dummy.jinja.md")
        template.render.assert_called_once_with(
            page.default_template_variables() | dict(foo="bar")
        )

    def test_link_component(self, component: Component):
        # Notice that `some-dir/here.md` will be rendered into `some-dir/here/index.html
        page = DummyPage(target_path="some-dir/here.md")
        assert (
            page.link_component("test", component=component)
            == f'[test](../../components/{component} "")'
        )

        page = DummyPage(target_path="")
        assert (
            page.link_component("test", component=component)
            == f'[test](components/{component} "")'
        )

        page = DummyPage(target_path="more/levels/here.md")
        assert (
            page.link_component(None, component=component, tooltip="test-tip")
            == f'[{component}](../../../components/{component} "test-tip")'
        )


class TestInternalStatsPage:
    def test_slo_violations(self, forecast_run_accessor: ForecastRunAccessor):
        page = InternalStatsPage(run_accessor=forecast_run_accessor, env=Mock())

        slo_violations = page.variables().get("slo_violations")

        assert slo_violations == [
            {
                "component": "monitoring.cloudsql_disk",
                "confidence_range": 0.046356918772766376,
                "earliest_slo_violation": Timestamp("2023-10-01 03:30:30"),
            }
        ]

    def test_confidence_ranges(self, forecast_run_accessor: ForecastRunAccessor):
        page = InternalStatsPage(run_accessor=forecast_run_accessor, env=Mock())

        slo_violations = page.variables().get("confidence_ranges")

        assert slo_violations == [
            {
                "component": "monitoring.cloudsql_disk",
                "confidence_range": 0.046356918772766376,
            },
            {
                "component": "monitoring.cloudsql_cpu",
                "confidence_range": 0.014248859631259988,
            },
        ]


class TestHeadroomSummaryPage:
    def test_counts(self, forecast_run_accessor_multiple_results: ForecastRunAccessor):
        page = HeadroomSummaryPage(
            run_accessor=forecast_run_accessor_multiple_results, env=Mock()
        )
        counts = page.variables().get("counts")

        assert counts["horizontally_scalable"] == ComponentCounts(
            total={"s4": 184, "s3": 137, "s2": 128, "s1": 6},
            violations={"s2": 6, "s3": 3, "s4": 11},
        )
        assert counts["non_horizontally_scalable"] == ComponentCounts(
            total={"s1": 14, "s2": 9, "s3": 26, "s4": 10},
            violations={"s1": 2, "s2": 1, "s3": 2},
        )

    def test_components_with_violations(
        self, forecast_run_accessor_multiple_results: ForecastRunAccessor
    ):
        page = HeadroomSummaryPage(
            run_accessor=forecast_run_accessor_multiple_results, env=Mock()
        )
        variables = page.variables()

        horizontally_scalable = [
            [c.component.service.name, c.component.saturation_point.name]
            for c in variables.get(
                "horizontally_scalable_important_components_with_violations"
            )
        ]
        non_horizontally_scalable = [
            [c.component.service.name, c.component.saturation_point.name]
            for c in page.variables().get(
                "non_horizontally_scalable_important_components_with_violations"
            )
        ]

        assert horizontally_scalable == [
            ["gitaly", "gitaly_active_node_available_space"],
            ["logging", "elastic_disk_space"],
            ["ci-runners", "disk_space"],
            ["ci-runners", "disk_inodes"],
            ["kube", "kube_persistent_volume_claim_disk_space"],
            ["patroni-ci", "disk_space"],
        ]
        assert non_horizontally_scalable == [
            ["patroni", "pg_int4_id"],
            ["patroni-ci", "pg_int4_id"],
            ["patroni-ci", "pg_primary_cpu"],
        ]


class TestConfigFile:
    def test_template_name(self):
        page = ConfigFile(Mock(), Mock())

        assert page.template_name == "mkdocs.jinja.yml"

    def test_target_path(self):
        page = ConfigFile(Mock(), Mock())

        assert page.target_path == "../mkdocs.yml"


class TestBasicReport:
    def test_generate(self, forecast_run_accessor, tmpdir):
        BasicReport(forecast_run_accessor, target_path=str(tmpdir)).generate()

        files = [basename(f) for f in tmpdir.listdir()]
        assert "mkdocs.yml" in files

        # Markdown files
        assert {"assets", "about", "index.md", "internal_stats.md"}.issubset(
            set([basename(f) for f in os.listdir(join(tmpdir, "docs"))])
        )

        # HTML files
        assert "index.html" in [basename(f) for f in os.listdir(join(tmpdir, "report"))]
        assert "index.html" in [
            basename(f) for f in os.listdir(join(tmpdir, "report", "internal_stats"))
        ]

    def test_clean_and_generate_with_non_empty_and_suspciously_looking_directory(
        self, forecast_run_accessor, tmpdir
    ):
        """Attempt to use and cleanup a directory that doesn't have a mkdocs.yml file"""
        tmpdir.mkdir("test")
        with pytest.raises(ReportGenerationError, match="refusing to cleanup"):
            BasicReport(forecast_run_accessor, target_path=str(tmpdir)).generate(
                clean=True
            )

    def test_clean_and_generate(self, forecast_run_accessor, tmpdir):
        """With a directory that has a mkdocs.yml before we call generate"""
        with open(join(tmpdir, "mkdocs.yml"), "w") as fp:
            fp.write("")
        with open(join(tmpdir, "other-file"), "w") as fp:
            fp.write("")

        BasicReport(forecast_run_accessor, target_path=str(tmpdir)).generate(clean=True)

        # Check we cleaned up, before we generated the new report
        assert "other-file" not in tmpdir.listdir()
        assert "mkdocs.yml" not in tmpdir.listdir()


def test_str_or_none():
    assert str_or_else("foo") == "foo"
    assert str_or_else("foo", prefix="pre") == "prefoo"
    assert str_or_else("foo", suffix="post") == "foopost"
    assert str_or_else("foo", prefix="pre", suffix="post") == "prefoopost"
    assert (
        str_or_else("foo", prefix="pre", suffix="post", if_none="bar") == "prefoopost"
    )
    assert str_or_else(None, prefix="pre", suffix="post", if_none="bar") == "bar"
    assert str_or_else(None, if_none="bar") == "bar"


@pytest.mark.freeze_time("2023-08-30")
def test_pretty_violation_date():
    assert pretty_violation_date(None) == ":ok:"
    assert pretty_violation_date(None, if_none="NONE") == "NONE"
    assert (
        pretty_violation_date(datetime.datetime(2023, 12, 1, 12, 0, 0))
        == ':warning:{ title="2023-12-01" } 3 months left'
    )
    assert (
        pretty_violation_date(
            datetime.datetime(2023, 12, 1, 12, 0, 0), format="%D-%m-%Y"
        )
        == ':warning:{ title="12/01/23-12-2023" } 3 months left'
    )


@pytest.mark.freeze_time("2023-08-30")
def test_pretty_violation_date_with_component(component: Component):
    assert (
        pretty_violation_date(
            datetime.datetime(2023, 12, 1, 12, 0, 0),
            component=component,
            page=DummyPage(target_path="test/foo.md"),
            format="%D-%m-%Y",
        )
        == ':warning:{ title="12/01/23-12-2023" } [3 months left](../../components/patroni.cpu "3 months left")'
    )


class TestForecastRunAccessor:
    def test_copy_forecast_artifact(self, forecast_run_accessor, component, tmpdir):
        path = os.path.join(tmpdir, "forecast.png")
        fixture_path = "/forecasts/components/patroni/cpu/20230925-030642-8d91d006-5b50-11ee-916e-b6701ad92d3b/forecast.png"

        status = forecast_run_accessor.copy_forecast_artifact(
            component, "forecast.png", path
        )

        assert status

        with open(
            path, "rb"
        ) as fp, forecast_run_accessor.forecast_persistence._fs.open(
            fixture_path, "rb"
        ) as fp_fixture:
            assert fp_fixture.read() == fp.read()

    def test_copy_forecast_artifact_doesntexist(
        self, forecast_run_accessor, component, tmpdir
    ):
        path = os.path.join(tmpdir, "forecast.png")

        status = forecast_run_accessor.copy_forecast_artifact(
            component, "unknown.png", path
        )

        assert status is False

        assert not Path(path).is_file()
